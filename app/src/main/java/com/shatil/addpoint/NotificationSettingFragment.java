package com.shatil.addpoint;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONArray;

import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;

/**
 * Created by Shatil on 1/26/2016.
 */
public class NotificationSettingFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    CheckBox cbEnableNotification;
    TextView tvSelectInterval;
    LinearLayout layTimer;
    TimePickerDialog timePickerDialog;
    Dialog dialogNumberPicker;
    int mHour=0,mMinute=1;
    TextView tvNeedHelp;
    String helpText="Select time interval to get notification of new products according to your notification list.";
    Activity activity;
    Context context;
    JPUtilities jpUtils;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.notification_setting_fragment , container , false);
        activity=getActivity();
        context=getContext();
        jpUtils=new JPUtilities(context);
        cbEnableNotification=(CheckBox) v.findViewById(R.id.cb_notification_enable);
        cbEnableNotification.setOnCheckedChangeListener(this);
        tvSelectInterval=(TextView) v.findViewById(R.id.tv_select_interval);
        tvSelectInterval.setOnClickListener(this);
        layTimer=(LinearLayout)v.findViewById(R.id.lay_timer);
        tvNeedHelp=(TextView)v.findViewById(R.id.tv_interval_help);
        tvNeedHelp.setOnClickListener(this);
        loadSettings();
        //timePickerDialog = new TimePickerDialog(getActivity(), mTimeSetListener, mHour, mMinute, false);
        setDialogInterface();
        setUI();
        return v;
    }
    public void setDialogInterface()
    {
        final NumberPicker npMinute,npHour;
        Button btDialogCancel,btDialogSet;
        dialogNumberPicker = new Dialog(getActivity());
        dialogNumberPicker.setTitle("Set Minimum Interval");
        dialogNumberPicker.setContentView(R.layout.dialog_layout);
        btDialogCancel = (Button) dialogNumberPicker.findViewById(R.id.bt_cancel);
        btDialogSet = (Button) dialogNumberPicker.findViewById(R.id.bt_ok);

        npHour = (NumberPicker) dialogNumberPicker.findViewById(R.id.nppicker1);
        npHour.setMaxValue(24);
        npHour.setMinValue(0);
        npHour.setValue(mHour);
        npHour.setWrapSelectorWheel(false);

        npMinute = (NumberPicker) dialogNumberPicker.findViewById(R.id.nppicker2);
        npMinute.setMaxValue(60);
        npMinute.setMinValue(1);
        npMinute.setValue(mMinute);
        npMinute.setWrapSelectorWheel(false);

        btDialogSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogNumberPicker != null && dialogNumberPicker.isShowing()) {
                    mHour = npHour.getValue();
                    mMinute = npMinute.getValue();
                    setUI();
                    dialogNumberPicker.dismiss();
                }
            }
        });
        btDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogNumberPicker != null && dialogNumberPicker.isShowing())
                    dialogNumberPicker.dismiss();
            }
        });
    }
    public void loadSettings()
    {
        if(jpUtils.getfrompref(CommonUtilities.ALARM_FLAG,false))
        {
            cbEnableNotification.setChecked(true);
        }
        else cbEnableNotification.setChecked(false);
        long interval=jpUtils.getfrompref(CommonUtilities.NOTIFICATION_INTERVAL_TIME,60*60*1000);
        interval=interval/60000;
        mMinute=(int)interval%60;
        mHour=(int)interval/60;
    }
    public void setUI()
    {
        if(cbEnableNotification.isChecked())
        {
            tvSelectInterval.setText(mHour+" Hours and " + mMinute+" Minutes");
            layTimer.setVisibility(View.VISIBLE);
        }
        else layTimer.setVisibility(View.GONE);
    }
    public boolean isNeedToKeepNotificationON()
    {
        JSONArray jsonArray = jpUtils.getjarrfrompref(NOTIFICATION_STATEMENT);
        if(jsonArray.length()==0)
        {
            return false;
        }
        return true;

    }
    public void saveSettings()
    {
        jpUtils.settopref(CommonUtilities.ALARM_FLAG,cbEnableNotification.isChecked());
        long interval_got=mHour*60+mMinute;
        interval_got=interval_got*60*1000;
        jpUtils.settopref(CommonUtilities.NOTIFICATION_INTERVAL_TIME, interval_got);
        cancelalarm(context);
        if(cbEnableNotification.isChecked()&&isNeedToKeepNotificationON())
        {
            setalarm();
        }
    }

    public void setalarm() {
        // TODO Auto-generated method stub
        try {
            AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
            Intent in = new Intent(context, Myalarm.class);
            in.putExtra("come_from", 0);
            PendingIntent alarmin = PendingIntent.getBroadcast(context, 0, in, 0);
            long interval=jpUtils.getfrompref(CommonUtilities.NOTIFICATION_INTERVAL_TIME, 60*60*1000);
            am.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+interval, interval, alarmin);
            jpUtils.settopref(CommonUtilities.ALARM_FLAG, true);
            enableboorreceiver();
        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
    }
    private void enableboorreceiver() {
        // TODO Auto-generated method stub
        try{
            ComponentName receiver = new ComponentName(context,
                    Simplebootrecever.class);
            PackageManager packm = context.getPackageManager();
            packm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        }catch(Exception e)
        {
            //Toast.makeText(getApplicationContext(), "enable error:"+e.toString(),
            //	Toast.LENGTH_LONG).show();
        }
    }

    public void cancelalarm(Context tempcont) {
        // TODO Auto-generated method stub
        try {
            Intent in = new Intent(tempcont, Myalarm.class);
            PendingIntent alarmin = PendingIntent.getBroadcast(tempcont, 0, in, 0);
            @SuppressWarnings("static-access")
            AlarmManager am = (AlarmManager) context.getSystemService(tempcont.ALARM_SERVICE);
            am.cancel(alarmin);
            disablebootreceiver(tempcont);
            jpUtils.settopref(CommonUtilities.ALARM_FLAG, false);
        } catch (Exception e) {
            //Toast.makeText(tempcont, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void disablebootreceiver(Context tempcont) {
        // TODO Auto-generated method stub
        ComponentName receiver = new ComponentName(tempcont,
                Simplebootrecever.class);
        PackageManager packm = tempcont.getPackageManager();
        packm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }



    @Override
    public void onPause() {
        super.onPause();
        saveSettings();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setUI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_select_interval:
                //timePickerDialog.show();
                dialogNumberPicker.show();
                break;
            case R.id.tv_interval_help:
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setMessage(helpText);
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.setTitle("Help Message");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }
    }
    public void show()
    {

        /*npHour.setOnValueChangedListener(this);
        b1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv.setText(String.valueOf(np.getValue()));
                d.dismiss();
            }
        });
        b2.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();*/
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mHour=hourOfDay;
            mMinute=minute;
            setUI();
        }
    };
}
