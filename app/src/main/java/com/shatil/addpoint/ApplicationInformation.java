package com.shatil.addpoint;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ApplicationInformation extends Activity{

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.applicationinformation);
		TextView tv3=(TextView)findViewById(R.id.mailme);
		tv3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mailme();
			}
		});

		ImageView ivBack=(ImageView) findViewById(R.id.ivback);
		ivBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void mailme()
	{
		String emailadd[]={"shatil200@gmail.com"};
		Intent emailintent=new Intent(android.content.Intent.ACTION_SEND);
		emailintent.putExtra(android.content.Intent.EXTRA_EMAIL,emailadd);//email addresses must be string array.....  
		emailintent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Need to do a job");
		emailintent.putExtra(android.content.Intent.EXTRA_TEXT, "");
		emailintent.setType("plain/text");
		startActivity(emailintent);
		
	}
}
