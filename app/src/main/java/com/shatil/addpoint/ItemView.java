package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;
import static com.shatil.addpoint.CommonUtilities.is_loading;
import static com.shatil.addpoint.CommonUtilities.element_count;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.SAVE_LIST;
import static com.shatil.addpoint.CommonUtilities.filename;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class ItemView extends RelativeLayout implements OnDismissListener, OnTouchListener {
	@SuppressLint("UseSparseArrays")
	int position=0;
	JSONObject jsonobj=new JSONObject();
	int add_status=0;
	long interval=0;
	int test=0;
	int no_op=0;
	HashMap<Integer, Item> hm_item;
	String name = "";
	String shop_name="No name found";
	String date = "";
	String price = "";
	String _id = "";
	String user_id = "";
	String process = "";
	String about = "";
	String address = "";
	String time = "";
	String txt_price = "Fixed";
	String txt_del = "Depends on discuss";
	String txt_pro="Personal product";
	String txt_use="New product";
	String catagory = "";
	TextView tvshopname,tvcatagory,tvtype,tvname, tvprice, tvdate, tvproduct, tvpro, tvadd,type_product,type_price,type_delivery,tvtxt_det;
	TextView etproduct, etprocess, etadd,tvrsuc,tvrunsuc,tvrratting,tvrname,tvrvatified;
	ImageView bt_ok,btopen,btclose,btmessage,bt_status,iv_show;
	TextView tvLoadPhoto;
	LinearLayout layopen,laydet,layobj,lay_ratting;
	ProgressBar pb_loading;
	RatingBar ratbar;
	FrameLayout lay_loading;
	Context context;
	SharedPreferences datacenter;
	ProgressDialog pdia;
	Bitmap bmp_admin;
	Bitmap bmp_saved;
	Bitmap bmp_not_saved;
	Uploaddownload updown;
	JPUtilities jputil;
	Create_array_statement loaded_cas;
	Create_array_statement new_cas;
    TextView tvUnsave,btremove;
	public static ItemView inflate(ViewGroup parent) {
        ItemView itemView = (ItemView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        return itemView;  
        }
	
    public ItemView(Context c) {
        this(c, null);
    }

    public ItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.item_view_children, this, true);

		jputil=new JPUtilities(context);
		datacenter = context.getSharedPreferences(filename, 0);
		bmp_admin=BitmapFactory.decodeResource(context.getResources(), R.drawable.admin);
		bmp_saved=BitmapFactory.decodeResource(context.getResources(), R.drawable.select);
		bmp_not_saved=BitmapFactory.decodeResource(context.getResources(), R.drawable.add_product);
        hm_item=new HashMap<Integer, Item>();
        setupChildren(context);
        this.context=context;
    }

    private void setupChildren(Context context) {
    	tvUnsave=(TextView)findViewById(R.id.tv_unsaved);
        tvrratting=(TextView)findViewById(R.id.tvrating);
    	tvrratting.setOnTouchListener(this);
    	lay_ratting=(LinearLayout)findViewById(R.id.layratting);
    	tvrsuc=(TextView)findViewById(R.id.tvrsuccess);
    	tvrunsuc=(TextView)findViewById(R.id.tvrunsuccess);
    	ratbar=(RatingBar)findViewById(R.id.ratown);
    	bt_ok=(ImageView)findViewById(R.id.btr_ok);
    	bt_ok.setOnTouchListener(this);
    	tvrname=(TextView)findViewById(R.id.tvrusername);
    	tvrvatified=(TextView)findViewById(R.id.tvrusertype);
    	tvshopname=(TextView)findViewById(R.id.shop_name);
    	tvname=(TextView)findViewById(R.id.name);
    	tvdate=(TextView)findViewById(R.id.tvdate);
    	tvprice=(TextView)findViewById(R.id.price);
    	tvtype=(TextView)findViewById(R.id.type);
    	tvcatagory=(TextView)findViewById(R.id.tvcatagory);
    	type_delivery=(TextView)findViewById(R.id.type_delivery);
    	type_price=(TextView)findViewById(R.id.type_price);
    	type_product=(TextView)findViewById(R.id.type_product);
    	tvtxt_det=(TextView)findViewById(R.id.tvtxt_det);
    	etadd=(TextView)findViewById(R.id.etaddress);
    	etproduct=(TextView)findViewById(R.id.etproduct);
    	etprocess=(TextView)findViewById(R.id.etprocess);
    	btopen=(ImageView)findViewById(R.id.btopen);
    	btclose=(ImageView)findViewById(R.id.btclose);
    	bt_status=(ImageView)findViewById(R.id.bt_status);
    	tvLoadPhoto=(TextView)findViewById(R.id.tv_load_photo);
    	btremove=(TextView)findViewById(R.id.btremove);
    	btmessage=(ImageView)findViewById(R.id.btmessage);
    	layopen=(LinearLayout)findViewById(R.id.layopen);
    	laydet=(LinearLayout)findViewById(R.id.laydetails);
    	layobj=(LinearLayout)findViewById(R.id.layobj);
    	lay_loading=(FrameLayout)findViewById(R.id.lay_loading);
    	pb_loading=(ProgressBar)findViewById(R.id.pb_loding);
    	iv_show=(ImageView)findViewById(R.id.iv_show);
    	iv_show.setOnTouchListener(this);
    	tvLoadPhoto.setOnTouchListener(this);
    	bt_status.setOnTouchListener(this);
    	btremove.setOnTouchListener(this);
    	btopen.setOnTouchListener(this);
    	btclose.setOnTouchListener(this);
    	btmessage.setOnTouchListener(this);
        tvUnsave.setOnTouchListener(this);
		//Toast.makeText(getContext(), "------ItemView------", Toast.LENGTH_LONG).show();
    }
    public void setItem(Item gitem,int pos){
    	position=pos;
    	lay_ratting.setVisibility(View.GONE);
    	tvrratting.setVisibility(View.VISIBLE);
    	if(gitem.get_remove_status())
    	{
    		layobj.setVisibility(View.GONE);
    		return;
    	}
    	else layobj.setVisibility(View.VISIBLE);
    	if(!hm_item.containsKey(position))
    	{
    		gitem.set_position(position);
    		hm_item.put(position, gitem);
    	}
    	Item item=hm_item.get(position);
    	try {
			setJsonvalue(item);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	tvname.setText("Name : "+ name);
    	tvprice.setText("Price : "+price);
        tvtype.setText("Use type : "+txt_use);
        tvcatagory.setText("Catagory : "+catagory);
        type_delivery.setText("Delivery type : "+txt_del);
        type_price.setText("Price type : "+txt_price);
        type_product.setText("Product type : "+txt_pro);
        etadd.setText(address);
        etprocess.setText(process);
        etproduct.setText(about);
        tvdate.setText("\nPost Date : "+date+"\nPost Time : "+time);
        //full details show or not...........
        if(!item.get_open_status())close_det();
        else open_det();
        //photo loading status....................
        try {
        	if(item.get_loding_status()==0)
        	{
        		String strphoto=dbutil.get_photo_string(_id);
        		if(!strphoto.startsWith("no_photo_found"))
        		{
        			Bitmap bmp=decodestringtophoto(strphoto);
					item.set_bitmap(bmp);
					item.set_loding_status(2);
					hm_item.put(position, item);
        		}
        	}
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(item.get_loding_status()==0)
        {
        	lay_loading.setVisibility(View.GONE);
        	tvLoadPhoto.setVisibility(View.VISIBLE);
        }
        else if(item.get_loding_status()==1)
        {
        	tvLoadPhoto.setVisibility(View.GONE);
        	lay_loading.setVisibility(View.VISIBLE);
        	iv_show.setVisibility(View.GONE);
        	pb_loading.setVisibility(View.VISIBLE);
        }
        else if(item.get_loding_status()==2)
        {
        	tvLoadPhoto.setVisibility(View.GONE);
			lay_loading.setVisibility(View.VISIBLE);
        	Bitmap bmp=item.get_bitmap();
			iv_show.setImageBitmap(bmp);
			iv_show.setVisibility(View.VISIBLE);
			pb_loading.setVisibility(View.GONE);
        }
        else if(item.get_loding_status()==3)
        {
        	tvLoadPhoto.setVisibility(View.GONE);
			lay_loading.setVisibility(View.VISIBLE);
        	Bitmap bmp=item.get_bitmap();
			iv_show.setImageBitmap(bmp);
			iv_show.setVisibility(View.VISIBLE);
			pb_loading.setVisibility(View.GONE);
        }
        if(admin())
        {
        	add_status=2;
        	tvUnsave.setVisibility(View.GONE);
        	//btmessage.setVisibility(View.GONE);
        	btremove.setVisibility(View.VISIBLE);
        	bt_status.setImageBitmap(bmp_admin);
            bt_status.setVisibility(View.VISIBLE);
        }
        else if(added())
        {
        	add_status=1;
            tvUnsave.setVisibility(View.VISIBLE);
            btremove.setVisibility(View.GONE);
        	//btmessage.setVisibility(View.VISIBLE);
			bt_status.setVisibility(View.GONE);
        }
        else 
        {
        	add_status=0;
            tvUnsave.setVisibility(View.GONE);
        	btremove.setVisibility(View.GONE);
        	//btmessage.setVisibility(View.VISIBLE);
			bt_status.setImageBitmap(bmp_not_saved);
            bt_status.setVisibility(View.VISIBLE);
        }
		//Toast.makeText(getContext(),n, Toast.LENGTH_LONG).show();
    }
    public Bitmap decodestringtophoto(String tstr)
	{
    	byte [] bytebmp;
		bytebmp=Base64.decode(tstr, 0);
		Bitmap bmp=BitmapFactory.decodeByteArray(bytebmp, 0, bytebmp.length);
        return bmp;
	}
    public void setJsonvalue(Item item) throws JSONException
    {
    	int type_pro=0,type_use=0,type_del=0,type_price=0;
    	jsonobj=item.getjson();
    	try {
			_id=jsonobj.getString("_id");
			name=jsonobj.getString("name");
			price=jsonobj.getString("price");
			user_id=jsonobj.getString("user_id");
			process=jsonobj.getString("process");
			about=jsonobj.getString("product");
			address=jsonobj.getString("address");
			date=jsonobj.getString("date");
			time=jsonobj.getString("time");
			catagory=jsonobj.getString("catagory");
			type_price=jsonobj.getInt("type_price");
			type_pro=jsonobj.getInt("type_product");
			type_use=jsonobj.getInt("type_use");
			type_del=jsonobj.getInt("type_delivery");
			tvshopname.setVisibility(View.GONE);
			if(type_pro==1)
			{
				shop_name=jsonobj.getString("shop_name");
				tvshopname.setVisibility(View.VISIBLE);
				tvshopname.setText("Company name: "+shop_name);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Toast.makeText(getContext(), "Error in set Json"+e.toString(), Toast.LENGTH_LONG).show();
		}
    	if(type_price==1) txt_price="Variable";
    	if(type_del==1) txt_del="Home delivery";
    	else if(type_del==2) txt_del="Can not give home delivery";
    	if(type_pro==1) txt_pro="Business product";
    	if(type_use==1) txt_use="Used product";
    	do_rating_works();
    }
    private void do_rating_works() throws JSONException {
		// TODO Auto-generated method stub
    	int rate=0,succ=0,unsucc=0,lavel=0;
    	String user_name="No name available";
    	try{
	    	if(stjson.has(user_id))
	    	{
	        	JSONObject temp_j=new JSONObject();
	    		temp_j=stjson.getJSONObject(user_id);
	    		user_name=temp_j.getString("user_name");
	    		if(temp_j.has("lavel"))
	    		{
		    		lavel=temp_j.getInt("lavel");
		    		rate=temp_j.getInt("rating");
					succ=temp_j.getInt("successfull");
					unsucc=temp_j.getInt("unsuccessfull");
	    		}
	    	}
    	}
    	catch (Exception e) {
			// TODO: handle exception
		}
    	if(lavel==5)
    	{
    		tvrname.setText("Publisher: "+user_name);
    		tvrvatified.setVisibility(View.VISIBLE);
    		ratbar.setMax(100);
    		ratbar.setProgress(rate);
	    	tvrsuc.setText("Successfull: "+succ);
	    	tvrunsuc.setText("Rated as cheat: "+unsucc);
    	}
    	else 
    	{
    		tvrname.setText("Publisher: "+user_name);
	    	ratbar.setMax(100);
    		ratbar.setProgress(rate);
    		tvrvatified.setVisibility(View.GONE);
	    	tvrsuc.setText("Successfull: "+succ);
	    	tvrunsuc.setText("Rated as cheat: "+unsucc);
    	}
	}

	public boolean admin()
    {
    	if(user_id.equals(admin_id)) return true;
    	return false;
    }
    public boolean added()
    {
    	return datacenter.getBoolean(_id, false);
    }
    public void add(String key) {
    	jputil.savecapedmessage(key, jsonobj, false);
    	Editor edit = datacenter.edit();
		edit.putBoolean(_id, true);
		edit.commit();
	}
    public void remove(String key)
    {
    	jputil.remove_capedmessage(key, jsonobj,"_id");
    	jputil.remove_photo(_id);
		Item item=hm_item.get(position);
		item.set_remove_status(true);
		hm_item.put(position, item);
    	if(key.equals(SAVE_LIST))
    	{
    		Editor edit = datacenter.edit();
			edit.remove(_id);
			edit.commit();
    	}
		layobj.setVisibility(View.GONE);
    }
    public void open_det()
    {
    	laydet.setVisibility(View.VISIBLE);
		//tvtxt_det.setVisibility(View.VISIBLE);
		layopen.setVisibility(View.GONE);
    }
    public void close_det()
    {
		//tvtxt_det.setVisibility(View.GONE);
		laydet.setVisibility(View.GONE);
		layopen.setVisibility(View.VISIBLE);
    }
    @Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
    public void work_for_click(View v)
    {
    	Item temp_item=hm_item.get(position);
		switch (v.getId()) {
		case R.id.btopen:
			open_det();
			temp_item.set_open_status(true);
			hm_item.put(position, temp_item);
			break;
		case R.id.btclose:
			close_det();
			temp_item.set_open_status(false);
			hm_item.put(position, temp_item);
			break;
		case R.id.bt_status:
			//add to saved list........
			if(add_status==0)
			{
				try{
				add(SAVE_LIST);
				add_status=1;
				bt_status.setVisibility(View.GONE);
                tvUnsave.setVisibility(View.VISIBLE);
				//bt_status.setImageBitmap(bmp_saved);

				Toast.makeText(getContext(), "Added to my saved ads", Toast.LENGTH_LONG).show();
				SavedProductListFragment.reset_saved_product_list.on_data_reset_pref_key(SAVE_LIST);
                }catch(Exception e)
				{
					//Toast.makeText(getContext(), "Error happened in adding the product to cart list", Toast.LENGTH_LONG).show();
				}
			}
			break;
		case R.id.btremove:
            pdia=ProgressDialog.show(context, "Operation in progress", "Please wait");
            pdia.setOnDismissListener(this);
            show_simpledecission("Confirmation", "Are you sure want to UNPUBLISH this?",get_drawable(200));
			break;
        case R.id.tv_unsaved:
            //upload value to server & then net notification
            bt_status.setImageBitmap(bmp_not_saved);
            bt_status.setVisibility(View.VISIBLE);
            tvUnsave.setVisibility(View.GONE);
            add_status=0;
            remove(SAVE_LIST);

            Toast.makeText(getContext(), "Removed from my saved ads", Toast.LENGTH_LONG).show();
            break;
		case R.id.tv_load_photo:
			
			if(element_count<100)
			{
				lay_loading.setVisibility(View.VISIBLE);
				pb_loading.setVisibility(View.VISIBLE);
				iv_show.setVisibility(View.GONE);
				tvLoadPhoto.setVisibility(View.GONE);
				temp_item.set_loding_status(1);
				hm_item.put(position, temp_item);
				if(element_count==0)
				{
					new_cas=new Create_array_statement(getContext());
				}
				element_count++;
				new_cas.add(catagory, _id, iv_show, pb_loading,lay_loading,tvLoadPhoto,add_status,hm_item.get(position));
				//Toast.makeText(getContext(), "Position:"+position+"\nELEMENT COUNT:"+element_count, Toast.LENGTH_LONG).show();
				if(is_loading==0)
				{
					photo_download_op();
				}
			}
			break;
		case R.id.iv_show:
			if(hm_item.get(position).get_loding_status()==3)
			{
				if(element_count<100)
				{
					lay_loading.setVisibility(View.VISIBLE);
					pb_loading.setVisibility(View.VISIBLE);
					iv_show.setVisibility(View.GONE);
					tvLoadPhoto.setVisibility(View.GONE);
					temp_item.set_loding_status(1);
					hm_item.put(position, temp_item);
					if(element_count==0)
					{
						new_cas=new Create_array_statement(getContext());
					}
					element_count++;
					new_cas.add(catagory, _id, iv_show, pb_loading,lay_loading,tvLoadPhoto,add_status,hm_item.get(position));
					//Toast.makeText(getContext(), "Is_LOading:"+is_loading+"\nELEMENT COUNT:"+element_count, Toast.LENGTH_LONG).show();
					if(is_loading==0)
					{
						photo_download_op();
					}
				}
			}
			else
			{
				try {
					Intent intent=new Intent(getContext(),Showphoto.class);
					intent.putExtra("_id",_id);
					intent.putExtra("catagory", jsonobj.getString("catagory"));
					intent.putExtra("name", name);
					getContext().startActivity(intent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
		case R.id.tvrating:
			tvrratting.setVisibility(View.GONE);
			lay_ratting.setVisibility(View.VISIBLE);
			break;
		case R.id.btmessage:
			if(admin_id.equals(user_id))
			{
				Intent i=new Intent(getContext(), Browsemessage.class);
				i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				i.putExtra("status_come_from", 1);
				i.putExtra("_id", _id);
				i.putExtra("catagory", catagory);
				getContext().startActivity(i);
			}
			else
			{
				Intent i=new Intent(getContext(), Browsemessage.class);
				i.putExtra("status_come_from", 2);
				i.putExtra("_id", _id);
				i.putExtra("catagory", catagory);
				i.putExtra("saver_id", user_id);
				getContext().startActivity(i);
			}
			break;
		case R.id.btr_ok:
			lay_ratting.setVisibility(View.GONE);
            tvrratting.setVisibility(View.VISIBLE);
            break;
		}	
    }
	
	public void photo_download_op() {
		// TODO Auto-generated method stub
		is_loading=1;
		loaded_cas=new Create_array_statement(getContext());
		loaded_cas=new_cas;
		Download_finish df=new Download_finish() {
			
			@Override
			public void download_finish(String tag) {
				// TODO Auto-generated method stub
				if(tag.equals("photo_load_finish"))
				{
					photo_loaded(updown.get_status_int());
				}
			}
		};
		updown=new Uploaddownload(getContext(), "/otherdetails",new_cas.create_find_statement(),df,"photo_load_finish");
		element_count=0;
		new_cas=new Create_array_statement(getContext());
	}
	public void merge_items() {
		int len=loaded_cas.get_item_length();
		for(int i=0;i<len;i++)
		{
			Item got_item=loaded_cas.get_item(i);
			int temp_pos=got_item.get_position();
			Item this_item=hm_item.get(temp_pos);
			got_item.set_open_status(this_item.get_open_status());
			got_item.set_remove_status(this_item.get_remove_status());
			hm_item.put(temp_pos, got_item);
		}
	}
	public void photo_loaded(int code)
	{
		if(code==0)
		{
			JSONArray jarr;
			try {
				jarr = new JSONArray(updown.getresult());
				int success=loaded_cas.Change_view(jarr);
				//Toast.makeText(getContext(), "Called asyntask success:"+success, Toast.LENGTH_LONG).show();
				merge_items();
				is_loading=0;
				if(element_count>0) 
				{
					photo_download_op();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				show_simpledialog("JsonError message", updown.get_status_string(),get_drawable(code));
			}
		}
		else 
		{
			loaded_cas.undo_view();
			merge_items();
			show_simpledialog("Error message", updown.get_status_string(),get_drawable(code));
		}
	}
	@Override
	public void onDismiss(DialogInterface arg0) {
		// TODO Auto-generated method stub
		if(no_op==0)
		{
			int code=updown.get_status_int();
			if(code==200)
			{
				layobj.setEnabled(true);
				remove(ADMIN_PRODUCT);
				show_simpledialog("Operation complete", "Product successfully removed",get_drawable(code));
			}
			else if(updown.get_status_int()==99)
			{
				layobj.setEnabled(true);
				remove(ADMIN_PRODUCT);
				show_simpledialog("Operation complete", "No product found on server",get_drawable(code));
			}
			else
			{
				show_simpledialog("Error message", updown.get_status_string(),get_drawable(code));
			}
		}
		else no_op=0;
	}
	
	public void show_simpledialog(String title,String message,Drawable icon)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
		    new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		            dialog.dismiss();
		        }
		    });
		alertDialog.setIcon(icon);
		alertDialog.show();
	}
	public void show_simpledecission(String title,String message,Drawable icon)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
		    new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        	//upload value to server & then set notification
					try {
				    	layobj.setEnabled(false);
						JSONObject jobj1=new JSONObject();
						jobj1.put("catagory",catagory);
						jobj1.put("statement", new JSONObject().put("_id", _id));
						jobj1.put("reg_id", reg_id);
						jobj1.put("_id", admin_id);
						updown=new Uploaddownload(context,"/removedocs",jobj1.toString(),pdia);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
		        }
		    });
		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
			    new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) {
			            dialog.dismiss();
			            no_op=1;
			            pdia.dismiss();
			        }
			    });
		alertDialog.setIcon(icon);
		alertDialog.show();
	}
	public Drawable get_drawable(int code) {
    	Bitmap bmp;
    	if(code==200||code==99) bmp=BitmapFactory.decodeResource(getResources(), R.drawable.select);
    	else bmp=BitmapFactory.decodeResource(getResources(), R.drawable.error);
		int iconsize=(int)jputil.getfrompref("drawable_size", 0);
    	bmp=Bitmap.createScaledBitmap(bmp, iconsize, iconsize, false);
    	Drawable dr=getResources().getDrawable(R.drawable.app_icon);
		dr=new BitmapDrawable(getResources(), bmp);
		return dr;
	}
}
