package com.shatil.addpoint;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import static com.shatil.addpoint.CommonUtilities.stjson;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateUtils;
import android.util.Base64;

public class MessageItem {
    Bitmap bmp;
	String KEY_TIME = "access_time";
	JSONObject jsonobj=new JSONObject();
	Context context;
    public MessageItem(JSONObject jsonobj,Context context) {
        super();
        this.jsonobj=jsonobj;
        this.context=context;
    }
    public JSONObject get_jsonobject() {
		return jsonobj;
	}
    public boolean is_ok(int status)
    {
    	if(status==0)
    	{
			return jsonobj.has("name");
    	}
    	else return true;
    }
    public boolean check_key(String key) {
		return jsonobj.has(key);
	} 
    public String get_product_id() {
		try {
			return jsonobj.getString("_id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
    public int get_rating()
    {

		try {
			JSONObject juser=stjson.getJSONObject(jsonobj.getString("saver_id"));
			return juser.getInt("rating");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
    }
    public String get_body(int status)
    {
    	String res="no datails";
    	try{
    		if(status==0)
    		{
		    	res="Product name:"+jsonobj.getString("name");
		    	res+="\nPrice:"+jsonobj.getString("price");
    		}
    		else if(status==1)
    		{
    			JSONObject juser=stjson.getJSONObject(jsonobj.getString("saver_id"));
    	    	res="Name: "+juser.getString("user_name");
    	    	if(juser.has("lavel"))
    	    	{
	    	    	int lavel=juser.getInt("lavel");
	    	    	int succ=juser.getInt("successfull");
	    	    	int unsucc=juser.getInt("unsuccessfull");
	    	    	if(lavel==5)
	    	    	{
	    	    		res+="\nSuccessfull transections: "+succ;
	    	    		res+="\nBad rated transections: "+unsucc;
	    	    		res+="\nVarified & trusted user";
	    	    	}
	    	    	else
	    	    	{
	    	    		res+="\nSuccessfull transections: "+succ;
	    	    		res+="\nRated as cheat: "+unsucc;
	    	    	}
    	    	}
    	    	else res+="\nNot rated yet";
    		}
    		else res=jsonobj.getString("message_body");
    		return res;
    	}catch(Exception e)
    	{
    		return res="No details available";
    	}
    }
    
    @SuppressWarnings("deprecation")
	public String get_time() {
		long curr_time;
		try {
			curr_time = jsonobj.getLong(KEY_TIME);
			Calendar date=Calendar.getInstance();
			date.setTimeInMillis(curr_time);
			String res=date.get(Calendar.DAY_OF_MONTH)+" "+new DateFormatSymbols().getMonths()[date.get(Calendar.MONTH)]+" "+date.get(Calendar.YEAR);
	    	res+=", "+date.get(Calendar.HOUR_OF_DAY)+":"+date.get(Calendar.MINUTE);
	    	return res;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Date 0:0:0  Time 0:0";
	}
    public Bitmap set_bitmap(String bitmapstr)
    {
		try {
			jsonobj.put("photo", true);
			byte[] bytebmp=Base64.decode(bitmapstr, 0);
			if(bytebmp.length>100)
			{
				bmp=BitmapFactory.decodeByteArray(bytebmp, 0, bytebmp.length);
			}
			else bmp=BitmapFactory.decodeResource(context.getResources(), R.drawable.no_photo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return bmp;
    }
    public Bitmap get_bitmap()
    {
    	return bmp;
    }
    public int get_receive()
    {
    	try {
			return jsonobj.getInt("receive");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
    }
}
