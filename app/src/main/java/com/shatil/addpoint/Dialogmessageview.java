package com.shatil.addpoint;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class Dialogmessageview extends DialogFragment {
    String message="";
    int status=0;
    int iconsize=0;
    String title;
    Bitmap bmp;
    Context context;
    public void initialize_bmp(Context context) {
		// TODO Auto-generated constructor stub
    	this.context=context;
    	JPUtilities jputil=new JPUtilities(context);
    	iconsize=(int)jputil.getfrompref("drawable_size", 0);
    }
    public void set_args(String message,int status) {
    	String title;
    	if(status>300) title="Taking input";
    	else if(status>200) title="Error message";
    	else if(status>100) title="Successfully done";
    	else title="Take decission";
    	set_args(message, status,title);
    }
    /**
	 * Initializes to show a dialog...
	 * @param  message shows as message. 
	 * @param  status 0 to 100 for permission type dialog.
	 * 101 to 200 for simple message showing.
	 * 201 to 300 for Error type message.
	 * @param title message title
	 * @return      void
	 * @see         #set_args(String message,int status,Bitmap bmp,String title)
	 */
    public void set_args(String message,int status,String title) {
    	Bitmap bmp;
    	if(status>300) bmp=BitmapFactory.decodeResource(context.getResources(), R.drawable.admin);
    	else if(status>200) bmp=BitmapFactory.decodeResource(context.getResources(), R.drawable.error);
    	else if(status>100) bmp=BitmapFactory.decodeResource(context.getResources(), R.drawable.select);
    	else bmp=BitmapFactory.decodeResource(context.getResources(), R.drawable.decession);
    	set_args(message, status, bmp,title);
    }
    /**
	 * Initializes to show a dialog...
	 * @param  message shows as message. 
	 * @param  status 0 to 100 for permission type dialog.
	 * 101 to 200 for simple message showing.
	 * 201 to 300 for Error type message.
	 * @param title message title
	 * @param bmp dialog icon
	 * @return      void
	 * @see         #set_args(String message,int status,String title)
	 */
    
    public void set_args(String message,int status,Bitmap bmp,String title) {
    	this.message=message;
    	this.status=status;
    	this.title=title;
    	if(iconsize>0) this.bmp=Bitmap.createScaledBitmap(bmp, iconsize, iconsize, false);
    	else this.bmp=bmp;
    }
 // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
    public Drawable get_drawable() {
		Drawable dr=new BitmapDrawable(getResources(), bmp);
		return dr;
	}
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
    	Drawable drawable=get_drawable();
    	if(status>300)
        { 
        	//input message..............................
        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(message).setCancelable(false).setIcon(drawable).setTitle(title)
	               .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                	   mListener.onDialogPositiveClick(Dialogmessageview.this);
	                   }
	               })
	               .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                	   mListener.onDialogNegativeClick(Dialogmessageview.this);
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
        }
        else if(status>200)
        {
        	//Error message..............................
        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(message).setIcon(drawable).setTitle(title).setCancelable(true)
	               .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                	   mListener.onDialogPositiveClick(Dialogmessageview.this);
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
        }
        else if(status>100)
        {
        	//success message..............................
        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(message).setIcon(drawable).setTitle(title).setCancelable(true)
	               .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                	   mListener.onDialogPositiveClick(Dialogmessageview.this);
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
        }
        else 
        { 
        	//permission message..............................
        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage("\n"+message+"\n").setCancelable(false)
	               .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                	   mListener.onDialogPositiveClick(Dialogmessageview.this);
	                   }
	               })
	               .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                	   mListener.onDialogNegativeClick(Dialogmessageview.this);
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
        }
        
    }
	
}
