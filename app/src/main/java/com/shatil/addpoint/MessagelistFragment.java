package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.MessageView.product_id;
import static com.shatil.addpoint.MessageView.catagory;
import static com.shatil.addpoint.CommonUtilities.MESSAGE_VIEW_STATUS;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.is_loading;
import static com.shatil.addpoint.CommonUtilities.element_count;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MessagelistFragment extends ListFragment{
	int status_come_from=0;
	String saver_id="";
	JSONObject jobj;
	static Onmessageupdate on_message_update;
    @SuppressLint("UseSparseArrays")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        //hm_mitem=new HashMap<Integer, MessageItem>(1000);
        element_count=0;
        is_loading=0;
        status_come_from=getActivity().getIntent().getIntExtra("status_come_from",0);
        if(status_come_from>0)
        {
        	product_id=getActivity().getIntent().getStringExtra("_id");
        	catagory=getActivity().getIntent().getStringExtra("catagory");
        }
        return v;
    }

    @Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	    MESSAGE_VIEW_STATUS=status_come_from;
        set_up_view();
        on_message_update=new Onmessageupdate() {
			@Override
			public void on_message_send(String tag) {
				// TODO Auto-generated method stub
				Log.d(getClass().getSimpleName(),"message send: successfully");
				set_up_view();
			}

			@Override
			public void on_message_received(String tag) {
				// TODO Auto-generated method stub
				Log.d(getClass().getSimpleName(),"message received: successfully");
				set_up_view();
			}
		};
	}

	public void set_up_view() {
        Log.d(getClass().getSimpleName(), "Setting Views:" + status_come_from);
        String result="";
		if(status_come_from==0)
	    {
	    	result=dbutil.get_products_with_messages().toString();
	    }
	    else if(status_come_from==1)
	    {
	    	product_id=getActivity().getIntent().getStringExtra("_id");
	    	result=dbutil.get_users_with_messages(product_id).toString();
	    }
	    else if(status_come_from==2)
	    {
	    	product_id=getActivity().getIntent().getStringExtra("_id");
	    	saver_id=getActivity().getIntent().getStringExtra("saver_id");
	    	jobj=dbutil.get_messages(product_id, saver_id);
	    	result=dbutil.get_messages(jobj).toString();
	    }
		//new GenerateNotification(getActivity(), "lay:"+result);
	    //set the list items.................

	    setlistitems(result);
	}
	public class ItemAdapter extends ArrayAdapter<MessageItem> {
        
        public ItemAdapter(Context c, List<MessageItem> items) {
            super(c, 0, items);
        }
		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
        		MessageView messageView = (MessageView)convertView;
                if (null == messageView)
                {
                    messageView = MessageView.inflate(parent);
                }
                MessageItem mi=getItem(position);
                if(!mi.check_key("photo")&&status_come_from==0)
                {
                	mi.set_bitmap(dbutil.get_photo_string(mi.get_product_id()));
                }
                messageView.setItem(mi);
                return messageView;
         }
    }
	public void setlistitems(String result)
    {
		Log.d(getClass().getSimpleName(),"Setting up List items"+result);
        try{
	    	JSONArray jsonarr=new JSONArray(result);
	    	int len=jsonarr.length();
	    	if(len==0)
	    	{
	    		ArrayList<MessageItem> items = new ArrayList<MessageItem>();
	    		MessageItem item = new MessageItem(new JSONObject(),getActivity());
				//items.add(item);
				setListAdapter(new ItemAdapter(getActivity(), items));
	    		return;
	    	}
	    	ArrayList<MessageItem> items = new ArrayList<MessageItem>();
			if(status_come_from==2||status_come_from==1)
			{
				len--;
				for (int i=0; i <= len; i++) {
					try {
						JSONObject jsonobj = jsonarr.getJSONObject(len-i);
						MessageItem item = new MessageItem(jsonobj,getActivity());
						items.add(item);
					} catch (Exception e) {
					}
				}
			}
			else
			{
		    	for (int i=0; i < len; i++) {
					try {
						JSONObject jsonobj = jsonarr.getJSONObject(i);
						MessageItem item = new MessageItem(jsonobj,getActivity());
						items.add(item);
					} catch (Exception e) {
					}
				}
			}
            setListAdapter(new ItemAdapter(getActivity(), items));
			
    	}catch(Exception e)
    	{
    	}
    }
}
