package com.shatil.addpoint;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import org.json.JSONException;
import org.json.JSONObject;

import static com.shatil.addpoint.CommonUtilities.MESSAGE_VIEW_STATUS;
import static com.shatil.addpoint.CommonUtilities.REG_STATUS;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;

/**
 * Created by Shatil on 1/13/2016.
 */
public class MessageFragment extends Fragment {
    long interval=0;
    JPUtilities jputil;
    String _id,catagory;
    JSONObject jobj;
    AppBarLayout appBarLayout;
    public MessageFragment()
    {

    }
    View rootView;
    Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        context=getContext();
        jputil = new JPUtilities(context);
        load_statics();
        rootView = inflater.inflate(R.layout.message_list , container , false);
        appBarLayout=(AppBarLayout)rootView.findViewById(R.id.message_action_bar);
        appBarLayout.setVisibility(View.GONE);
        return rootView;
    }

    private void work_for_click(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
        }
    }
    /*@Override
    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub
        if(MotionEvent.ACTION_DOWN==event.getAction())
        {
            interval=System.currentTimeMillis();
            View view=(View)rootView.findViewById(v.getId());
            view.setBackgroundColor(Color.argb(100, 0, 200, 0));
        }
        else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
        {
            long temp=System.currentTimeMillis()-interval;
            if(temp>20&&temp<2000) work_for_click(v);
            View view=(View)rootView.findViewById(v.getId());
            view.setBackgroundColor(Color.TRANSPARENT);

        }
        else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
        {
            interval=0;
            View view=(View)rootView.findViewById(v.getId());
            view.setBackgroundColor(Color.TRANSPARENT);
        }
        return true;
    }*/
    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        load_statics();
    }
    public void load_statics()
    {
        jputil.settopref("should_open_option"+"service_message",false);
        try {
            jputil.settopref("stjson_open", true);
            stjson=new JSONObject(jputil.getfrompref("user_rating"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            stjson=new JSONObject();
            jputil.settopref("user_rating", stjson.toString());
        }
    }
    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        jputil.settopref("stjson_open", false);
        jputil.settopref("user_rating", stjson.toString());
    }
}
