package com.shatil.addpoint;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class Showmessage extends Activity{
	ImageView ivmessage;
	RelativeLayout layrelative;
	ListView listitem,liststat;
	int m_status=0;
	String liststring[];
	String statusview[];
	JPUtilities jputil;
	ProgressDialog pdia;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.showmessage);
		pdia=ProgressDialog.show(this, "Searching information", "Please wait..");
		jputil=new JPUtilities(getApplicationContext());
		ivmessage=(ImageView)findViewById(R.id.ivsendmessage);
		layrelative=(RelativeLayout)findViewById(R.id.relativeop);
		listitem=(ListView)findViewById(R.id.listitem);
		liststat=(ListView)findViewById(R.id.liststat);
		
		m_status=getIntent().getIntExtra("m_status", 0);
		if(m_status==0)
		{
			layrelative.setVisibility(View.GONE);
		}
		
		try{
			String key=getIntent().getStringExtra("key");
			liststring=jputil.getcapedstarr(key);
			int len=liststring.length;
			statusview=new String[len];
			for(int i=0;i<len;i++) statusview[i]="";
		}catch(Exception e)
		{
			liststring=new String[1];
			liststring[0]="No messages found..";
			statusview=new String[1];
			statusview[0]="";
		}

		ArrayAdapter<String> ap1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.messagebody,liststring);
		ArrayAdapter<String> ap2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.statusimage,statusview);
		listitem.setAdapter(ap1);
		liststat.setAdapter(ap2);
		pdia.dismiss();
	}

}
