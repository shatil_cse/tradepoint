package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.filename;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Create_array_statement {
	JSONArray catagory,options;
	JSONObject track_cat,track_op;
	ProgressBar imvible[];
	Item item[];
	ImageView visible[];
	TextView btphoto[];
	FrameLayout lay_shophoto[];
	Bitmap bmp[],not_found;
	HashMap<String, Integer> hm;
	int len,product_status[];
	SharedPreferences datacenter;
	Context context;
	public Create_array_statement(Context context)
	{
		this.context=context;
		datacenter=context.getSharedPreferences(filename, 0);
		not_found=BitmapFactory.decodeResource(context.getResources(), R.drawable.error);
		len=0;
		product_status=new int[100];
		item=new Item[100];
		bmp=new Bitmap[100];
		lay_shophoto=new FrameLayout[100];
		btphoto=new TextView[100];
		hm=new HashMap<String, Integer>(100);
		visible=new ImageView[100];
		imvible=new ProgressBar[100];
		catagory=new JSONArray();
		options=new JSONArray();
		track_cat=new JSONObject();
		track_op=new JSONObject();
	}
	public void add(String catagory_name,String options_name,ImageView will_be_visible,ProgressBar now_visible,FrameLayout lay_photo,TextView bt_photo_load,int add_status,Item sitem)
	{
		JSONObject jop=new JSONObject();
		try {
			jop.put("_id", options_name);
			options.put(jop);
			visible[len]=will_be_visible;
			imvible[len]=now_visible;
			item[len]=sitem;
			btphoto[len]=bt_photo_load;
			lay_shophoto[len]=lay_photo;
			if(add_status>0) product_status[len]=0;
			else product_status[len]=1;
			hm.put(options_name, len++);
			if(!track_cat.has(catagory_name))
			{
				track_cat.put(catagory_name, true);
				catagory.put(catagory_name);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String create_find_statement()
	{
		try {
			JSONObject jobj=new JSONObject();
			jobj.put("$or", options);
			JSONObject job=new JSONObject();
			job.put("statement",jobj);
			job.put("catagory", catagory);
			return job.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return "{}";
		}
	}
	public int Change_view(JSONArray jarr) throws JSONException {
		String key="_id";
		int success_count=0;
		int temlen=jarr.length();
		for(int i=0;i<temlen;i++)
		{
			try{
				JSONObject jobj=jarr.getJSONObject(i);
				String option_name=jobj.getString(key);
				int position=hm.get(option_name);
				String strphoto=jobj.getString("photo");
				bmp[position]=decodestringtophoto(strphoto);
				visible[position].setImageBitmap(bmp[position]);
				imvible[position].setVisibility(View.GONE);
				visible[position].setVisibility(View.VISIBLE);
				item[position].set_loding_status(2);
				item[position].set_bitmap(bmp[position]);
				//storing data on JSONObject............
				JSONObject jres=new JSONObject();
				jres.put("count",product_status[position]);
				//Toast.makeText(context, "product_st:"+product_status[position], Toast.LENGTH_SHORT).show();
				jres.put("photo",jobj.getString("photo"));
				dbutil.createentry(option_name, strphoto);
				success_count++;
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		for(int i=0;i<len;i++)
		{
			if(imvible[i].isShown())
			{
				imvible[i].setVisibility(View.GONE);
				visible[i].setVisibility(View.VISIBLE);
				visible[i].setImageBitmap(not_found);
				item[i].set_loding_status(3);
				item[i].set_bitmap(not_found);
			}
		}
		return success_count;
	}
	public int undo_view()
	{
		for(int i=0;i<len;i++)
		{
			item[i].set_loding_status(0);
			lay_shophoto[i].setVisibility(View.GONE);
			btphoto[i].setVisibility(View.VISIBLE);
		}
		return len;
	}

	public Item get_item(int position)
	{
		if(position>=len) return null;
		else return item[position];
	}

	public int get_item_length()
	{
		return len;
	}
	
	public Bitmap decodestringtophoto(String tstr)
	{
		byte []bytebmp;
		bytebmp=Base64.decode(tstr, 0);
		Bitmap sbmp=BitmapFactory.decodeByteArray(bytebmp, 0, bytebmp.length);
        return sbmp;
	}
}
