package com.shatil.addpoint;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.MessagelistFragment.on_message_update;
import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DButilities extends Activity {
	public static final String KEY_ID = "_id";
	public static final String KEY_PHOTO = "photo_string";
	public static final String KEY_TIME = "access_time";
	public static final String KEY_MESSAGES = "all_messages";
	public static final String KEY_USERS = "all_users";
	public static final String KEY_USERS_POSITION = "users_position";
	public static final String KEY_DETAILS = "details";
	public static final String KEY_CODE = "message_code";
	public static final String KEY_CAN_SEND = "message_send";
	public static final String KEY_CONFIRM_STATUS = "confirm";
	public static final String KEY_NUMBER = "message_number";
	
	private static final String DATABASE_NAME = "Trade_DATABASE";
	private static final String DATABASE_TABLE = "ptoto_TABLE";
	private static final String MESSAGE_TABLE = "messagess_TABLE";
	private static final int DATABASE_VERSION = 1;
	Context ourcontext;
	SQLiteDatabase ourdatabase;
	Dbhelp ourhelp;
	int open=0;
	public class Dbhelp extends SQLiteOpenHelper {

		public Dbhelp(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			//Toast.makeText(ourcontext, "On create", Toast.LENGTH_LONG).show();
			db.execSQL("CREATE TABLE " + DATABASE_TABLE + " ( " + KEY_ID
					+ " TEXT PRIMARY KEY NOT NULL," + KEY_PHOTO
					+ " TEXT NOT NULL," + KEY_TIME + " INTEGER NOT NULL);");

			db.execSQL("CREATE TABLE " + MESSAGE_TABLE + " ( " + KEY_ID
					+ " TEXT PRIMARY KEY NOT NULL," + KEY_DETAILS
					+ " TEXT NOT NULL," + KEY_USERS + " TEXT NOT NULL,"+ KEY_USERS_POSITION + " TEXT NOT NULL,"+ KEY_TIME + " INTEGER NOT NULL);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			//db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			//onCreate(db);
		}
		
	}

	public DButilities(Context c) {
		ourcontext = c;
	}

	public DButilities opendbhelper() {
		open=1;
		ourhelp = new Dbhelp(ourcontext);
		ourdatabase = ourhelp.getWritableDatabase();
		//ourdatabase.delete(MESSAGE_TABLE, null, null);
		/*try{
			ourdatabase.execSQL("CREATE TABLE " + MESSAGE_TABLE + " ( " + KEY_ID
					+ " TEXT PRIMARY KEY NOT NULL," + KEY_DETAILS
					+ " TEXT NOT NULL," + KEY_USERS + " TEXT NOT NULL,"+ KEY_USERS_POSITION + " TEXT NOT NULL,"+ KEY_TIME + " INTEGER NOT NULL);");
		}catch(Exception e)
		{
		}*/
		return this;
	}

	public void close() {
		open=0;
		ourhelp.close();
	}
	public int isopen()
	{
		return open;
	}
	public long createentry(String _id, String photo_str) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(KEY_ID, _id);
		cv.put(KEY_PHOTO, photo_str);
		cv.put(KEY_TIME, System.currentTimeMillis());
		return ourdatabase.insert(DATABASE_TABLE, null, cv);
	}

	public String get_photo_string(String _id) {
		// TODO Auto-generated method stub
		String result = "no_photo_found";
		try{
			String[] data = new String[] {KEY_ID,KEY_PHOTO};
			Cursor c;
			c = ourdatabase.query(DATABASE_TABLE, data,KEY_ID+"='"+_id+"'", null, null, null,
					null);
			c.moveToFirst();
			int irow = c.getColumnIndex(KEY_ID);
			int iphoto=c.getColumnIndex(KEY_PHOTO);
			
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				if(c.getString(irow).equals(_id))
				{
					result=c.getString(iphoto);
					break;
				}
			}
		}catch(Exception e)
		{
			
		}
		return result;
	}
	public long set_message(String product_id,JSONObject details,JSONObject message,int receive,boolean can_send)
	{
		//details should contain _id & other product details...............
		//message should contain *saver_id,to,from,confirm & others..............
		long currtime=System.currentTimeMillis();
		Cursor c;
		JSONObject jpos=new JSONObject();
		JSONObject jobj=new JSONObject();
		JSONArray jarr=new JSONArray();
		try{
			message.put("receive", receive);
			String saver_id=message.getString("to");
			c = ourdatabase.query(MESSAGE_TABLE, null,KEY_ID+"='"+product_id+"'", null, null, null,
					null);
			int j=0;
			if(c.getCount()>0)
			{
				c.moveToFirst();
				//works for sorting user_id.....
				int cuser=c.getColumnIndex(KEY_USERS);
				jarr=new JSONArray(c.getString(cuser));
				int len=jarr.length();
				if(len>999)len=999;
				for(int i=0;i<len;i++)
				{
					JSONObject json=jarr.getJSONObject(i);
					String temp=json.getString("saver_id");
					if(temp.equals(saver_id))
					{
						jobj=json;
					}
					else 
					{
						jpos.put(temp, j);
						jarr.put(j++,json);
					}
				}
				ourdatabase.delete(MESSAGE_TABLE, KEY_ID+"='"+product_id+"'", null);
				//sort complete............................
			}
			//work for inserting the message.............
			int confirm=0;
			confirm=message.getInt(KEY_CONFIRM_STATUS);
			if(confirm!=5)
			{
				JSONArray message_arr=new JSONArray();
				if(jobj.has(KEY_MESSAGES))
				{
					message_arr=jobj.getJSONArray(KEY_MESSAGES);
				}
				message_arr.put(message_arr.length(),message);
				//constraction of message_array is complete...
				jobj.put(KEY_MESSAGES, message_arr);
				jobj.put(KEY_TIME, currtime);
				jobj.put(KEY_CAN_SEND, can_send);
				int num=0;
				if(jobj.has(KEY_NUMBER)) num=jobj.getInt(KEY_NUMBER);
				jobj.put(KEY_NUMBER,++num);
				jobj.put(KEY_CONFIRM_STATUS,confirm);
			}
			else if(confirm==5)
			{
				JSONArray message_arr=new JSONArray();
				if(jobj.has(KEY_MESSAGES))
				{
					message_arr=jobj.getJSONArray(KEY_MESSAGES);
				}
				message_arr.put(message_arr.length(),message);
				//constraction of message_array is complete...
				jobj.put(KEY_MESSAGES, message_arr);
				jobj.put(KEY_TIME, currtime);
				jobj.put(KEY_CAN_SEND, can_send);				
				jobj.put(KEY_CONFIRM_STATUS, 0);
				int num=0;
				jobj.put(KEY_NUMBER,num);
			}
			jobj.put("saver_id", saver_id);
			jarr.put(j,jobj);
			jpos.put(saver_id,j);
			details.put(KEY_TIME, currtime);
		}catch(Exception e)
		{
			Toast.makeText(ourcontext,"Inserting:.."+ e.toString(),Toast.LENGTH_LONG).show();
			Toast.makeText(ourcontext,"Inserting:.."+ e.toString(),Toast.LENGTH_LONG).show();
		}
		try{
		ContentValues cv = new ContentValues();
		cv.put(KEY_ID, product_id);
		cv.put(KEY_DETAILS, details.toString());
		cv.put(KEY_USERS, jarr.toString());
		cv.put(KEY_TIME, currtime);
		cv.put(KEY_USERS_POSITION, jpos.toString());
		long lll=ourdatabase.insert(MESSAGE_TABLE, null, cv);
		try{
		on_message_update.on_message_send("send");
		}catch(Exception e)
		{
			
		}
		return lll;
		}catch(Exception e)
		{
			Toast.makeText(ourcontext,"Inserting:..2:"+ e.toString(),Toast.LENGTH_LONG).show();
			Toast.makeText(ourcontext,"Inserting:..2:"+ e.toString(),Toast.LENGTH_LONG).show();
			Toast.makeText(ourcontext,"Inserting:..2:"+ e.toString(),Toast.LENGTH_LONG).show();
			Toast.makeText(ourcontext,"Inserting:..2:"+ e.toString(),Toast.LENGTH_LONG).show();
			return -1;
		}
	}
	public long set_message(String product_id,boolean can_send,int confirm,String to)
	{
		//details should contain _id & other product details...............
		//message should contain *saver_id,to,from,confirm & others..............
		Cursor c;
		JSONObject jpos=new JSONObject();
		JSONObject jobj=new JSONObject();
		JSONArray jarr=new JSONArray();
		try{
			String saver_id=to;
			c = ourdatabase.query(MESSAGE_TABLE, null,KEY_ID+"='"+product_id+"'", null, null, null,
					null);
			c.moveToFirst();
			//works for sorting user_id.....
			int cuser=c.getColumnIndex(KEY_USERS);
			jarr=new JSONArray(c.getString(cuser));
			jpos=new JSONObject(c.getString(c.getColumnIndex(KEY_USERS_POSITION)));
			int pos=jpos.getInt(saver_id);
			jobj=jarr.getJSONObject(pos);
			//work for inserting the message.............
			if(confirm==5) confirm=0;
			jobj.put(KEY_CAN_SEND, can_send);
			jobj.put(KEY_CONFIRM_STATUS,confirm);
			jarr.put(pos,jobj);
			ContentValues cv = new ContentValues();
			cv.put(KEY_ID, product_id);
			cv.put(KEY_DETAILS, c.getString(c.getColumnIndex(KEY_DETAILS)));
			cv.put(KEY_USERS, jarr.toString());
			cv.put(KEY_TIME, c.getLong(c.getColumnIndex(KEY_TIME)));
			cv.put(KEY_USERS_POSITION, jpos.toString());
			ourdatabase.delete(MESSAGE_TABLE, KEY_ID+"='"+product_id+"'", null);
			try{
				on_message_update.on_message_send("send");
			}catch(Exception e)
			{
				
			}
			long lll=ourdatabase.insert(MESSAGE_TABLE, null, cv);
			return lll;
			
		}catch(Exception e)
		{
			//Toast.makeText(ourcontext,"Inserting:.."+ e.toString(),Toast.LENGTH_LONG).show();
			//Toast.makeText(ourcontext,"Inserting:.."+ e.toString(),Toast.LENGTH_LONG).show();
			return -1;
		}
	}
	public long delete_message_of_product(String product_id)
	{
		//details should contain _id & other product details...............
				//message should contain *saver_id,to,from,confirm & others..............
				try{
					long lll=ourdatabase.delete(MESSAGE_TABLE, KEY_ID+"='"+product_id+"'", null);
					try{
						on_message_update.on_message_send("send");
					}catch(Exception e)
					{	
					}
					return lll;
					
				}catch(Exception e)
				{
					return -1;
				}
	}
	public JSONObject get_details(String product_id,JSONObject message) throws JSONException
	{
		JSONObject details=new JSONObject();
		try{
			if(message.getString("to").equals(admin_id))
			{
				JPUtilities jputil=new JPUtilities(ourcontext);
				JSONArray t_jarr=jputil.getjarrfrompref(ADMIN_PRODUCT);
				int len=t_jarr.length();
				for(int i=0;i<len;i++)
				{
					if(product_id.equals(t_jarr.getJSONObject(i).getString("_id")))
					{
						return t_jarr.getJSONObject(i);
					}
				}
			}
		}catch(Exception e)
		{
			new GenerateNotification(ourcontext,"E1"+e.toString());
		}
		details.put("_id", product_id);
		return details;
		
	}
	public long set_message(String product_id,JSONObject message,boolean can_send)
	{
		//details should contain _id & other product details...............
		//message should contain *saver_id,to,from,confirm & others..............
		int receive=1;
		long currtime=System.currentTimeMillis();
		Cursor c;
		JSONObject jpos=new JSONObject();
		JSONObject jobj=new JSONObject();
		JSONArray jarr=new JSONArray();
		JSONObject details=new JSONObject();
		try{
			message.put("receive", receive);
			String saver_id=message.getString("from");
			c = ourdatabase.query(MESSAGE_TABLE, null,KEY_ID+"='"+product_id+"'", null, null, null,
					null);
			int j=0;
			if(c.getCount()>0)
			{
				c.moveToFirst();
				//works for sorting user_id.....
				try{
				int cuser=c.getColumnIndex(KEY_USERS);
				int cdet=c.getColumnIndex(KEY_DETAILS);
				jarr=new JSONArray(c.getString(cuser));
				details=new JSONObject(c.getString(cdet));
				if(!details.has("_id")) details=get_details(product_id, message);
				//Toast.makeText(ourcontext, product_id+"\n"+message.toString(),Toast.LENGTH_LONG).show();
				}catch(Exception e)
				{
					int cuser=c.getColumnIndex(KEY_USERS);
					jarr=new JSONArray(c.getString(cuser));
					details=get_details(product_id,message);
				}
				int len=jarr.length();
				if(len>999)len=999;
				for(int i=0;i<len;i++)
				{
					JSONObject json=jarr.getJSONObject(i);
					String temp=json.getString("saver_id");
					if(temp.equals(saver_id))
					{
						jobj=json;
					}
					else 
					{
						jpos.put(temp, j);
						jarr.put(j++,json);
					}
				}
				
				ourdatabase.delete(MESSAGE_TABLE, KEY_ID+"='"+product_id+"'", null);
				//sort complete............................
			}
			else details=get_details(product_id,message);
			//Toast.makeText(ourcontext, "Details:\n"+details.toString(),Toast.LENGTH_LONG).show();
			//work for inserting the message.............
			int confirm=0;
			if(message.has(KEY_CONFIRM_STATUS)) confirm=message.getInt(KEY_CONFIRM_STATUS);
			
			JSONArray message_arr=new JSONArray();
			if(jobj.has(KEY_MESSAGES))
			{
				message_arr=jobj.getJSONArray(KEY_MESSAGES);
			}
			message_arr.put(message_arr.length(),message);
			//constraction of message_array is complete...
			jobj.put(KEY_MESSAGES, message_arr);
			jobj.put(KEY_TIME, currtime);
			if(can_send)jobj.put(KEY_CAN_SEND, can_send);
			int num=0;
			if(jobj.has(KEY_NUMBER)) num=jobj.getInt(KEY_NUMBER);
			jobj.put(KEY_NUMBER,++num);
			if(confirm==2) jobj.put(KEY_CONFIRM_STATUS,confirm);
			
			jobj.put("saver_id", saver_id);
			jarr.put(j,jobj);
			jpos.put(saver_id,j);

			details.put(KEY_TIME, currtime);
		}catch(Exception e)
		{
			Toast.makeText(ourcontext,"rInserting:"+ e.toString(),Toast.LENGTH_LONG).show();
		}
		ContentValues cv = new ContentValues();
		cv.put(KEY_ID, product_id);
		cv.put(KEY_DETAILS, details.toString());
		cv.put(KEY_USERS, jarr.toString());
		cv.put(KEY_TIME, currtime);
		cv.put(KEY_USERS_POSITION, jpos.toString());
		long lll=ourdatabase.insert(MESSAGE_TABLE, null, cv);
		try{
		on_message_update.on_message_received("received");
		}catch(Exception e)
		{
			
		}
		return lll;
	}
	public JSONArray get_products_with_messages()
	{
		JSONArray jarr=new JSONArray();
		try{
			String[] data = new String[] {KEY_DETAILS};
			Cursor c;
			c = ourdatabase.query(MESSAGE_TABLE, data,null, null, null, null,
					KEY_TIME+" DESC");
			c.moveToFirst();
			int irow = c.getColumnIndex(KEY_DETAILS);
			jarr=new JSONArray();
			int i=0;
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				jarr.put(i++,new JSONObject(c.getString(irow)));
			}
		}catch(Exception e)
		{
			
		}
		return jarr;
	}
	public JSONArray get_users_with_messages(String _id)
	{
		JSONArray arr_users=new JSONArray();
		try{
			String[] data = new String[] {KEY_USERS};
			Cursor c;
			c = ourdatabase.query(MESSAGE_TABLE, data,KEY_ID+"='"+_id+"'", null, null, null,null);
			c.moveToFirst();
			int irow = c.getColumnIndex(KEY_USERS);
			arr_users=new JSONArray(c.getString(irow));
		}catch(Exception e)
		{
			
		}
		return arr_users;
	}
	public JSONObject get_messages(String _id,String saver_id)
	{
		JSONObject jobj=new JSONObject();
		JSONArray arr_users=new JSONArray();
		try{
			String[] data = new String[] {KEY_USERS,KEY_USERS_POSITION};
			Cursor c;
			c = ourdatabase.query(MESSAGE_TABLE, data,KEY_ID+"='"+_id+"'", null, null, null,null);
			c.moveToFirst();
			int irow = c.getColumnIndex(KEY_USERS);
			int ipos=c.getColumnIndex(KEY_USERS_POSITION);
			arr_users=new JSONArray(c.getString(irow));
			int pos=new JSONObject(c.getString(ipos)).getInt(saver_id);
			jobj=arr_users.getJSONObject(pos);
		}catch(Exception e)
		{
			
		}
		return jobj;
	}
	public JSONArray get_messages(JSONObject source)
	{
		try {
			return source.getJSONArray(KEY_MESSAGES);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new JSONArray();
		}
	}
	public boolean get_can_send(JSONObject source)
	{
		try {
			return source.getBoolean(KEY_CAN_SEND);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
	}
	public int get_confirm_status(JSONObject source)
	{
		try {
			return source.getInt(KEY_CONFIRM_STATUS);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	public int get_number(JSONObject source)
	{
		try {
			return source.getInt(KEY_NUMBER);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	public long get_time(JSONObject source)
	{
		try {
			return source.getLong(KEY_TIME);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	public long deleteentry(String _id) {
		// TODO Auto-generated method stub
		return ourdatabase.delete(DATABASE_TABLE, KEY_ID + "='" + _id+"'", null);
	}
/*
	public String getname(long l) {
		// TODO Auto-generated method stub
		String[] data = new String[] { KEY_ROWID, KEY_NAME, KEY_AGE };
		Cursor c;
		c = ourdatabase.query(DATABASE_TABLE, data, KEY_ROWID + "=" + l, null,
				null, null, null);
		String result = "No data";
		if (c != null) {
			c.moveToFirst();
			result = c.getString(1);
		}
		return result;
	}

	public String getage(long l) {
		// TODO Auto-generated method stub
		String[] data = new String[] { KEY_ROWID, KEY_NAME, KEY_AGE };
		Cursor c;
		c = ourdatabase.query(DATABASE_TABLE, data, KEY_ROWID + "=" + l, null,
				null, null, null);
		String result = "No data";
		if (c != null) {
			c.moveToFirst();
			result = c.getString(2);
		}
		return result;
	}

	public long updateentry(String pname, String page, long l) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(KEY_NAME, pname);
		cv.put(KEY_AGE, page);
		return ourdatabase.update(DATABASE_TABLE, cv, KEY_ROWID + "=" + l, null);
		
	}

	public void search(int roll, String name, String marks, int flag) {
		element = 0;
		String[] data = new String[] { KEY_ROWID, KEY_NAME, KEY_AGE };
		Cursor c;
		if (flag == 0)
			c = ourdatabase.query(DATABASE_TABLE, data, select[flag] + roll,
					null, null, null, null);
		else if (flag == 1)
			c = ourdatabase.query(DATABASE_TABLE, data, select[flag] + name+"'",
					null, null, null, null);
		else
			c = ourdatabase.query(DATABASE_TABLE, data, select[flag] + marks,
					null, null, null, null);

		if (c != null) {

			element = c.getCount();
			sname = new String[element];
			sroll = new String[element];
			smarks = new String[element];
			int temp = 0;
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				sroll[temp] = c.getString(0);
				sname[temp] = c.getString(1);
				smarks[temp] = c.getString(2);
				temp++;
			}
		}
	}

	public String getindexname(int flag) {
		return sname[flag];
	}

	public String getinderoll(int flag) {
		return sroll[flag];
	}

	public String getindemarks(int flag) {
		return smarks[flag];
	}

	public int getmaxrow() {
		// TODO Auto-generated method stub
		return element;
	}*/
}