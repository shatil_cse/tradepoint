package com.shatil.addpoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.shatil.addpoint.CommonUtilities.EXTRA_MESSAGE;
import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;
public class Myalarm extends BroadcastReceiver {
	Context context;
	static On_task_complete notification_download_complete;
	@Override
	public void onReceive(Context cont, Intent intent) {
		// TODO Auto-generated method stub
		try{
			context = cont;
			int come_from=intent.getIntExtra("come_from", 0);
			if(come_from==0)
			{
				notification_download_complete=new On_task_complete() {
					
					@Override
					public void on_task_complete(String tag) {
						// TODO Auto-generated method stub
						download_rating(tag);
					}
				};
				JPUtilities jputil=new JPUtilities(context);
				String statement=jputil.getcapedjarr(NOTIFICATION_STATEMENT).toString();
				if(!statement.startsWith("false")) new Uploaddownload(context,"/takenotification",statement,2);
			}
			else if(come_from==1)
			{
				String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
	        	JPUtilities jputil=new JPUtilities(context);
	        	try{
	        		JSONObject jobj=new JSONObject(newMessage);
	        		if(jobj.getBoolean("enable_setting"))
	            	{
	        			jputil.savecapedmessage("service_message", jobj.getString("message_body"),false,false);
		            	jputil.settopref("settings", jobj.getJSONObject("settings").toString());
		            	Intent i=new Intent(context, Records.class);
		            	i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		            	new GenerateNotification(context,"New setting saved");
		            	jputil.settopref("should_open_option"+"service_message",true);
	            		
	            	}
		            if(jobj.getBoolean("enable_registration"))
		            {
		            	jputil.savecapedmessage("service_message", jobj.getString("message_body"),false,false);
		            	//Intent i=new Intent(context,Browsemessage.class);
		            	Intent i=new Intent(context, Service_messages.class);
		            	i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		            	new GenerateNotification(context,"Registration successfull",i,R.drawable.noti_app_icon);
		            	jputil.settopref("should_open_option"+"service_message",true);
		            }
		            if(jobj.getBoolean("enable_service"))
		            {
		            	jputil.savecapedmessage("service_message", jobj.getString("message_body"),false,false);
		            	Intent i=new Intent(context, Service_messages.class);
		            	i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		            	new GenerateNotification(context,"Service Message received",i,R.drawable.noti_app_icon);
		            	jputil.settopref("should_open_option"+"service_message",true);
	            		
		            	//Toast.makeText(getApplicationContext(), "Service message:"+jobj.getString("message_body"), Toast.LENGTH_LONG).show();
		            }
		            /*if(jobj.getBoolean("enable_product"))
		            {
		            	JSONArray jarr=jobj.getJSONArray("product_list");
		            	int len=jarr.length();
		            	for(int i=0;i<len;i++) jputil.savecapedmessage("product_message", jarr.getString(i),false,false);
		            	//Toast.makeText(getApplicationContext(), "Check product:"+jobj.getJSONArray("product_list").toString(), Toast.LENGTH_LONG).show();
		            	new GenerateNotification(context,"Product checking..");
		            }*/
		            if(jobj.has("enable_pref_change"))
		            {
			            if(jobj.getBoolean("enable_pref_change"))
			            {
			            	JSONObject eleobj=new JSONObject();
				            JSONArray jarr=new JSONArray();
				            int len=0;
			            	try{
				            	jarr=jobj.getJSONArray("int_arr");
				            	len=jarr.length();		            	
				            	for(int i=0;i<len;i++) 
				            	{
				            		eleobj=jarr.getJSONObject(i);
				            		jputil.setintpref(eleobj.getString("key"),eleobj.getInt("value"));
				            	}
				            }catch(Exception e)
			            	{
			            		
			            	}
				            try{
				            	jarr=jobj.getJSONArray("str_arr");
				            	len=jarr.length();		            	
				            	for(int i=0;i<len;i++) 
				            	{
				            		eleobj=jarr.getJSONObject(i);
				            		jputil.settopref(eleobj.getString("key"),eleobj.getString("value"));
				            	}
				            }catch(Exception e)
			            	{
			            		
			            	}
			            	try{
				            	jarr=jobj.getJSONArray("long_arr");
				            	len=jarr.length();
				            	for(int i=0;i<len;i++) 
				            	{
				            		eleobj=jarr.getJSONObject(i);
				            		jputil.settopref(eleobj.getString("key"),eleobj.getLong("value"));
				            	}
				            }catch(Exception e)
			            	{
			            		
			            	}
			            	try{
				            	jarr=jobj.getJSONArray("boolean_arr");
				            	len=jarr.length();
				            	for(int i=0;i<len;i++) 
				            	{
				            		eleobj=jarr.getJSONObject(i);
				            		jputil.settopref(eleobj.getString("key"),eleobj.getBoolean("value"));
				            	}
			            	}catch(Exception e)
			            	{
			            		
			            	}
			            }
		            }
		            if(jobj.getBoolean("proposal_message"))
		            {
		            	try{
		            		JSONObject jobj2=jobj.getJSONObject("message");
		            		JSONObject message=jobj2.getJSONObject("message");
		            		JSONObject juser=jobj2.getJSONObject("user_info");
		            		try
		            		{
		            			dbutil.set_message(message.getString("_id"), message,true);
		            			if(!jputil.getfrompref("stjson_open", false)) 
		            			{
		            				try{
		            				stjson=new JSONObject(jputil.getfrompref("user_rating"));
		            				}catch(Exception e)
		            				{
		            					stjson=new JSONObject();
		            					jputil.settopref("user_rating", stjson.toString());
		            				}
		            			}
		            			stjson.put(juser.getString("_id"), juser);
		            			jputil.settopref("user_rating", stjson.toString());
		            		}catch(Exception e)
		            		{
		            			try{
	            				stjson=new JSONObject(jputil.getfrompref("user_rating"));
	            				}catch(Exception e1)
	            				{
	            					stjson=new JSONObject();
	            					jputil.settopref("user_rating", stjson.toString());
	            				}
		            			stjson.put(juser.getString("_id"), juser);
		            			jputil.settopref("user_rating", stjson.toString());
		            			
		            			DButilities dbutil=new DButilities(context);
		            			dbutil.opendbhelper();
		            			dbutil.set_message(message.getString("_id"), message,true);
		            			dbutil.close();
		            		}
		            		set_new_message(message.getString("_id"), message.getString("from"), jputil);
		            		Intent i=new Intent(context, Browsemessage.class);
							i.putExtra("status_come_from",0);
			            	i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			            	jputil.settopref("should_open_option"+"all_message",true);
		            		new GenerateNotification(context, "New message received",i,R.drawable.noti_message);
		            	}catch(Exception e)
		            	{
		            		new GenerateNotification(context, "E5"+e.toString());
		            	}
		            }
		            //else Toast.makeText(getApplicationContext(), "Simple Broadcast Message:\n"+newMessage, Toast.LENGTH_LONG).show();
	        	}catch(Exception e)
	        	{
	        		/*Toast.makeText(cont, newMessage, Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, newMessage, Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, newMessage, Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, newMessage, Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, newMessage, Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, e.toString(), Toast.LENGTH_LONG).show();
	        		Toast.makeText(cont, e.toString(), Toast.LENGTH_LONG).show();*/
	        		//new GenerateNotification(cont,"Error happned in receiving message"+e.toString());
	        	}
			}
			else if(come_from==2)
			{
				new GenerateNotification(cont, intent.getExtras().getString(EXTRA_MESSAGE));
			}
		}catch(Exception e)
		{
			
		}
	}
	public void set_new_message(String product_id,String saver_id,JPUtilities jputil)
	{
		try{
			JSONObject json;
			try {
				json=new JSONObject(jputil.getfrompref("got_new_message"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				json=new JSONObject();
			}
			JSONObject jproduct=new JSONObject();
			if(json.has(product_id))  jproduct=json.getJSONObject(product_id);
			jproduct.put(saver_id, true);
			json.put(product_id, jproduct);
			jputil.settopref("got_new_message", json.toString());
		}catch(Exception e)
		{
			
		}
	}
	public void download_rating(String result)
	{
		try{
			JSONArray jarr=new JSONArray(result);
			int len=jarr.length();
			if(len<1) return;
			JSONArray jarr2=new JSONArray();
			JSONObject jtrac=new JSONObject();
			for(int i=0;i<len;i++) 
			{
				String tuser_id=jarr.getJSONObject(i).getString("user_id");
				if(!jtrac.has(tuser_id)) 
				{
					jarr2.put(new JSONObject().put("_id",tuser_id));
					jtrac.put(tuser_id, true);
				}
			}
			JSONObject object=new JSONObject();
			object.put("reg_id",reg_id );
			object.put("user_id",admin_id);
			JSONObject statement=new JSONObject();
			statement.put("$or", jarr2);
			object.put("statement",statement);
			new Uploaddownload(context, "/download_rating",object.toString(),3);
			}catch(Exception e)
			{
				//new GenerateNotification(context,"In rating:"+ e.toString());
			}
	}
}
