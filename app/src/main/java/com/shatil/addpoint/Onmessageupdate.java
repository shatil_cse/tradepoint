package com.shatil.addpoint;

public interface Onmessageupdate {
	public void on_message_received(String tag);
	public void on_message_send(String tag);
}
