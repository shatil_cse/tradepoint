package com.shatil.addpoint;
import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.NOTIFIED_PRODUCT;

import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class Browseresult extends FragmentActivity implements OnDismissListener,Dialogmessageview.NoticeDialogListener, OnTouchListener{
	String result;
	int view_status=0;
	TextView ivoption,ivchange;
	JPUtilities jputil;
	RelativeLayout lay_op;
	LinearLayout lay_timer;
	Button bt_save;
	Uploaddownload updown;
	long interval=0;
	TimePicker np_interval;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		int open=0;
		try{
			open=dbutil.isopen();
			if(open==0) finish();
		}catch(Exception e)
		{
			finish();
		}
		if(open==0) finish();
		else
		{
			view_status=getIntent().getIntExtra("view_status", 0);
			jputil=new JPUtilities(getApplicationContext());
			if(view_status==11||view_status==5||view_status==6)
	        {
				setContentView(R.layout.activity_list_with_option);
				ivoption=(TextView)findViewById(R.id.ivoption);
				ivchange=(TextView)findViewById(R.id.ivchange);
				lay_op=(RelativeLayout)findViewById(R.id.option_menu);
				lay_timer=(LinearLayout)findViewById(R.id.lay_timer);
				bt_save=(Button)findViewById(R.id.bt_save);
				np_interval=(TimePicker)findViewById(R.id.npinterval);
				np_interval.setIs24HourView(true);
				if(view_status==11)
				{
					this.setTitle("Notification settings");
					//notification list
					if(jputil.getfrompref("alarm_flag", false)) ivoption.setText("Disable notification");
					else ivoption.setText("Enable notification");
					ivchange.setVisibility(View.VISIBLE);
				}
				else if(view_status==5)
				{
					//admin product
					ivoption.setText("Refresh");
				}
				else if(view_status==6)
				{
					//notified product..
					ivoption.setText("Clear all");
				}
				ivoption.setOnTouchListener(this);
				ivchange.setOnTouchListener(this);
				bt_save.setOnTouchListener(this);
				ImageView ivBack=(ImageView) findViewById(R.id.ivback);
				ivBack.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						finish();
					}
				});
	        }
	        else
	        {
	        	setContentView(R.layout.activity_list);
	        }
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(view_status==11||view_status==5||view_status==6)
        {
			if(lay_timer.getVisibility()==View.VISIBLE)
			{
				lay_op.setVisibility(View.VISIBLE);
				lay_timer.setVisibility(View.GONE);
			}
			else finish();
        }
		else finish();
	}

	public void work_for_click(View v)
	{
		switch (v.getId()) {
		case R.id.ivoption:
			if(view_status==11)
			{
				if(jputil.getfrompref("alarm_flag", false)) 
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(getApplicationContext());
					
					cdia.set_args("Are you sure to turn notification OFF?", 0);
					cdia.show(getSupportFragmentManager(), "cancel_alarm");
				}
				else 
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(getApplicationContext());
					
					cdia.set_args("Are you sure to turn notification ON?", 0);
					cdia.show(getSupportFragmentManager(), "set_alarm");
				}
			}
			else if(view_status==5)
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Are you sure to check your product on server?", 0);
				cdia.show(getSupportFragmentManager(), "update_admin");
			}
			else if(view_status==6)
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Are you sure to Clear product list?", 0);
				cdia.show(getSupportFragmentManager(), "clear_np_list");
			}
			break;
		case R.id.ivchange:
			lay_op.setVisibility(View.GONE);
			long interval=jputil.getfrompref("notification_time_interval", 60*60*1000);
			int minutes=(int)interval/60000;
			int hours=minutes/60;
			minutes=minutes%60;
			np_interval.setCurrentHour(hours);
			np_interval.setCurrentMinute(minutes);
			lay_timer.setVisibility(View.VISIBLE);
			break;
		case R.id.bt_save:
			lay_op.setVisibility(View.VISIBLE);
			lay_timer.setVisibility(View.GONE);
			long interval_got=np_interval.getCurrentHour()*60+np_interval.getCurrentMinute();
			interval_got=interval_got*60*1000;
			jputil.settopref("notification_time_interval", interval_got);
			if(jputil.getfrompref("alarm_flag", false))
			{
				cancelalarm(getApplicationContext());
				setalarm();
			}
			//Toast.makeText(getApplicationContext(), "interval:"+interval_got, Toast.LENGTH_LONG).show();
			break;
		}
	}
	public void restart_browse_result(String key,int view_status) {
		Intent intent=new Intent(getApplicationContext(),Browseresult.class);
		intent.putExtra("pref_key",key);
		intent.putExtra("load_result", 1);
		intent.putExtra("view_status", view_status);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	public String get_json_string() {
		try{
		JSONObject statement=new JSONObject();
		statement.put("user_id", jputil.getfrompref("user_id"));
		JSONObject jsonobj=new JSONObject();
		jsonobj.put("statement", statement);
		jsonobj.put("text_search", false);
		jsonobj.put("reg_id", reg_id);
		jsonobj.put("user_id",admin_id);
		return jsonobj.toString();
		}catch(Exception e)
		{
			return new JSONObject().toString();
		}
		
	}
	public void update_admin_product()
	{
		ProgressDialog pdia=ProgressDialog.show(this, "Operation in progress", "Please wait..");
		pdia.setOnDismissListener(this);
		//do the search....
		updown=new Uploaddownload(getApplicationContext(), "/getalldata", get_json_string(),pdia);
	}
	public void setalarm() {
		// TODO Auto-generated method stub
		try {
			AlarmManager am = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
			Intent in = new Intent(getApplicationContext(), Myalarm.class);
			in.putExtra("come_from", 0);
			PendingIntent alarmin = PendingIntent.getBroadcast(getApplicationContext(), 0, in, 0);
			long interval=jputil.getfrompref("notification_time_interval", 60*60*1000);
			am.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+interval, interval, alarmin);
			jputil.settopref("alarm_flag", true);
			enableboorreceiver();
		} catch (Exception e) {
			//Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	private void enableboorreceiver() {
		// TODO Auto-generated method stub
		try{
		ComponentName receiver = new ComponentName(getApplicationContext(),
				Simplebootrecever.class);
		PackageManager packm = getApplicationContext().getPackageManager();
		packm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
		}catch(Exception e)
		{
			//Toast.makeText(getApplicationContext(), "enable error:"+e.toString(),
				//	Toast.LENGTH_LONG).show();
		}
	}

	private void disablebootreceiver(Context tempcont) {
		// TODO Auto-generated method stub

		ComponentName receiver = new ComponentName(tempcont,
				Simplebootrecever.class);
		PackageManager packm = tempcont.getPackageManager();
		packm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

	public void cancelalarm(Context tempcont) {
		// TODO Auto-generated method stub
		try {
				Intent in = new Intent(tempcont, Myalarm.class);
				PendingIntent alarmin = PendingIntent.getBroadcast(tempcont, 0, in, 0);
				@SuppressWarnings("static-access")
				AlarmManager am = (AlarmManager) getSystemService(tempcont.ALARM_SERVICE);
				am.cancel(alarmin);
				jputil.settopref("alarm_flag", false);
				//Toast.makeText(tempcont, "Alarm Canceled..", Toast.LENGTH_SHORT).show();
				disablebootreceiver(tempcont);
		} catch (Exception e) {
			//Toast.makeText(tempcont, e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		try{
			if(updown.get_status_int()==0)
			{
				String result=updown.getresult();
				jputil.settopref(ADMIN_PRODUCT, result);
				restart_browse_result(ADMIN_PRODUCT, 5);
			}
			else if(updown.get_status_int()==99)
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args(updown.get_status_string(),101,BitmapFactory.decodeResource(getResources(), R.drawable.warning),"Empty result");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
			else
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args(updown.get_status_string(),201,"Error happned");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
		}catch(Exception e)
		{
		}
	}
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		String st=dialog.getTag();
		if(st.equals("set_alarm"))
		{
			setalarm();
			ivoption.setText("Disable notification");
		}
		else if(st.equals("cancel_alarm"))
		{
			cancelalarm(getApplicationContext());
			ivoption.setText("Enable notification");
		}
		else if(st.equals("update_admin"))
		{
			update_admin_product();
		}
		else if(st.equals("clear_np_list"))
		{
			jputil.removefrompref(NOTIFIED_PRODUCT);
			restart_browse_result(NOTIFIED_PRODUCT, 6);
		}
	}
	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
}
