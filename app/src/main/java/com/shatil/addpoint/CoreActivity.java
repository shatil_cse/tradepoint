package com.shatil.addpoint;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.LAST_SEARCH;
import static com.shatil.addpoint.CommonUtilities.NOTIFIED_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.REG_STATUS;
import static com.shatil.addpoint.CommonUtilities.SAVE_LIST;
import static com.shatil.addpoint.CommonUtilities.SENDER_ID;
import static com.shatil.addpoint.CommonUtilities.SERVER_URL;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.CommonUtilities.catagory;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.progdia;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;

/**
 * Created by Shatil on 1/12/2016.
 */

public class CoreActivity extends ActionBarActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    ImageView ivsell,ivsearch,ivhome;
    Bitmap bmpsell,bmpsearch,bmphome;
    Bitmap bmpsell1,bmpsearch1,bmphome1;
    AsyncTask<Void, Void, Boolean> mRegisterTask;
    String user_id;
    int status_open_tab=3;
    ProgressDialog pdia;
    int status_pdia=0;
    JPUtilities jputil;
    Context context;
    ViewPager pager;
    CoreViewPagerAdapter tab_adapter;
    String Titles[]=new String[]{"Search Ads","Saved Adds","Notified Adds","Messages"};
    int[] tabIcons = new int[]{R.drawable.search,R.drawable.select,R.drawable.note_product,R.drawable.message};
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.core_activity);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        pager = (ViewPager) findViewById(R.id.pager);
        this.context = getApplicationContext();
        tab_adapter = new CoreViewPagerAdapter(getApplicationContext(), getSupportFragmentManager(), Titles, tabIcons, Titles.length);
        pager.setAdapter(tab_adapter);
        pager.setOffscreenPageLimit(3);
        tabByCustom();
        pager.addOnPageChangeListener(this);
        jputil=new JPUtilities(getApplicationContext());
        jputil.settopref("drawable_size", 30);
        checkappsettings();
        status_pdia=1;
        if(checkname()) checkregistration();
        load_statics();
    }
    @SuppressWarnings("deprecation")
    @Override
    protected void onDestroy() {
        save_statics();
        super.onDestroy();
    }
    @SuppressWarnings("deprecation")
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        try {
            jputil.settopref("stjson_open", true);
            stjson=new JSONObject(jputil.getfrompref("user_rating"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            stjson=new JSONObject();
            jputil.settopref("user_rating", stjson.toString());
        }
        super.onResume();
    }
    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        jputil.settopref("stjson_open", false);
        jputil.settopref("user_rating", stjson.toString());
        super.onPause();
    }
    public void load_statics()
    {
        dbutil=new DButilities(getApplicationContext());
        dbutil.opendbhelper();
    }
    public void save_statics() {
        dbutil.close();
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
    }

    private boolean checkname() {
        // TODO Auto-generated method stub
        if(jputil.getfrompref("user_name").length()<1)
        {
            final LinearLayout lin_lay=(LinearLayout)findViewById(R.id.lay_input_name);
            lin_lay.setVisibility(View.VISIBLE);
            final EditText et_name=(EditText)findViewById(R.id.et_name);
            ImageView bt_ok=(ImageView)findViewById(R.id.iv_next);
            bt_ok.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    String name=et_name.getText().toString();
                    name=name.trim();
                    if(name.length()>2&&name.length()<31)
                    {
                        jputil.settopref("user_name", name);
                        jputil.settopref("user_address", "Not available");
                        jputil.settopref("user_phone", "Not available");
                        admin_name=name;
                        lin_lay.setVisibility(View.GONE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                        checkregistration();
                    }
                    else Toast.makeText(getApplicationContext(), "Enter a valid name between 3 to 30 latters", Toast.LENGTH_LONG).show();
                }
            });
        }
        else
        {
            admin_name=jputil.getfrompref("user_name");
            return true;
        }
        return false;
    }

    @SuppressLint("NewApi")
    private void design() {
        // TODO Auto-generated method stub
        ivsell=(ImageView)findViewById(R.id.ivsell);
        ivsell.setOnClickListener(this);

        ivsearch=(ImageView)findViewById(R.id.ivsearch);
        ivsearch.setOnClickListener(this);

        ivhome=(ImageView)findViewById(R.id.ivhome);
        ivhome.setOnClickListener(this);

        bmpsearch= BitmapFactory.decodeResource(getResources(), R.drawable.ssearch);
        bmpsearch1=BitmapFactory.decodeResource(getResources(), R.drawable.dsearch);
        bmpsell=BitmapFactory.decodeResource(getResources(), R.drawable.ssell);
        bmpsell1=BitmapFactory.decodeResource(getResources(), R.drawable.dsell);
        bmphome=BitmapFactory.decodeResource(getResources(), R.drawable.shome);
        bmphome1=BitmapFactory.decodeResource(getResources(), R.drawable.dhome);
    }
    public void setupview() {
        // TODO Auto-generated method stub
        checkappsettings();
        status_open_tab=getIntent().getIntExtra("status_open_tab", 3);
        //design();
        //newtabbyintent();
    }
    @SuppressLint("NewApi")
    private void newtabbyintent() {
        // TODO Auto-generated method stub
        try{
            setrecords_tab();
            setbrowse_tab();
            setsellproduct_tab();
            setcurrent_tab(status_open_tab);
        }catch(Exception e)
        {
            //Toast.makeText(getApplicationContext(), "Error in newtabbyintent"+e.toString(), Toast.LENGTH_LONG).show();
        }
    }
    private void setcurrent_tab(int position) {
        // TODO Auto-generated method stub
        /*ivhome.setImageBitmap(bmphome1);
        ivsearch.setImageBitmap(bmpsearch1);
        ivsell.setImageBitmap(bmpsell1);
        if(position==1)
        {
            ivsell.setImageBitmap(bmpsell);
            th.setCurrentTabByTag("sellproduct");
        }
        else if(position==2)
        {
            ivsearch.setImageBitmap(bmpsearch);
            th.setCurrentTabByTag("browse");
        }
        else
        {
            ivhome.setImageBitmap(bmphome);
            th.setCurrentTabByTag("records");
        }*/
    }
    private void setsellproduct_tab() {
        // TODO Auto-generated method stub
        /*i=new Intent(getApplicationContext(),Sellproduct.class);
        //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        specs=th.newTabSpec("sellproduct");
        specs.setContent(i);
        specs.setIndicator("");
        th.addTab(specs);*/
    }
    private void setbrowse_tab() {
        // TODO Auto-generated method stub
       /* i=new Intent(getApplicationContext(),Browseproduct.class);
        //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("pref_key",LAST_SEARCH);
        i.putExtra("load_result", 1);
        i.putExtra("view_status", 0);
        specs=th.newTabSpec("browse");
        specs.setContent(i);
        specs.setIndicator("");
        th.addTab(specs);*/
    }

    private void setrecords_tab() {
        // TODO Auto-generated method stub
       /* i=new Intent(getApplicationContext(),Records.class);
        //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("user_id", user_id);*/

    }

    private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException(
                    getString(R.string.error_config, name));
        }
    }
    public boolean checkregistration()
    {   int t_status=0;
        try{
            t_status=jputil.getintpref(REG_STATUS, 0);
        }catch(Exception e)
        {

        }
        setupview();
        if(t_status<2)
        {
            checkNotNull(SERVER_URL, "SERVER_URL");
            checkNotNull(SENDER_ID, "SENDER_ID");
            GCMRegistrar.checkDevice(this);
            GCMRegistrar.checkManifest(this);
            reg_id = GCMRegistrar.getRegistrationId(this);
            //if(t_status==1) reg_id = GCMRegistrar.getRegistrationId(this);
            //else reg_id=jputil.getfrompref("reg_id");
            if (reg_id.equals("")) {
                // Automatically registers application on startup.
                progdia=ProgressDialog.show(this, "Checking registration..","Please wait and keep connection on...",true);
                GCMRegistrar.register(this, SENDER_ID);
            } else {
                jputil.setintpref(REG_STATUS, 1);
                jputil.settopref("reg_id", reg_id);
                // Device is already registered on GCM, check server.
                if (GCMRegistrar.isRegisteredOnServer(this)) {
                    // Skips registration.
                    admin_id=jputil.getfrompref("user_id");
                    jputil.setintpref(REG_STATUS, 2);
                    setupview();
                } else {
                    progdia=ProgressDialog.show(this, "Checking registration..","Please wait and keep connection on...",true);
                    final Context context = this;
                    mRegisterTask = new AsyncTask<Void, Void, Boolean>() {
                        @Override
                        protected Boolean doInBackground(Void... params) {
                            boolean registered =
                                    ServerUtilities.register(context, reg_id);
                            if (!registered) {
                                //GCMRegistrar.unregister(context);
                                return false;
                            }
                            return true;
                        }
                        @Override
                        protected void onPostExecute(Boolean result) {
                            //Toast.makeText(getApplicationContext(), "Dismiss ", Toast.LENGTH_LONG).show();
                            mRegisterTask = null;
                            if(result) setupview();
                        }
                    };
                    mRegisterTask.execute(null, null, null);
                }
            }
        }
        else
        {
            reg_id=jputil.getfrompref("reg_id");
            admin_id=jputil.getfrompref("user_id");
        }
        return true;
    }
    public boolean setpref(String key,String value)
    {
        SharedPreferences datacenter;
        datacenter = getSharedPreferences(filename, 0);
        SharedPreferences.Editor edit = datacenter.edit();
        edit.putString(key, value);
        edit.commit();
        return true;
    }

    public void checkappsettings()
    {
        SharedPreferences  datacenter = getSharedPreferences(filename, 0);
        if(datacenter.getBoolean("datacenterisnotsaved",true))
        {
            JSONObject jobj=new JSONObject();
            JSONArray jarr=new JSONArray();
            try {
                int len=catagory.length;
                for(int i=1;i<len;i++) jarr.put(catagory[i]);
                jobj.put("catagory", jarr);
                jobj.put("sender_id",SENDER_ID);
                jobj.put("server_url", SERVER_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            SharedPreferences.Editor edit=datacenter.edit();
            edit.putString("settings", jobj.toString());
            edit.putBoolean("datacenterisnotsaved",false);
            edit.commit();
            //Toast.makeText(getApplicationContext(), "APP SETTING SAVED:\n"+jobj.toString(), Toast.LENGTH_LONG).show();
        }
        else
        {
            try {
                JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
                JSONArray jarr=new JSONArray();
                jarr=jobj.getJSONArray("catagory");
                int len=jarr.length();
                catagory=new String[len+1];
                catagory[0]="Select catagory";
                for(int i=0;i<len;i++) catagory[i+1]=jarr.getString(i);
                //SENDER_ID=jobj.getString("sender_id");
                //SERVER_URL=jobj.getString("server_url");
                //Toast.makeText(getApplicationContext(), "Loaded from prefs:\n"SENDER_ID+"\n"+SERVER_URL, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    /*private final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };*/
    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.iv_tab1:
                setSelected(0);
                break;
            case R.id.iv_tab2:
                setSelected(1);
                break;
            case R.id.iv_tab3:
                setSelected(2);
                break;
            case R.id.iv_tab4:
                setSelected(3);
                break;
        }
    }
    int selectedposition=0;
    ImageView ivTab[]=new ImageView[4];
    Bitmap selectedBitmap[],deSelectedBitmap[];
    View indicator[]=new View[4];
    //int indicatorColor;//,deSelectTextColor;
    int selectedTabIcons[];
    int deSelectedTabIcons[];
    public void tabByCustom() {
        selectedTabIcons=tabIcons;
        deSelectedTabIcons=tabIcons;
        //deSelectTextColor=getApplicationContext().getResources().getColor(R.color.deselect_text_color);
        ivTab[0]=(ImageView)findViewById(R.id.iv_tab1);
        ivTab[0].setOnClickListener(this);
        ivTab[1]=(ImageView)findViewById(R.id.iv_tab2);
        ivTab[1].setOnClickListener(this);
        ivTab[2]=(ImageView)findViewById(R.id.iv_tab3);
        ivTab[2].setOnClickListener(this);
        ivTab[3]=(ImageView)findViewById(R.id.iv_tab4);
        ivTab[3].setOnClickListener(this);
        indicator[0]=(View)findViewById(R.id.indicator1);
        indicator[1]=(View)findViewById(R.id.indicator2);
        indicator[2]=(View)findViewById(R.id.indicator3);
        indicator[3]=(View)findViewById(R.id.indicator4);

        int length=selectedTabIcons.length;
        deSelectedBitmap=new Bitmap[length];
        selectedBitmap=new Bitmap[length];
        for(int i=0;i<length;i++)
        {
            deSelectedBitmap[i]= BitmapFactory.decodeResource(getResources(), deSelectedTabIcons[i]);
            selectedBitmap[i]= BitmapFactory.decodeResource(getResources(),selectedTabIcons[i]);
        }
        setSelected(selectedposition);
    }
    public void setSelected(int position)
    {
        selectedposition=position;
        for(int i=0;i<ivTab.length;i++)
        {
            ivTab[i].setImageBitmap(deSelectedBitmap[i]);
            indicator[i].setBackgroundColor(Color.TRANSPARENT);
        }
        ivTab[position].setImageBitmap(selectedBitmap[position]);
        indicator[position].setBackgroundColor(Color.WHITE);
        pager.setCurrentItem(position);
    }
    public void work_for_click(View v) {
        switch (v.getId()) {
            case R.id.layslist:
                show_product(SAVE_LIST,7);
                break;

            case R.id.laynote:
                show_product(NOTIFIED_PRODUCT,6);
                break;

            case R.id.laympro:
                show_product(ADMIN_PRODUCT,5);
                break;
            case R.id.laylast:
                startActivity(new Intent(getApplicationContext(), Selectoption.class));
                break;
            case R.id.layserv:
                Intent intent2=new Intent(getApplicationContext(), Browsemessage.class);
                startActivity(intent2);
                jputil.settopref("should_open_option"+"all_message",false);
                break;
            case R.id.layservice:
                Intent intent3=new Intent(getApplicationContext(),Service_messages.class);
                startActivity(intent3);
                jputil.settopref("should_open_option"+"service_message",false);
                break;
        }
    }
    public boolean show_product(String key,int view_status)
    {
        try{
            //send preference key to load data from preference..
            jputil.settopref("should_open_option"+key,false);
            Intent i=new Intent(getApplicationContext(),Browseresult.class);
            i.putExtra("pref_key", key);
            i.putExtra("load_result", 1);
            i.putExtra("view_status", view_status);
            startActivity(i);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    @Override
    public void onPageSelected(int position) {
        setTitle(Titles[position]);
        setSelected(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
