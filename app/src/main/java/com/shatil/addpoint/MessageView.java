package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.LAST_SEARCH;
import static com.shatil.addpoint.CommonUtilities.MESSAGE_VIEW_STATUS;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.admin_id;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MessageView extends RelativeLayout  implements OnTouchListener{
	JPUtilities jputil;
	//static Bitmap bmp_admin,bmp_user;
	static String product_id,catagory;
	long interval=0;
	ImageView ivicon,ivremove,ivSend;
	LinearLayout lay_total,lay_body;
	TextView lay_pad,lay_pad2;
	TextView tv_message_body,tv_time,tv_load,tvStaus;
	String message_body=" ";
	RatingBar rat;
	Context context;
	ImageView iv_back;
	JSONObject jobj=new JSONObject();
    JSONObject new_message=new JSONObject();
	public static MessageView inflate(ViewGroup parent) {
		 MessageView messageView = ( MessageView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_view, parent, false);
        return  messageView;
    }

    public MessageView(Context c) {
        this(c, null);
    }

    public MessageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MessageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(MESSAGE_VIEW_STATUS==0) LayoutInflater.from(context).inflate(R.layout.message_view_children, this, true);
        else if(MESSAGE_VIEW_STATUS==1) LayoutInflater.from(context).inflate(R.layout.message_view_children1, this, true);
        else LayoutInflater.from(context).inflate(R.layout.message_view_children2, this, true);
        new_message=load_new_messages();
        setupChildren(context);
        this.context=context;
    }

    public JSONObject load_new_messages()
    {
        JSONObject new_message;
        try {
            JPUtilities jputil;
            jputil = new JPUtilities(getContext());
            new_message=new JSONObject(jputil.getfrompref("got_new_message"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            new_message=new JSONObject();
        }
        Log.d(getClass().getSimpleName(),"New Messages loaded:"+new_message.toString());
        return new_message;
    }
    public void setupChildren(Context context)
    {
		tvStaus=(TextView)findViewById(R.id.tvstatus);
    	tv_time=(TextView)findViewById(R.id.tvtime);
    	tv_message_body=(TextView)findViewById(R.id.tvmessage_body);
    	lay_total=(LinearLayout)findViewById(R.id.laytotal);
    	lay_body=(LinearLayout)findViewById(R.id.lay_body);
    	if(MESSAGE_VIEW_STATUS==0)
    	{
	    	ivicon=(ImageView)findViewById(R.id.ivicon);
	    	ivremove=(ImageView)findViewById(R.id.ivremove);
	    	tv_load=(TextView)findViewById(R.id.tvload);
	    	tv_load.setOnTouchListener(this);
	    	lay_total.setOnTouchListener(this);
    	}
    	else if(MESSAGE_VIEW_STATUS==1)
    	{
    		rat=(RatingBar)findViewById(R.id.ratown);
	    	lay_total.setOnTouchListener(this);
    	}
    	else if(MESSAGE_VIEW_STATUS==2)
    	{
    		lay_pad=(TextView)findViewById(R.id.lay_pad);
    		lay_pad2=(TextView)findViewById(R.id.lay_pad2);
    	}
    	
    }
    public void setItem(MessageItem item) {
    try {
    	jobj=item.get_jsonobject();

    	if(MESSAGE_VIEW_STATUS==0)
    	{
    		ivicon.setImageBitmap(item.get_bitmap());
    		if(admin_id.equals(jobj.getString("user_id"))) ivremove.setVisibility(View.VISIBLE);
    		else ivremove.setVisibility(View.GONE);
    		if(item.is_ok(MESSAGE_VIEW_STATUS)) tv_load.setText("See product details");
    		else tv_load.setText("Load product details");
    		if(new_message.has(jobj.getString("_id")))
    		{
                Log.d(getClass().getSimpleName(),"New "+jobj.getString("_id"));
    			tv_message_body.setTextColor(Color.RED);
    		}
    		else tv_message_body.setTextColor(Color.BLACK);
			Log.d(getClass().getSimpleName(),"New Messages"+new_message);
			Log.d(getClass().getSimpleName(),"_id"+jobj.getString("_id"));

		}
    	if(MESSAGE_VIEW_STATUS==1)
    	{
    		JSONObject json=new JSONObject();
    		if(new_message.has(product_id)) json=new_message.getJSONObject(product_id);
    		if(json.has(jobj.getString("saver_id")))
    		{
    			tv_message_body.setTextColor(Color.RED);
    		}
    		else tv_message_body.setTextColor(Color.BLACK);
    		rat.setMax(100);
    		rat.setProgress(item.get_rating());
    	}
    	if(MESSAGE_VIEW_STATUS==2)
    	{
			tv_message_body.setTextColor(Color.BLACK);
    		if(item.get_receive()==0)
    		{
    			lay_body.setBackgroundResource(R.drawable.bac_send);
    			lay_pad.setVisibility(View.VISIBLE);
    			lay_pad2.setVisibility(View.GONE);
                tvStaus.setText("Sent");
    		}
    		else
    		{
    			lay_body.setBackgroundResource(R.drawable.bac_receive);
    			//lay_total.setBackgroundColor(col2);
    			lay_pad2.setVisibility(View.VISIBLE);
    			lay_pad.setVisibility(View.GONE);
                tvStaus.setText("Received");
    		}
    	}
    	message_body=item.get_body(MESSAGE_VIEW_STATUS);
    	tv_message_body.setText(message_body);
    	tv_time.setText(item.get_time());
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
	}
    }
	
	public void work_for_click(View v)
	{
		switch (v.getId()) {
		case R.id.lay_body:
			go_next_stage();
			break;
		case R.id.laytotal:
			go_next_stage();
			break;
		case R.id.tvload:
			JSONArray result=new JSONArray();
			result.put(jobj);
			Intent intent=new Intent(getContext(),Browseresult.class);
			intent.putExtra("pref_key",LAST_SEARCH);
			intent.putExtra("load_result", 0);
			intent.putExtra("intent_result", result.toString());
			intent.putExtra("view_status", 0);
			getContext().startActivity(intent);
			break;
		}
	}
	private void go_next_stage() {
		// TODO Auto-generated method stub
		try {
			Intent i=new Intent(getContext(), Browsemessage.class);
			//i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			if(MESSAGE_VIEW_STATUS==0&&!jobj.getString("user_id").equals(admin_id))
			{
					i.putExtra("_id",jobj.getString("_id"));
					i.putExtra("catagory", jobj.getString("catagory"));
					i.putExtra("saver_id", jobj.getString("user_id"));
					i.putExtra("status_come_from", 2);
					getContext().startActivity(i);
			}
			else if(MESSAGE_VIEW_STATUS==0)
			{
					i.putExtra("_id",jobj.getString("_id"));
					i.putExtra("status_come_from", 1);
					i.putExtra("catagory", jobj.getString("catagory"));
					getContext().startActivity(i);
			}
			else if(MESSAGE_VIEW_STATUS==1)
			{
				i.putExtra("_id",product_id);
				i.putExtra("catagory", catagory);
				i.putExtra("saver_id", jobj.getString("saver_id"));
				i.putExtra("status_come_from", 2);
				getContext().startActivity(i);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			else if(temp>2000&&MESSAGE_VIEW_STATUS==0)
			{
				show_simpledecission("Delete message!!", "Are you sure want to delete messages of this product?",get_drawable(0));
			}
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
	public void show_simpledecission(String title,String message,Drawable icon)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
		    new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        	//upload value to server & then set notification
					try {
						dbutil.delete_message_of_product(jobj.getString("_id"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
		        }
		    });
		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
			    new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) {
			            dialog.dismiss();
			        }
			    });
		alertDialog.setIcon(icon);
		alertDialog.show();
	}
	public Drawable get_drawable(int code) {
    	Bitmap bmp;
    	if(code==0) bmp=BitmapFactory.decodeResource(getResources(), R.drawable.warning);
    	else bmp=BitmapFactory.decodeResource(getResources(), R.drawable.error);
		int iconsize=30;
    	bmp=Bitmap.createScaledBitmap(bmp, iconsize, iconsize, false);
    	Drawable dr;
		dr=new BitmapDrawable(getResources(), bmp);
		return dr;
	}
}
