package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.NOTIFIED_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.SAVE_LIST;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Records extends FragmentActivity implements Dialogmessageview.NoticeDialogListener, OnTouchListener{
//view_status notification_list=11,notified_product=6,myproduct=5
	LinearLayout laympro,layslist,laynote,layserv,laylast,layservice;
	JPUtilities jputil;
	ImageView iv_note_product,iv_note_admin,iv_note_message,iv_note_service,iv_note_menu;
	TextView tv_mpro,tv_list,tv_note,tv_serv,tv_last,tv_service;
	long interval=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.records);
		laympro=(LinearLayout)findViewById(R.id.laympro);
		layslist=(LinearLayout)findViewById(R.id.layslist);
		laynote=(LinearLayout)findViewById(R.id.laynote);
		layserv=(LinearLayout)findViewById(R.id.layserv);
		laylast=(LinearLayout)findViewById(R.id.laylast);
		layservice=(LinearLayout)findViewById(R.id.layservice);
		iv_note_admin=(ImageView)findViewById(R.id.iv_note_admin);
		iv_note_product=(ImageView)findViewById(R.id.iv_note_product);
		iv_note_message=(ImageView)findViewById(R.id.iv_note_message);
		iv_note_service=(ImageView)findViewById(R.id.iv_note_service);
		iv_note_menu=(ImageView)findViewById(R.id.iv_note_last);
		laylast.setOnTouchListener(this);
		laympro.setOnTouchListener(this);
		layslist.setOnTouchListener(this);
		laynote.setOnTouchListener(this);
		layserv.setOnTouchListener(this);
		layservice.setOnTouchListener(this);
		tv_last=(TextView)findViewById(R.id.tvlast);
		tv_list=(TextView)findViewById(R.id.tvslist);
		tv_mpro=(TextView)findViewById(R.id.tvmpro);
		tv_note=(TextView)findViewById(R.id.tvnote);
		tv_serv=(TextView)findViewById(R.id.tvserv);
		tv_service=(TextView)findViewById(R.id.tvservice);
		Typeface tf=Typeface.createFromAsset(getAssets(), "uregular.ttf");
		tv_last.setTypeface(tf);
		tv_list.setTypeface(tf);
		tv_mpro.setTypeface(tf);
		tv_note.setTypeface(tf);
		tv_serv.setTypeface(tf);
		tv_service.setTypeface(tf);
		jputil=new JPUtilities(getApplicationContext());
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(150, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
	public void work_for_click(View v) {
		switch (v.getId()) {
		case R.id.layslist:
			show_product(SAVE_LIST,7);
			break;
			
		case R.id.laynote:
			show_product(NOTIFIED_PRODUCT,6);
		break;
		
		case R.id.laympro:
			show_product(ADMIN_PRODUCT,5);
			break;
		case R.id.laylast:
			startActivity(new Intent(getApplicationContext(), Selectoption.class));
			break;
		case R.id.layserv:
			Intent intent2=new Intent(getApplicationContext(), Browsemessage.class);
			startActivity(intent2);
			jputil.settopref("should_open_option"+"all_message",false);
			break;
		case R.id.layservice:
			Intent intent3=new Intent(getApplicationContext(),Service_messages.class);
			startActivity(intent3);
			jputil.settopref("should_open_option"+"service_message",false);
			break;
		}
	}
	@Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
		String st=dialog.getTag();
		if(st.equals("exit_decission"))
		{
			finish();
		}
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    	String st=dialog.getTag();
		if(st.equals("notification_decission"))
		{
			
		}
    }
/*
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		cdia.set_args("Are you sure want to exit ?", 0);
		cdia.show(getSupportFragmentManager(), "exit_decission");
	}
*/
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!jputil.getfrompref("should_open_option"+NOTIFIED_PRODUCT,false))
		{
			iv_note_product.setVisibility(View.GONE);
		}
		else iv_note_product.setVisibility(View.VISIBLE);
		
		if(!jputil.getfrompref("should_open_option"+ADMIN_PRODUCT,false))
		{
			iv_note_admin.setVisibility(View.GONE);
		}
		else iv_note_admin.setVisibility(View.VISIBLE);
		
		if(!jputil.getfrompref("should_open_option"+"all_message",false))
		{
			iv_note_message.setVisibility(View.GONE);
		}
		else iv_note_message.setVisibility(View.VISIBLE);
		
		if(!jputil.getfrompref("should_open_option"+"service_message",false))
		{
			iv_note_service.setVisibility(View.GONE);
		}
		else iv_note_service.setVisibility(View.VISIBLE);
		if(!jputil.getfrompref("mail_found", false)||!jputil.getfrompref("pin_found",false)) 
		{
			iv_note_menu.setVisibility(View.VISIBLE);
		}
		else iv_note_menu.setVisibility(View.GONE);
	}
	@Override
		protected void onPause() {
			// TODO Auto-generated method stub
		close_keyboard(tv_mpro);	
		super.onPause();
		}
	public void close_keyboard(View view)
    {
    	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

	public boolean show_product(String key,int view_status)
	{
		try{
			//send preference key to load data from preference..
			jputil.settopref("should_open_option"+key,false);
			Intent i=new Intent(getApplicationContext(),Browseresult.class);
			i.putExtra("pref_key", key);
			i.putExtra("load_result", 1);
			i.putExtra("view_status", view_status);
			startActivity(i);
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}
	public void arraysearch(String jarrkey)
	{
		try {
			JSONObject statement=new JSONObject(jputil.getarrstate(jputil.getjarrfrompref(jarrkey),"_id"));
			JSONObject jsonobj=new JSONObject();
			jsonobj.put("catagory",jputil.getjarray(jputil.getcatagory()));
			jsonobj.put("statement", statement);	
			jputil.search(jsonobj.toString(),getApplicationContext(),this,false);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void searchbyfield(String key)
	{
		try {
			JSONObject statement=new JSONObject();
			statement.put(key,jputil.getfrompref(key));
			JSONObject jsonobj=new JSONObject();
			jsonobj.put("catagory",jputil.getjarray(jputil.getcatagory()));
			jsonobj.put("statement", statement);
			jputil.search(jsonobj.toString(),getApplicationContext(),this,false);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void searchbyfield(String key,String catagory)
	{
		try {
			JSONObject statement=new JSONObject();
			statement.put(key,jputil.getfrompref(key));
			JSONObject jsonobj=new JSONObject();
			jsonobj.put("catagory",catagory);
			jsonobj.put("statement", statement);
			jputil.search(jsonobj.toString(),getApplicationContext(),this,false);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}