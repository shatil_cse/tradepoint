/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.ADMIN_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.reg_id;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main UI for the demo app.
 */
public class Sellproduct extends FragmentActivity implements OnClickListener, OnDismissListener,Dialogmessageview.NoticeDialogListener, OnTouchListener{
	String catagory[];
	long interval=0;
	int Max,tmax,vMax;
	TextView tv_head,tv_shop_name;
	EditText etshop_name,etname,etprice,etproduct,etprocess,etaddress;
	RadioButton rbnew,rbused,rbfixed,rbvar,rbpersonal,rbbusiness,rbhome,rbnothome,rbdiscuss;
    int type_use=0,type_price=0,type_delivery=0,type_product=0;
	Spinner spcatagory;
    ImageView btbrowse,bttakepic;
    ImageView ivupload,ivphoto,ivBack;
    int selecteditem=0,idcount=0;
    String name,price,product,process,address,shop_name="No name found";
    String encoded_photo_string="";
    boolean photo_uploaded=false;
    Bitmap bmp;
    ScrollView sv;
    Uploaddownload updown;
    Typeface tf2,tf3;
    private static final int SELECT_PHOTO = 100,cameradata=0;
    ProgressDialog pdia;
    JPUtilities jputil;
    boolean keyboard=false;
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		tf3=Typeface.createFromAsset(getAssets(),"uitalic.ttf");
		tf2=Typeface.createFromAsset(getAssets(),"uregular.ttf");
		jputil=new JPUtilities(getApplicationContext());
		appsettings();
		setviews();
	}
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
		String st=dialog.getTag();
		if(st.equals("exit_decission"))
		{
			finish();
		}
		else if(st.equals("upload_decission"))
		{
			try{
				setJSONvalues();
			}catch(Exception e)
			{
				//Toast.makeText(getApplicationContext(), "Operation Failed:"+e.toString(), Toast.LENGTH_LONG).show();
			}
		}
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    	String st=dialog.getTag();
		if(st.equals("notification_decission"))
		{
			
		}
    }

    public void appsettings()
    {
    	SharedPreferences  datacenter = getSharedPreferences(filename, 0);
    	try {
			JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
			JSONArray jarr=new JSONArray();
			jarr=jobj.getJSONArray("catagory");
			int len=jarr.length();
			catagory=new String[len+1];
			catagory[0]="Select catagory";
			for(int i=0;i<len;i++) catagory[i+1]=jarr.getString(i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    private void setviews() {
		// TODO Auto-generated method stub
    	tv_shop_name=(TextView)findViewById(R.id.tv_shopname);
    	etshop_name=(EditText)findViewById(R.id.et_shopname);
    	tv_head=(TextView)findViewById(R.id.tv_headline);
    	tv_head.setTypeface(tf3);
    	etprice=(EditText)findViewById(R.id.etprice);
        etproduct=(EditText)findViewById(R.id.etproduct);
        etname=(EditText)findViewById(R.id.etname);
        etprocess=(EditText)findViewById(R.id.etprocess);
        etaddress=(EditText)findViewById(R.id.etaddress);
        etshop_name.setTypeface(tf2);
        tv_shop_name.setTypeface(tf2);
        etprice.setTypeface(tf2);
        etproduct.setTypeface(tf2);
        etprocess.setTypeface(tf2);
        etname.setTypeface(tf2);
        etaddress.setTypeface(tf2);
        spcatagory=(Spinner)findViewById(R.id.spcatagory);
        ArrayAdapter<String> spad=new ArrayAdapter<String>(Sellproduct.this,android.R.layout.simple_spinner_dropdown_item,catagory);
        spcatagory.setAdapter(spad);
        rbnew=(RadioButton)findViewById(R.id.rbnew);
        rbused=(RadioButton)findViewById(R.id.rbused);
        rbpersonal=(RadioButton)findViewById(R.id.rbpersonal);
        rbbusiness=(RadioButton)findViewById(R.id.rbbusiness);
        rbhome=(RadioButton)findViewById(R.id.rbhome);
        rbnothome=(RadioButton)findViewById(R.id.rbnothome);
        rbdiscuss=(RadioButton)findViewById(R.id.rbdiscuss);
        rbfixed=(RadioButton)findViewById(R.id.rbfixed);
        rbvar=(RadioButton)findViewById(R.id.rbvariable);
        rbnew.setTypeface(tf2);
        rbused.setTypeface(tf2);
        rbpersonal.setTypeface(tf2);
        rbbusiness.setTypeface(tf2);
        rbhome.setTypeface(tf2);
        rbnothome.setTypeface(tf2);
        rbdiscuss.setTypeface(tf2);
        rbfixed.setTypeface(tf2);
        rbvar.setTypeface(tf2);
        //Toast.makeText(getApplicationContext(), "Tset", Toast.LENGTH_LONG).show();
        ivupload=(ImageView)findViewById(R.id.ivupload);
        //ivupload.setBackgroundColor(Color.TRANSPARENT);
        btbrowse=(ImageView)findViewById(R.id.btbrowse);
        bttakepic=(ImageView)findViewById(R.id.btcamera);
        ivphoto=(ImageView)findViewById(R.id.ivphoto);
		ivBack=(ImageView)findViewById(R.id.ivback);
        bmp=BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.productsadd);
        Max=200;
    	Bitmap tbmp=getscaledbitmap(bmp,Max);
        ivphoto.setImageBitmap(tbmp);
        ivphoto.setOnClickListener(this);
        
        rbnew.setOnClickListener(this);
        rbused.setOnClickListener(this);
        rbpersonal.setOnClickListener(this);
        rbbusiness.setOnClickListener(this);
        rbhome.setOnClickListener(this);
        rbnothome.setOnClickListener(this);
        rbdiscuss.setOnClickListener(this);
        rbfixed.setOnClickListener(this);
        rbvar.setOnClickListener(this);
        btbrowse.setOnTouchListener(this);
        bttakepic.setOnTouchListener(this);
        ivupload.setOnTouchListener(this);
		ivBack.setOnClickListener(this);
        spcatagory.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Tset"+arg2, Toast.LENGTH_LONG).show();		
				selecteditem=arg2;
				close_keyboard(spcatagory);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Nothings", Toast.LENGTH_LONG).show(); 
				close_keyboard(spcatagory);
			}
		});
        sv=(ScrollView)findViewById(R.id.sv_total);
		sv.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction()==MotionEvent.ACTION_DOWN)
				{
					close_keyboard(v);
				}
				return false;
			}
		});
	}
    
    public void close_keyboard(View view)
    {
    	//Configuration config = getResources().getConfiguration();

        //Toast.makeText(getApplicationContext(), "Comed"+config.keyboardHidden+""+Configuration.KEYBOARDHIDDEN_NO, Toast.LENGTH_LONG).show();
        // Dont allow the default keyboard to show up
        //if (config.keyboardHidden == Configuration.KEYBOARDHIDDEN_NO) {
            //Toast.makeText(getApplicationContext(), "Yes", Toast.LENGTH_LONG).show();
        	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        //}
    }
    public int fitmaxsize(float theight,float twidth,int viewhight,int viewwide)
	{
        float tmax=viewhight;
        vMax=viewwide;
        twidth=(twidth*viewhight)/theight;
        if(twidth>viewwide)
        {
        	tmax=viewwide;
        	vMax=viewhight;
        	
        }
        int tMax=(int)tmax;
        return tMax;
	}
    private Bitmap getscaledbitmap(Bitmap tbmp,int tMax) {
		// TODO Auto-generated method stub
		int theight=tbmp.getHeight();
        int twidth=tbmp.getWidth();
        if(theight>twidth)
        {
        	twidth=tMax*twidth;
        	twidth=twidth/theight;
        	theight=tMax;
        }
        else
        {
        	theight=tMax*theight;
        	theight=theight/twidth;
        	twidth=tMax;
        }
		return Bitmap.createScaledBitmap(tbmp, twidth, theight, true);
	}
	private void setJSONvalues() {
		// TODO Auto-generated method stub
		product=" ";
		process=" ";
		if(selecteditem==0)
		{
			Toast.makeText(getApplicationContext(), "Select a catagory..", Toast.LENGTH_LONG).show();
			return;
		}
		name=etname.getText().toString();
		if(name.length()<2)
		{
			Toast.makeText(getApplicationContext(), "Enter a valid name..", Toast.LENGTH_LONG).show();
			return;
		}
		try{
			price=etprice.getText().toString();
			Double.parseDouble(price);
		}catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Enter a valid price..", Toast.LENGTH_LONG).show();
			price=null;
			return;
		}
		address=etaddress.getText().toString();
		if(address.length()<2)
		{
			Toast.makeText(getApplicationContext(), "Enter a valid address..", Toast.LENGTH_LONG).show();
			return;
		}
		shop_name=etshop_name.getText().toString();
		if(type_product==1&&shop_name.length()<1)
		{
			Toast.makeText(getApplicationContext(), "Enter a company or shop name..", Toast.LENGTH_LONG).show();
			return;
		}
		if(!photo_uploaded)
		{
			//Toast.makeText(getApplicationContext(), "Give a photo of your product..", Toast.LENGTH_LONG).show();
			//return;
		}
		product=etproduct.getText().toString();
		process=etprocess.getText().toString();
		//Input taken finished....................................
		try {
			pdia=ProgressDialog.show(this, "Uploading to server", "Please wait..");
			pdia.setOnDismissListener(this);
			String objstring=getjsonstring();
			if(objstring.length()<2) Toast.makeText(getApplicationContext(), "Enter valid informations", Toast.LENGTH_LONG).show();
			else updown=new Uploaddownload(getApplicationContext(), "/databasesetting", objstring, pdia);
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Enter valid informations", Toast.LENGTH_LONG).show();
		}
	}
	public String getjsonstring()
	{
		JSONObject objresult=new JSONObject();
		JSONObject objres1=new JSONObject();
		JSONObject objres2 =new JSONObject();
		try {
			name=name.toLowerCase();
			String _id=getproduct_id();
			if(type_product==1) objres1.put("shop_name", shop_name);
			else objres1.put("shop_name", "No shop name");
			objres1.put("_id", _id);
			objres1.put("user_id", admin_id);
			objres1.put("name", name);
			objres1.put("price",price);
			objres1.put("product", product);
			objres1.put("process", process);
			objres1.put("address", address);
			objres1.put("type_price",type_price);
			objres1.put("type_use", type_use);
			objres1.put("type_product",type_product);
			objres1.put("type_delivery", type_delivery);
			objres1.put("catagory", catagory[selecteditem]);
			JSONArray tags=jputil.get_tagname(name,address,shop_name,type_product);
            if(tags.length()<2) return " ";
            String tagStr=name.trim()+" "+address.trim();
            if(shop_name!=null&&shop_name.length()>0) tagStr+=" "+shop_name;
			if(product!=null&&product.length()>0)
            {
                tagStr+=" "+product;
            }
            //tags.put(tagStr);
			objres1.put("primary_tag",tagStr);
            objres1.put("tag_name", tags);
			objres1.put("VERSION_NUMBER", 1);
			encoded_photo_string=encodephototostring();
			objres2.put("photo",encoded_photo_string);
			objres2.put("_id", _id);
			objres2.put("VERSION_NUMBER", 1);
			objresult.put("catagory", catagory[selecteditem]);
			objresult.put("searchdet", objres1);
			objresult.put("otherdet", objres2);
			objres1.put("user_id", admin_id);
			objresult.put("reg_id", reg_id);
			objresult.put("_id", _id);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), "Internal Error happned", Toast.LENGTH_LONG).show();
		}
		return objresult.toString();
	}

	private String getproduct_id() {
		// TODO Auto-generated method stub
		return (String.valueOf(System.currentTimeMillis())+admin_id+String.valueOf(idcount));
	}
	
	public String encodephototostring() {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Bitmap bmp=getscaledbitmap(this.bmp, 200);
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte [] bytearr = baos.toByteArray();
        return Base64.encodeToString(bytearr, 0);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		work_for_click(v);
	}
 	public void change_radiobuttons(RadioButton rb0,RadioButton rb1,RadioButton rb2,int rbselect)
 	{
 		rb0.setChecked(false);
 		rb1.setChecked(false);
 		rb2.setChecked(false);
 		if(rbselect==0) rb0.setChecked(true);
 		else if(rbselect==1) rb1.setChecked(true);
 		else rb2.setChecked(true);
 	}
 	public void change_radiobuttons(RadioButton rb0,RadioButton rb1,int rbselect)
 	{
 		rb0.setChecked(false);
 		rb1.setChecked(false);
 		if(rbselect==0) rb0.setChecked(true);
 		else rb1.setChecked(true);
 	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) { 
	    case SELECT_PHOTO:
	        if(resultCode == RESULT_OK){  
	        	try{
		            Uri selectedImage = data.getData();
		            InputStream imageStream = getContentResolver().openInputStream(selectedImage);
		            bmp = BitmapFactory.decodeStream(imageStream);
		            bmp=getscaledbitmap(bmp,Max);
		            ivphoto.setImageBitmap(bmp);
		            photo_uploaded=true;
	        	}catch(Exception e)
	        	{
	        		//Toast.makeText(getApplicationContext(), "Error in loading photo:", Toast.LENGTH_LONG).show();
	        	}
	        }
	        break;
	    case cameradata:
	    	if(resultCode==RESULT_OK)
			{
				//camera...
				Bundle extras=data.getExtras();
				bmp=(Bitmap) extras.get("data");
				bmp=getscaledbitmap(bmp,Max);
				ivphoto.setImageBitmap(bmp);
				photo_uploaded=true;
			}
	    	break;
	    }
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stubToast.makeText(getApplicationContext(), "Code"+updown.get_status_int()+"\n"+updown.get_status_string(), Toast.LENGTH_LONG).show();
		if(updown.get_status_int()==100)
		{
			String result=updown.getresult();
			try {
				JSONArray jarr=new JSONArray(result);
				JSONObject jobject=jarr.getJSONObject(0);
				jputil.savecapedmessage(ADMIN_PRODUCT,jobject, false);
				jputil.add_photo(jobject.getString("_id"), encoded_photo_string,0);
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args(updown.get_status_string(),101,"Successfull");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Format error happned",201,"Error happned");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
				e.printStackTrace();
			}
		}
		else 
		{
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			cdia.set_args(updown.get_status_string(),201,"Error happned");
			cdia.show(getSupportFragmentManager(), "Uploading_result");
		}
		
		//else  Toast.makeText(getApplicationContext(), "Failed to Upload:"+result, Toast.LENGTH_LONG).show();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			else if(temp>20)
			{
				Toast.makeText(getApplicationContext(),v.getTag().toString(), Toast.LENGTH_LONG).show();
			}
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
	public void work_for_click(View v) {
		switch (v.getId()) {
		case R.id.btbrowse:
			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, SELECT_PHOTO);
			break;
		case R.id.btcamera:
			//camera...
			Intent i=new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(i,cameradata);
			break;
		case R.id.ivupload:
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			cdia.set_args("Are sure want to upload ?", 0);
			cdia.show(getSupportFragmentManager(), "upload_decission");
			break;
		case R.id.rbnew:
			type_use=0;
			change_radiobuttons(rbnew, rbused,type_use);
			break;
		case R.id.rbused:
			type_use=1;
			change_radiobuttons(rbnew, rbused,type_use);
			break;
		case R.id.rbfixed:
			type_price=0;
			change_radiobuttons(rbfixed, rbvar,type_price);
			break;
		case R.id.rbvariable:
			type_price=1;
			change_radiobuttons(rbfixed, rbvar,type_price);
			break;
		case R.id.rbpersonal:
			type_product=0;
			change_radiobuttons(rbpersonal, rbbusiness,type_product);
			etshop_name.setVisibility(View.GONE);
			tv_shop_name.setVisibility(View.GONE);
			break;
		case R.id.rbbusiness:
			type_product=1;
			change_radiobuttons(rbpersonal, rbbusiness,type_product);
			etshop_name.setVisibility(View.VISIBLE);
			tv_shop_name.setVisibility(View.VISIBLE);
			break;
		case R.id.rbdiscuss:
			type_delivery=0;
			change_radiobuttons(rbdiscuss, rbhome,rbnothome,type_delivery);
			break;
		case R.id.rbhome:
			type_delivery=1;
			change_radiobuttons(rbdiscuss, rbhome,rbnothome,type_delivery);
			break;
		case R.id.rbnothome:
			type_delivery=2;
			change_radiobuttons(rbdiscuss, rbhome,rbnothome,type_delivery);
			break;
		case R.id.ivphoto:
			//work here to show the image......................
			if(photo_uploaded)
			{
				jputil.settopref("load_temp_photo", encodephototostring());
				Intent temp_intent=new Intent(getApplicationContext(),Showphoto.class);
				temp_intent.putExtra("from", 1);
				startActivity(temp_intent);
			}
			break;
			case R.id.ivback:
				finish();
				break;
		}
		close_keyboard(v);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		close_keyboard(spcatagory);
		super.onPause();
	}
}