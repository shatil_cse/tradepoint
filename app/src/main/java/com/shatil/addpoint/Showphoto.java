package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.filename;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

public class Showphoto extends Activity implements OnTouchListener {
	int Max,vMax;
	ImageView ivphoto,ivback;
	TextView ivsave;
	String catagory="",_id="",name="";
	Bitmap bmp,tbmp;
	byte[] bytebmp;
	String imagepath = "imagepath";
	ProgressDialog pdia;
	JSONArray jarr,cat;
	SharedPreferences datacenter;
	ZoomControls zc;
	int from=0;
	long count=1,interval=0;
	String photo_name="no-name";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showphoto);
		bmp=BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.add_product);
		from=getIntent().getIntExtra("from", 0);
		datacenter=getSharedPreferences(filename, 0);
		if(from==0)
		{
			catagory=getIntent().getStringExtra("catagory");
			name=getIntent().getStringExtra("name");
			_id=getIntent().getStringExtra("_id");
		}
		setdesign();
	}
	
	public void setdesign()
	{
		zc=(ZoomControls)findViewById(R.id.z_control);
		ivback=(ImageView)findViewById(R.id.ivback);
    	ivsave=(TextView)findViewById(R.id.ivsave);
		ivphoto=(ImageView)findViewById(R.id.ivphoto);
		ivback.setOnTouchListener(this);
		ivsave.setOnTouchListener(this);
		if(from==0)
		{
			try {
				DButilities dbutil=new DButilities(getApplicationContext());
				dbutil.opendbhelper();
				String strphoto=dbutil.get_photo_string(_id);
				dbutil.close();
				if(!strphoto.startsWith("no_photo_found"))
				{
					bmp=decodestringtophoto(strphoto);
				}
				else Toast.makeText(getApplicationContext(), strphoto, Toast.LENGTH_LONG).show();
				ivphoto.setImageBitmap(getscaledbitmap(bmp, Max));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			}
		}
		else
		{
			ivsave.setVisibility(View.GONE);
			String str=datacenter.getString("load_temp_photo", "");
			bmp=decodestringtophoto(str);
			ivphoto.setImageBitmap(getscaledbitmap(bmp, Max));
		}
		zc.setOnZoomInClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Max<vMax-128)
				{
					Max = Max + 50;
					ivphoto.setImageBitmap(getscaledbitmap(bmp,Max));
				}
			}
		});
		zc.setOnZoomOutClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Max > 100) {
					Max = Max - 50;
					ivphoto.setImageBitmap(getscaledbitmap(bmp, Max));
				}	
			}
		});
	}
	public boolean remove_photo(){
		boolean flag=false;
		try{
		String strphoto=datacenter.getString("saved_photos",new JSONObject().toString());
		JSONObject j_saved_photos=new JSONObject(strphoto);
		if(j_saved_photos.has(_id)) {
			j_saved_photos.remove(_id);
			flag=true;
		}
		//saving the stored data........................
		Editor edit=datacenter.edit();
		edit.putString("saved_photos", j_saved_photos.toString());
		edit.commit();
		return flag;
		}catch(Exception e)
		{
			return false;
		}
	}
	
	public Bitmap decodestringtophoto(String tstr)
	{
		bytebmp=Base64.decode(tstr, 0);
		bmp=BitmapFactory.decodeByteArray(bytebmp, 0, bytebmp.length);
        Max=fitmaxsize(bmp.getHeight(),bmp.getWidth(), getWindowManager().getDefaultDisplay().getHeight(), getWindowManager().getDefaultDisplay().getWidth());
		return bmp;
	}
    public int fitmaxsize(float theight,float twidth,int viewhight,int viewwide)
	{
        float tmax=viewhight;
        vMax=viewwide;
        twidth=(twidth*viewhight)/theight;
        if(twidth>viewwide)
        {
        	tmax=viewwide;
        	vMax=viewhight;
        }
        int tMax=(int)tmax;
        return tMax;
	}
    private Bitmap getscaledbitmap(Bitmap tbmp,int tMax) {
		// TODO Auto-generated method stub
		int theight=tbmp.getHeight();
        int twidth=tbmp.getWidth();
        if(theight>twidth)
        {
        	twidth=tMax*twidth;
        	twidth=twidth/theight;
        	theight=tMax;
        }
        else
        {
        	theight=tMax*theight;
        	theight=theight/twidth;
        	twidth=tMax;
        }
		return Bitmap.createScaledBitmap(tbmp, twidth, theight, true);
	}
	public class Savebitmap extends AsyncTask<String, Integer, String> {
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {

				File f=new File(Environment.getExternalStorageDirectory()+"/Trade_Point/photos");
				f.mkdirs();
				FileOutputStream out = new FileOutputStream(
                        Environment.getExternalStorageDirectory()
                                + "/Trade_Point/photos/"+photo_name);
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
				out.close();
                return "success";

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "failed1:"+e.toString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "failed2:"+e.toString();
			}

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pdia.dismiss();
			if (result.startsWith("failed")) {
				Toast.makeText(getApplicationContext(), "Error happned photo not saved",
						Toast.LENGTH_LONG).show();
			}
			else
			{
				Toast.makeText(getApplicationContext(), "Photo saved as "+photo_name,
						Toast.LENGTH_LONG).show();
				Editor edit=datacenter.edit();
				edit.putLong("save_photo_counter", count+1);
				edit.commit();
			}
		}
	}

    public void work_for_click(View v)
    {
    	switch (v.getId()) {
		case R.id.ivsave:
			count=datacenter.getLong("save_photo_counter", 1);
			if(count>20000) count=1;
			photo_name=catagory+"-"+name+count+".png";
			pdia=ProgressDialog.show(this, "Saving Photo..", "Please Wait...");
			new Savebitmap().execute("success");
			break;
			
		case R.id.ivback:
			try{
				remove_photo();
			}catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(getApplicationContext(), "Error rmoving:"+e.toString(),Toast.LENGTH_LONG).show();
			}
			finish();
			break;
		}
    }
    @Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
}