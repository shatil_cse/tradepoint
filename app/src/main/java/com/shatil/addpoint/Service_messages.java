package com.shatil.addpoint;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class Service_messages extends ListActivity{

	String menuitem[];
	JPUtilities jputil;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		jputil=new JPUtilities(getApplicationContext());
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		jputil.settopref("service_message_opened",false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		jputil.settopref("should_open_option"+"service_message",false);
		menuitem=jputil.getcapedstarr("service_message");
		ArrayAdapter <String> ap=new ArrayAdapter<String>(getApplicationContext(),R.layout.wrap_textview, menuitem);
		setListAdapter(ap);
		jputil.settopref("service_message_opened",true);
		super.onResume();
	}
	
}
