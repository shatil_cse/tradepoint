package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.SAVE_LIST;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

public class JPUtilities {

	Context context;
	SharedPreferences  datacenter;
	String catagory[],server_url,sender_id;
	int CAPED_SIZE;
	public JPUtilities(Context context)
	{
		this.context=context;
		this.CAPED_SIZE=900;
		datacenter= context.getSharedPreferences(filename, 0);
		loadprefs();
	}
	public JPUtilities(Context context,int capped_size)
	{
		this.context=context;
		this.CAPED_SIZE=capped_size;
		datacenter= context.getSharedPreferences(filename, 0);
		loadprefs();
	}
	public void loadprefs()
	{
		try {
			JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
			server_url=jobj.getString("server_url");
			sender_id=jobj.getString("sender_id");
			JSONArray jarr=new JSONArray();
			jarr=jobj.getJSONArray("catagory");
			int len=jarr.length();
			catagory=new String[len+1];
			catagory[0]="SELECT CATAGORY";
			for(int i=0;i<len;i++) catagory[i+1]=jarr.getString(i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Prefs key for JSONArray returns string array....
	public String[] getcapedstarr(String key)
	{
		try {
			JSONArray jarr=getcapedjarr(key);
			int len=jarr.length();
			String message_list[]=new String[len];
			for(int i=0;i<len;i++) message_list[i]=jarr.getString(i);
			return message_list;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Toast.makeText(context, "CAPPED ERROR:"+e.toString(), Toast.LENGTH_LONG).show();
			return null;
		}
	}
	//json array contains JSONObject.........
	//notification statement..
	public JSONArray getcapedjarr(String key)
	{
		try {
			return new JSONArray(datacenter.getString(key,new JSONArray().toString()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Toast.makeText(context, "CAPPED ERROR:"+e.toString(), Toast.LENGTH_LONG).show();
			return new JSONArray();
		}
	}
	public String[] getcatagory()
	{
		return catagory;
	}
	public String getserver_url()
	{
		return server_url;
	}
	public String getsender_id()
	{
		return sender_id;
	}
	public JSONArray getjarrfrompref(String key)
	{
		try {
			return new JSONArray(datacenter.getString(key,new JSONArray().toString()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return new JSONArray();
		}
	}
	
	public String getfrompref(String key)
	{
		return datacenter.getString(key, "");
	}
	public boolean getfrompref(String key,boolean defaultvalue)
	{
		return datacenter.getBoolean(key, defaultvalue);
	}
	public long getfrompref(String key,long defaultvalue)
	{
		return datacenter.getLong(key, defaultvalue);
	}
	public int getintpref(String key,int defaultvalue)
	{
		return datacenter.getInt(key, defaultvalue);
	}
	public JSONArray getjarray(String array[])
	{
		JSONArray jarr=new JSONArray();
		int len=array.length;
		for(int i=0;i<len;i++)
		{
			jarr.put(array[i]);
		}
		return jarr;
	}
	
	public String[] getstarr(JSONArray jarr) throws JSONException
	{
		int len=jarr.length();
		String st[]=new String[len];
		for(int i=0;i<len;i++) st[i]=jarr.getString(i);
		return st;
	}
	
	public String getarrstate(String array[],String field)
	{
		return getarrstate(getjarray(array),field);
	}
	
	public String getarrstate(JSONArray jarr,String field)
	{
		try {
			JSONObject jobj1=new JSONObject();
			jobj1.put("$in", jarr);
			JSONObject jobj2=new JSONObject();
			jobj2.put(field, jobj1);
			return jobj2.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return "{}";
		}
	}
	
	public String get_notification_state(JSONArray jarr)
	{
		try {
			JSONObject jobj1=new JSONObject();
			JSONObject cmptime=new JSONObject();
			cmptime.put("$gt", datacenter.getLong("notification_time", 0));
			jobj1.put("curtime", cmptime);
			jobj1.put("$or", jarr);
			JSONObject job=new JSONObject();
			job.put("statement",jobj1);
			job.put("catagory", new JSONObject());
			return job.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return "{}";
		}
	}
	public String get_nin_saerch_staring(String name,boolean exactmatch)
	{
		try{
			//making space separated..
			String st=name.replace(',',' ')+" ";
			if(exactmatch) return st.trim();
			//spilting the all keys...
			String tags[]=st.split("\\s++");
			int len=tags.length;
			//generate nearest string for each key and to a final string with previous keys...
			String finalstring="";
			for(int i=0;i<len;i++)
			{
				tags[i]=get_middle_characters_replaced(tags[i]);
				finalstring=finalstring+" "+tags[i]+" "+get_formated_string(tags[i]);
				finalstring=finalstring.trim();
			}
			return finalstring.trim();
		}catch(Exception e)
		{
			return " ";
		}
	}

	public String get_in_saerch_staring(String name)
	{
		try{
            name=name.replaceAll("\\.*[^a-zA-Z0-9_]",".*");
            return name+".*";
		}catch(Exception e)
		{
			return " ";
		}
	}
    public JSONArray get_in_saerch_array(String name)
    {
        JSONArray jsonArray=new JSONArray();
        try{
			name=name.replace("[^a-zA-Z0-9]",".*");
            return jsonArray.put(name);
        }catch(Exception e)
        {
            return jsonArray;
        }
    }
	public String get_saerch_staring(String name,boolean exactmatch)
	{
		try{
		//making space separated..
		String st=name.replace(',',' ')+" ";
		if(exactmatch) return st.trim();
		//spilting the all keys...
		String tags[]=st.split("\\s++");
		int len=tags.length;
		//generate nearest string for each key and to a final string with previous keys...
		String finalstring="";
		for(int i=0;i<len;i++)
		{
			tags[i]=get_middle_characters_replaced(tags[i]);
			finalstring=finalstring+" "+tags[i]+" "+get_formated_string(tags[i]);
			finalstring=finalstring.trim();
		}
		return finalstring.trim();
		}catch(Exception e)
		{
			return " ";
		}
	}

	public String get_formated_string(String name)
	{
		try{
		String name1=name.trim();
		int len=name1.length();
		char []carr=new char[len];
		int j=0;
		carr[j++]=name1.charAt(0);
		len--;
		for(int i=1;i<len;i++)
		{
			if(!(name1.charAt(i)=='a'||name1.charAt(i)=='e'||name1.charAt(i)=='i'||name1.charAt(i)=='o'||name1.charAt(i)=='u'))
			{
				if(name1.charAt(i)!=name1.charAt(i-1)) carr[j++]=name1.charAt(i);
			}
		}
		carr[j++]=name1.charAt(len);
		String st=new String(carr,0,j);
		if(st.length()<2) return " ";
		return st;
		}catch(Exception e)
		{
			return " ";
		}
	}
	public JSONArray get_tagname(String name,String address,String shop_name,int shop_flag)
	{
		try{
		//making space separated..
		String st=name.replace(',',' ')+" ";
		address=address.replace(',',' ');
		st=st+" "+address;
		if(shop_flag==1)
		{
			shop_name=shop_name.replace(',',' ');
			st=st+" "+shop_name;
		}
		//spilting the all keys...
		String tags[]=st.split("\\s++");
		int len=tags.length;
		//generate nearest string for each key and to a final string with previous keys...
		
		String finalstring="";
		for(int i=0;i<len;i++)
		{
			tags[i]=get_middle_characters_replaced(tags[i]);
			finalstring=finalstring+" "+tags[i]+" "+get_formated_string(tags[i]);
			finalstring=finalstring.trim();
		}
		finalstring=finalstring.trim();
		//spilting all the keywords....... 
		String finaltags[]=finalstring.split("\\s++");
		return getjarray(finaltags);
		}catch(Exception e)
		{
			return new JSONArray();
		}
	}
	public String get_middle_characters_replaced(String name)
	{
		try{
		int len=name.length()-1;
		if(len<1) return name;
		String sub=name.substring(len,len+1);
		name=name.substring(0,len);
		name=name.replace('.', '_');
		name=name.replace('-', '_');
		name=name.concat(sub);
		return name;
		}catch(Exception e)
		{
			return " ";
		}
	}
	public boolean savecapedmessage(String key,String singlevalue,boolean remove,boolean checkrep)
	{
		return savecapedmessage(key, singlevalue, remove,checkrep,CAPED_SIZE);
	}
	//single string insert method at the top of capped JSONArray....
	public boolean savecapedmessage(String key,String singlevalue,boolean remove,boolean checkrep,int CAPED_SIZE)
	{
		boolean ret=true;
		int inc=0; 
		try {
			JSONArray jarr=getjarrfrompref(key);
			int len=jarr.length();
			if(len>CAPED_SIZE) len=CAPED_SIZE;			
			JSONArray newjarr=new JSONArray();
			if(!remove)
			{
				checkrep=true;
				newjarr.put(inc++,singlevalue);
			}
			if(checkrep)
			{
				for(int i=0;i<len;i++) 
				{
					if(!singlevalue.equals(jarr.getString(i))) newjarr.put(inc++, jarr.getString(i));
					else ret=false;
				}
			}
			else for(int i=0;i<len;i++) newjarr.put(inc++, jarr.getString(i));
			settopref(key, newjarr.toString());
			if(remove)
			{
				if(ret) ret=false;
				else ret=true;
			}
			return ret;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	public boolean savecapedmessage(String key,JSONObject singlevalue,boolean remove)
	{
		return savecapedmessage(key, singlevalue, remove,CAPED_SIZE);
	}
	//single JSONObject insert method at the top of the capped message.......
		public boolean savecapedmessage(String key,JSONObject singlevalue,boolean remove,int CAPED_SIZE)
		{
			boolean checkrep=false;
			boolean ret=true;
			int inc=0;
			try {
				JSONArray jarr=getjarrfrompref(key);
				int len=jarr.length();
				if(len>CAPED_SIZE) len=CAPED_SIZE;
				JSONArray newjarr=new JSONArray();
				if(remove) checkrep=true;
				else
				{
					newjarr.put(inc++,singlevalue);
				}
				if(checkrep)
				{
					for(int i=0;i<len;i++) 
					{
						try{
							if(!singlevalue.equals(jarr.getJSONObject(i))) newjarr.put(inc++, jarr.getJSONObject(i));
							else ret=false;
						}catch(Exception e)
						{
							
						}
					}
					
				}
				else 
				{
					try{
						for(int i=0;i<len;i++) newjarr.put(inc++, jarr.getJSONObject(i));
					}catch(Exception e)
					{
						
					}
				}
				settopref(key, newjarr.toString());
				if(remove)
				{
					if(ret) ret=false;
					else ret=true;
				}
				else 
				{
					if(key.equals(SAVE_LIST))
					{
						Editor edit = datacenter.edit();
						for(int tem_len=jarr.length();len<tem_len;len++)
						{
							JSONObject j_temp=jarr.getJSONObject(len);
				    		edit.remove(j_temp.getString("_id"));
						}
						edit.commit();
					}
				}
					return ret;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show();
				return false;
			}
		}
		//single JSONObject insert method at the top of the capped message.......
		public boolean remove_capedmessage(String key,JSONObject singlevalue,String field)
		{
			boolean ret=true;
			int inc=0;
			try {
				JSONArray jarr=getjarrfrompref(key);
				int len=jarr.length();
				JSONArray newjarr=new JSONArray();
				String _id=singlevalue.getString(field);
				//Toast.makeText(context,"len:"+len+" "+CAPED_SIZE, Toast.LENGTH_SHORT).show();
				for(int i=0;i<len;i++) 
				{
					try{
						JSONObject jobj=jarr.getJSONObject(i);
						if(!_id.equals(jobj.getString(field))) newjarr.put(inc++, jarr.getJSONObject(i));
						else ret=false;
					}catch(Exception e)
					{
						//Toast.makeText(context,"i:"+i, Toast.LENGTH_SHORT).show();
					}
				}
				settopref(key, newjarr.toString());
				if(ret) return false;
				else return true;
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				//Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show();
				return false;
			}
		}
	//Without checking multiple objects insert method at the top of the capped message.......
	public boolean savecapedmessage(String key,JSONArray multiplevalue,int temp_cap)
	{
		try {
			JSONArray jarr=getjarrfrompref(key);
			int len=jarr.length();
			int inlen=multiplevalue.length();
			if(inlen>temp_cap) temp_cap=inlen;
			else if(inlen+len<temp_cap) temp_cap=inlen+len;
			int j=0;
			for(int i=inlen;i<temp_cap;i++) multiplevalue.put(i, jarr.get(j++));
			settopref(key, multiplevalue.toString());
			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	public void chang_notification_time(long note_time) throws JSONException {
		// TODO Auto-generated method stub
		JSONArray jarr=getcapedjarr(NOTIFICATION_STATEMENT);
		int len=jarr.length();
		JSONObject cmptime=new JSONObject();
		cmptime.put("$gt",note_time);
		for(int i=0;i<len;i++)
		{
			JSONObject jobj=jarr.getJSONObject(i);
			JSONObject statement=jobj.getJSONObject("statement");
			statement.put("curtime", cmptime);
			jobj.put("statement", statement);
			jarr.put(i, jobj);
		}
		settopref(NOTIFICATION_STATEMENT, jarr.toString());
	}
	public void settopref(String key,String value)
	{
		Editor edit=datacenter.edit();
		edit.putString(key, value);
		edit.commit();
	}
	public void settopref(String key,long value)
	{
		Editor edit=datacenter.edit();
		edit.putLong(key, value);
		edit.commit();
	}
	public void setintpref(String key,int value)
	{
		Editor edit=datacenter.edit();
		edit.putInt(key, value);
		edit.commit();
	}
	public void settopref(String key,Boolean value)
	{
		Editor edit=datacenter.edit();
		edit.putBoolean(key, value);
		edit.commit();
	}
	public void removefrompref(String key) {
		Editor edit=datacenter.edit();
		edit.remove(key);
		edit.commit();
	}
	public void search(String jsondata,Context context,Activity ac,boolean notification_enabled)
	{
		Intent intent=new Intent(context,Browseresult.class);
		intent.putExtra("jsondata", jsondata);
		intent.putExtra("notification_enabled", notification_enabled);
		ac.startActivity(intent);
	}
	public boolean add_photo(String _id,String photo_str,int count_status){
		try{
			dbutil.createentry(_id,photo_str);
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}
	public boolean remove_photo(String _id){
		boolean flag=false;
		try{
			dbutil.deleteentry(_id);
			return flag;
		}catch(Exception e)
		{
			return false;
		}
	}
}
