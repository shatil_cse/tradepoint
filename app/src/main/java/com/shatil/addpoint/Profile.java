package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.CommonUtilities.progdia;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Profile extends FragmentActivity implements OnDismissListener,Dialogmessageview.NoticeDialogListener{
	EditText et_name,et_address,et_phone;
	Button bt_update;
	CheckBox cb_edit;
	String name,address,phone;
	JPUtilities jputil;
	Uploaddownload updown;
	ImageView ivBack;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		jputil=new JPUtilities(getApplicationContext());
		progdia=new ProgressDialog(getApplicationContext());
		et_address=(EditText)findViewById(R.id.et_address);
		et_name=(EditText)findViewById(R.id.et_name);
		et_phone=(EditText)findViewById(R.id.et_phone);
		bt_update=(Button)findViewById(R.id.bt_update);
		cb_edit=(CheckBox)findViewById(R.id.cb_edit);
		setview(false);
		bt_update.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					name = et_name.getText().toString();
					address = et_address.getText().toString();
					phone = et_phone.getText().toString();
					name = name.trim();
					if (name.length() < 1) {
						Toast.makeText(getApplicationContext(), "Enter a valid name", Toast.LENGTH_LONG).show();
						return;
					}
					if (address.length() == 0) address = "Not available";
					if (phone.length() == 0) phone = "Not available";
					Update_profile();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Toast.makeText(getApplicationContext(), ""+e.toString(), Toast.LENGTH_LONG).show();
				}
			}
		});
		cb_edit.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				setview(isChecked);
			}
		});
		ivBack=(ImageView) findViewById(R.id.ivback);
		ivBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	public void Update_profile() {
		try{
			JSONObject profile=new JSONObject();
			profile.put("_id", admin_id);
			profile.put("reg_id", reg_id);
			profile.put("user_name", name);
			profile.put("user_address", address);
			profile.put("user_phone", phone);
			//new GenerateNotification(getApplicationContext(), profile.toString());
			progdia=ProgressDialog.show(this, "Updating profile", "Please wait.....");
			progdia.setOnDismissListener(this);
			updown=new Uploaddownload(getApplicationContext(), "/profile",profile.toString(),5);
		}catch(Exception e)
		{
			//Toast.makeText(getApplicationContext(), address+name+phone+"Profile:"+e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	public void setview(boolean isChecked) {
		et_address.setEnabled(isChecked);
		et_phone.setEnabled(isChecked);
		et_name.setEnabled(isChecked);
		bt_update.setEnabled(isChecked);
		cb_edit.setChecked(isChecked);
		if(!isChecked)
		{
			/*et_address.setTextColor(Color.BLACK);
			et_phone.setTextColor(Color.BLACK);
			et_name.setTextColor(Color.BLACK);*/
			load_pref();
		}
		else
		{
			/*et_address.setTextColor(Color.WHITE);
			et_phone.setTextColor(Color.WHITE);
			et_name.setTextColor(Color.WHITE);*/
			if(address.equals("Not available")) address="";
			if(phone.equals("Not available")) phone="";
		}
		et_address.setText(address);
		et_name.setText(name);
		et_phone.setText(phone);
	}
	public void load_pref()
	{
		name=jputil.getfrompref("user_name");
		address=jputil.getfrompref("user_address");
		phone=jputil.getfrompref("user_phone");
	}
	public void set_to_pref()
	{
		jputil.settopref("user_name", name);
		admin_name=name;
		jputil.settopref("user_address", address);
		jputil.settopref("user_phone", phone);
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		
		try {
			JSONObject jobj=new JSONObject(updown.getresult());
			if(jobj.getInt("message_code")==0)
			{
				set_to_pref();
				setview(false);
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Profile update succesfull", 101,"Successfully updated");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
				stjson.put(admin_id, stjson.getJSONObject(admin_id).put("user_name", name));
				jputil.settopref("user_rating", stjson.toString());
			}
			else 
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Profile not updated", 201,"Error happned");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			cdia.set_args("Connection error happened..", 201,"Error happned");
			cdia.show(getSupportFragmentManager(), "uploading_result");
		}
	}
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
