package com.shatil.addpoint;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;

public class CoreViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]=null;//new String[]{"Search Ads","Saved Adds","Notified Adds","Messages"}; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs=4; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

    int[] icons = null;//new int[]{R.drawable.search,R.drawable.select,R.drawable.note_set,R.drawable.message};
    Context context;

    Fragment searchProductFragment = null;
    Fragment savedProductFragment = null;
    Fragment notifiedProductFragment = null;
    Fragment messageProductFragment = null;
    // Build a Constructor and assign the passed Values to appropriate values in the class
    public CoreViewPagerAdapter(Context context, FragmentManager fm,String [] titles,int [] icons,int length) {
        super(fm);
        this.context = context;
        this.Titles=titles;
        this.icons=icons;
        this.NumbOfTabs=length;
    }
    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0)
        {
            if(searchProductFragment==null) searchProductFragment=new Browseproduct();
            return searchProductFragment;
        }
        else if(position == 1)
        {
            if(savedProductFragment==null) savedProductFragment=new SavedProductListFragment();
            return savedProductFragment;
        }
        else if(position==2){
            //if(groupFragment==null)groupFragment=new GroupFragment();
            if(notifiedProductFragment==null) notifiedProductFragment=new NotifiedProductListFragment();
            return notifiedProductFragment;
        }
        else if(position == 3)
        {
            //if(callLogsFragment==null)callLogsFragment = new CallLogFragment();
            if(messageProductFragment==null) messageProductFragment=new MessageFragment();
            return messageProductFragment;
        }

        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        SpannableStringBuilder spannableString = new SpannableStringBuilder(""+Titles[position]);

        Drawable drawableIcon = getContext().getResources().getDrawable(icons[position]);
        drawableIcon.setBounds(0 , 0 , drawableIcon.getIntrinsicWidth() , drawableIcon.getIntrinsicHeight());

        ImageSpan iconSpan = new ImageSpan(drawableIcon , ImageSpan.ALIGN_BASELINE);
        spannableString.setSpan(iconSpan , 0 , 1 , Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
