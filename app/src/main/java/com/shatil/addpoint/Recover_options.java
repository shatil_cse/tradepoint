package com.shatil.addpoint;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.reg_id;

public class Recover_options extends FragmentActivity implements OnClickListener, OnDismissListener,Dialogmessageview.NoticeDialogListener{
	ProgressDialog pdia;
	int operation=0;
	JPUtilities jputil;
	Uploaddownload updown;
	CheckBox cb_mail;
	ImageView iv_change_pin,iv_change_mail,iv_control,iv_lost,iv_forget,iv_getkey;
	ImageView bt_set,bt_change_pin,bt_change_mail,bt_control,bt_lost,bt_forget,bt_getkey;
	Button tv_change_pin,tv_change_mail,tv_control,tv_lost,tv_forget,tv_getkey;
	TextView tv_forget_email,tv_getkey_mail,tv_note;
	LinearLayout lay_set,lay_change_pin,lay_change_mail,lay_control,lay_lost,lay_forget,lay_getkey;
	LinearLayout op_set,op_change_pin,op_change_mail,op_control,op_lost,op_forget,op_getkey;
	EditText et_set_new,et_set_mail,et_pchange_old,et_pchange_new,et_mchange_pin,et_mchange_newmail;
	EditText et_control_curr,et_control_pre,et_control_key,et_lost_curr,et_lost_pre,et_lost_mail,et_getkey_pin;
	boolean pin_found=false,mail_found=false;
	String email_name="no name found";
	long pin=0,old_pin=0,key=0;
	int st_next=0;
	JSONObject jdata=new JSONObject();
	JSONObject statement=new JSONObject();
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.recover_options);
		jputil=new JPUtilities(getApplicationContext());
		pin_found=jputil.getfrompref("pin_found",false);
		try {
			statement.put("_id", admin_id);
			statement.put("reg_id", reg_id);
			jdata.put("statement", statement);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(pin_found) pin=jputil.getfrompref("provided_pin",0);
		mail_found=jputil.getfrompref("mail_found", false);
		if(mail_found) email_name=jputil.getfrompref("provided_mail");
		pdia=new ProgressDialog(this);
		setup_views();
		ImageView ivBack=(ImageView) findViewById(R.id.ivback);
		ivBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void setup_views() {
		// TODO Auto-generated method stub
		tv_note=(TextView)findViewById(R.id.tv_note);
		cb_mail=(CheckBox)findViewById(R.id.cb_set_mail);
		cb_mail.setOnClickListener(this);
		
		//six textviews..............
		iv_change_mail=(ImageView)findViewById(R.id.iv_cmail_close);
		iv_change_pin=(ImageView)findViewById(R.id.iv_cpin_close);
		iv_control=(ImageView)findViewById(R.id.iv_cotrol_close);
		iv_forget=(ImageView)findViewById(R.id.iv_gpin_close);
		iv_getkey=(ImageView)findViewById(R.id.iv_gserver_close);
		iv_lost=(ImageView)findViewById(R.id.iv_lost_close);
		
		iv_change_mail.setOnClickListener(this);
		iv_change_pin.setOnClickListener(this);
		iv_control.setOnClickListener(this);
		iv_forget.setOnClickListener(this);
		iv_getkey.setOnClickListener(this);
		iv_lost.setOnClickListener(this);
		
		//seven layouts................
		lay_change_mail=(LinearLayout)findViewById(R.id.lay_cmail);
		lay_change_pin=(LinearLayout)findViewById(R.id.lay_cpin);
		lay_control=(LinearLayout)findViewById(R.id.lay_control);
		lay_forget=(LinearLayout)findViewById(R.id.lay_gpin);
		lay_getkey=(LinearLayout)findViewById(R.id.lay_gserverkey);
		lay_lost=(LinearLayout)findViewById(R.id.lay_lost);
		lay_set=(LinearLayout)findViewById(R.id.lay_set);
		//seven option loyouts.............. 

		op_set=(LinearLayout)findViewById(R.id.op_set);
		op_change_mail=(LinearLayout)findViewById(R.id.lay_cmail_options);
		op_change_pin=(LinearLayout)findViewById(R.id.lay_cpin_options);
		op_control=(LinearLayout)findViewById(R.id.lay_control_options);
		op_forget=(LinearLayout)findViewById(R.id.lay_gpin_options);
		op_getkey=(LinearLayout)findViewById(R.id.lay_gserver_options);
		op_lost=(LinearLayout)findViewById(R.id.lay_lost_options);
		
		//seven buttons.....
		bt_change_mail=(ImageView)findViewById(R.id.bt_cmail);
		bt_change_pin=(ImageView)findViewById(R.id.bt_cpin);
		bt_control=(ImageView)findViewById(R.id.bt_control);
		bt_forget=(ImageView)findViewById(R.id.bt_gpin);
		bt_getkey=(ImageView)findViewById(R.id.bt_gserverkey);
		bt_lost=(ImageView)findViewById(R.id.bt_lost);
		bt_set=(ImageView)findViewById(R.id.bt_set_recover);
		
		bt_change_mail.setOnClickListener(this);
		bt_change_pin.setOnClickListener(this);
		bt_control.setOnClickListener(this);
		bt_forget.setOnClickListener(this);
		bt_getkey.setOnClickListener(this);
		bt_lost.setOnClickListener(this);
		bt_set.setOnClickListener(this);
		
		//six textviews.........
		tv_change_mail=(Button)findViewById(R.id.tv_change_mail);
		tv_change_pin=(Button)findViewById(R.id.tv_change_pin);
		tv_control=(Button)findViewById(R.id.tv_control);
		tv_forget=(Button)findViewById(R.id.tv_forget);
		tv_getkey=(Button)findViewById(R.id.tv_getkey);
		tv_lost=(Button)findViewById(R.id.tv_lost);
		
		tv_change_mail.setOnClickListener(this);
		tv_change_pin.setOnClickListener(this);
		tv_control.setOnClickListener(this);
		tv_forget.setOnClickListener(this);
		tv_getkey.setOnClickListener(this);
		tv_lost.setOnClickListener(this);
		
		//Others textviews..........
		tv_forget_email=(TextView)findViewById(R.id.tv_gmail);
		tv_getkey_mail=(TextView)findViewById(R.id.tv_gservermail);
		
		//EditTexts.................
		et_control_curr=(EditText)findViewById(R.id.et_control_cur_pin);
		et_control_pre=(EditText)findViewById(R.id.et_control_pre_pin);
		et_control_key=(EditText)findViewById(R.id.et_control_key);
		et_mchange_newmail=(EditText)findViewById(R.id.et_cmail_address);
		et_mchange_pin=(EditText)findViewById(R.id.et_cmail_pin);
		et_pchange_old=(EditText)findViewById(R.id.et_cpin_old);
		et_pchange_new=(EditText)findViewById(R.id.et_cpin_new);
		et_set_new=(EditText)findViewById(R.id.et_set_new);
		et_set_mail=(EditText)findViewById(R.id.et_set_mail);
		et_getkey_pin=(EditText)findViewById(R.id.et_gserverpin);
		et_lost_curr=(EditText)findViewById(R.id.et_lost_cur_pin);
		et_lost_pre=(EditText)findViewById(R.id.et_lost_pre_pin);
		et_lost_mail=(EditText)findViewById(R.id.et_lost_pre_mail);
		//set_color();
		pin_found(pin_found);
		mail_found(mail_found);
	}
	public void set_color()
	{
		op_change_mail.setBackgroundColor(Color.parseColor("#005588"));
		op_change_pin.setBackgroundColor(Color.parseColor("#005588"));
		op_control.setBackgroundColor(Color.parseColor("#005588"));
		op_getkey.setBackgroundColor(Color.parseColor("#005588"));
		op_forget.setBackgroundColor(Color.parseColor("#005588"));
		op_set.setBackgroundColor(Color.parseColor("#005588"));
		op_lost.setBackgroundColor(Color.parseColor("#005588"));
	}
	
	public void mail_found(boolean found)
	{
		if(found)
		{
			tv_change_mail.setText("Change email address");
			tv_change_mail.setTextColor(Color.parseColor("#00ee00"));
			tv_getkey_mail.setText("Email address :"+email_name);
			tv_note.setVisibility(View.GONE);
			et_getkey_pin.setVisibility(View.GONE);
			lay_forget.setVisibility(View.VISIBLE);
			tv_forget_email.setText("Email address :"+email_name);
		}
		else
		{
			tv_change_mail.setText("Set email address");
			tv_change_mail.setTextColor(Color.RED);
			if(!mail_found) tv_note.setText("Note that \nif you don't provide email address then we will not be able to help you when you forgot PIN or totaly lost your device");
			tv_note.setVisibility(View.VISIBLE);
			tv_getkey_mail.setText("Enter PIN :");
			et_getkey_pin.setVisibility(View.VISIBLE);
			lay_forget.setVisibility(View.GONE);
		}
	}
	public void pin_found(boolean found)
	{
		if(found)
		{
			lay_change_mail.setVisibility(View.VISIBLE);
			lay_change_pin.setVisibility(View.VISIBLE);
			lay_control.setVisibility(View.VISIBLE);
			lay_getkey.setVisibility(View.VISIBLE);
			lay_lost.setVisibility(View.VISIBLE);
			lay_set.setVisibility(View.GONE);
		}
		else
		{
			lay_change_mail.setVisibility(View.GONE);
			lay_change_pin.setVisibility(View.GONE);
			lay_control.setVisibility(View.GONE);
			lay_getkey.setVisibility(View.GONE);
			lay_lost.setVisibility(View.GONE);
			lay_set.setVisibility(View.VISIBLE);
		}
	}
	
	public void op_view_gone()
	{
		op_change_mail.setVisibility(View.GONE);
		op_change_pin.setVisibility(View.GONE);
		op_control.setVisibility(View.GONE);
		op_forget.setVisibility(View.GONE);
		op_getkey.setVisibility(View.GONE);
		op_lost.setVisibility(View.GONE);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if(!pin_found)
		{
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			cdia.set_args("If you don't set the PIN we will be unable to give you recovery service..", 0,BitmapFactory.decodeResource(getResources(), R.drawable.warning),"Important notice");
			cdia.show(getSupportFragmentManager(), "exit_decission");
		}
		else finish();
	}
	public void set_to_server() throws JSONException {
		pdia=ProgressDialog.show(this, "Loading data", "please wait..");
		pdia.setOnDismissListener(this);
		jdata.put("_id", admin_id);
		jdata.put("reg_id",reg_id);
		updown=new Uploaddownload(getApplicationContext(), "/updatedocs", jdata.toString(), pdia);
		st_next=0;
	}
	public JSONObject create_set() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		String new_pin=et_set_new.getText().toString();
		if(new_pin.length()!=4)
		{
			Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
			return attach;
		}
		pin=Long.valueOf(new_pin);
		attach.put("saved_pin",pin);
		if(cb_mail.isChecked())
		{
			attach.put("attach_mail", true);
			email_name=et_set_mail.getText().toString();
			if(email_name.length()>3)
			{
				attach.put("saved_mail", email_name);
			}
			else 
			{
				Toast.makeText(getApplicationContext(), "Enter valid email address", Toast.LENGTH_LONG).show();
				return attach;
			}
		}
		else attach.put("attach_mail", false);
		jdata.put("attachment", attach);
		jdata.put("data_change_st",0);
		st_next=1;
		return attach;
	}
	public JSONObject create_change_pin() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		String new_pin=et_pchange_new.getText().toString();
		String old_pin=et_pchange_old.getText().toString();
		if(new_pin.length()!=4||old_pin.length()!=4)
		{
			Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
			return attach;
		}
		long n_pin=Long.valueOf(new_pin);
		pin=Long.valueOf(old_pin);
		attach.put("saved_pin",pin);
		attach.put("new_pin",n_pin);
		jdata.put("attachment", attach);
		jdata.put("data_change_st",1);
		st_next=1;
		return attach;
	}
	public JSONObject create_change_mail() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		String new_mail=et_mchange_newmail.getText().toString();
		String old_pin=et_mchange_pin.getText().toString();
		if(old_pin.length()!=4)
		{
			Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
			return attach;
		}
		if(new_mail.length()<4)
		{
			Toast.makeText(getApplicationContext(), "Enter valid email address", Toast.LENGTH_LONG).show();
			return attach;
		}
		pin=Long.valueOf(old_pin);
		email_name=new_mail;
		attach.put("saved_pin",pin);
		attach.put("new_mail",new_mail);
		jdata.put("attachment", attach);
		jdata.put("data_change_st",2);
		st_next=1;
		return attach;
	}
	public JSONObject create_forget_pin() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		jdata.put("attachment", attach);
		jdata.put("data_change_st",3);
		st_next=1;
		return attach;
	}
	public JSONObject create_get_key() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		if(!mail_found)
		{
			String old_pin=et_getkey_pin.getText().toString();
			pin=Long.valueOf(old_pin);
			attach.put("saved_pin",pin);
			jdata.put("data_change_st",4);
			if(old_pin.length()!=4)
			{
				Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
				return attach;
			}
		}
		else
		{
			jdata.put("data_change_st",5);
		}		
		jdata.put("attachment", attach);
		st_next=1;
		return attach;
	}
	public JSONObject create_control() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		String new_pin=et_control_curr.getText().toString();
		String pre_pin=et_control_pre.getText().toString();
		String serv_key=et_control_key.getText().toString();
		if(pre_pin.length()!=4||new_pin.length()!=4||serv_key.length()<1)
		{
			Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
			return attach;
		}
		pin=Long.valueOf(new_pin);
		attach.put("saved_pin",pin);
		attach.put("pre_pin",Long.valueOf(pre_pin));
		attach.put("saved_server_key",Long.valueOf(serv_key));
		jdata.put("attachment", attach);
		jdata.put("data_change_st",8);
		st_next=1;
		return attach;
	}
	
	public JSONObject create_lost() throws JSONException {
		st_next=0;
		JSONObject attach=new JSONObject();
		String new_pin=et_lost_curr.getText().toString();
		String pre_pin=et_lost_pre.getText().toString();
		String mailadd=et_lost_mail.getText().toString();
		if(pre_pin.length()!=4||new_pin.length()!=4)
		{
			Toast.makeText(getApplicationContext(), "Enter 4 digit pin", Toast.LENGTH_LONG).show();
			return attach;
		}
		pin=Long.valueOf(new_pin);
		attach.put("saved_pin",pin);
		attach.put("lost_pin",Long.valueOf(pre_pin));
		attach.put("lost_mail",mailadd);
		jdata.put("attachment", attach);
		jdata.put("data_change_st",9);
		st_next=1;
		return attach;
	}
		@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try{
		switch (v.getId()) {
		case R.id.cb_set_mail:
			if(cb_mail.isChecked())
			{
				et_set_mail.setVisibility(View.VISIBLE);
			}
			else et_set_mail.setVisibility(View.GONE);
			break;
		case R.id.bt_set_recover:
			operation=0;
			create_set();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_cmail:
			operation=2;
			create_change_mail();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_cpin:
			operation=1;
			create_change_pin();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_control:
			operation=5;
			create_control();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_gpin:
			operation=3;
			create_forget_pin();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_gserverkey:
			operation=4;
			create_get_key();
			if(st_next==1) set_to_server();
			break;
		case R.id.bt_lost:
			operation=6;
			create_lost();
			if(st_next==1) set_to_server();
			break;
		case R.id.iv_cmail_close:
			op_change_mail.setVisibility(View.GONE);
			break;
		case R.id.iv_cpin_close:
			op_change_pin.setVisibility(View.GONE);
			break;
		case R.id.iv_cotrol_close:
			op_control.setVisibility(View.GONE);
			break;
		case R.id.iv_gpin_close:
			op_forget.setVisibility(View.GONE);
			break;
		case R.id.iv_gserver_close:
			op_getkey.setVisibility(View.GONE);
			break;
		case R.id.iv_lost_close:
			op_lost.setVisibility(View.GONE);
			break;
		case R.id.tv_change_mail:
			op_view_gone();
			op_change_mail.setVisibility(View.VISIBLE);
			break;
		case R.id.tv_change_pin:
			op_view_gone();
			op_change_pin.setVisibility(View.VISIBLE);
			break;
		case R.id.tv_control:
			op_view_gone();
			op_control.setVisibility(View.VISIBLE);
			break;
		case R.id.tv_forget:
			op_view_gone();
			op_forget.setVisibility(View.VISIBLE);
			break;
		case R.id.tv_getkey:
			op_view_gone();
			op_getkey.setVisibility(View.VISIBLE);
			break;
		case R.id.tv_lost:
			op_view_gone();
			op_lost.setVisibility(View.VISIBLE);
			break;
		}
		}catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Enter data in correct form", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	public void restart_recovery_options() {
		Intent intent=new Intent(getApplicationContext(),Recover_options.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		int st_int=updown.get_status_int();
		//Toast.makeText(getApplicationContext(), "int:"+st_int,Toast.LENGTH_LONG).show();
		if(st_int==250)
		{
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			if(operation==0)
			{
				jputil.settopref("pin_found", true);
				jputil.settopref("provided_pin",pin );
				if(cb_mail.isChecked())
				{
					jputil.settopref("mail_found", true);
					jputil.settopref("provided_mail", email_name);
					cdia.set_args("Registration information successfully updated & you are secure with email address",101,"Successfully updated");
				}
				else cdia.set_args("Registration information successfully updated but as you haven't provided email your account will be lost if you lose your device. You set email from recovery options",101,BitmapFactory.decodeResource(getResources(), R.drawable.warning),"Important notice");
				
			}
			else if(operation==1)
			{
				jputil.settopref("provided_pin",pin );
				jputil.settopref("pin_found", true);
				cdia.set_args("PIN has been changed successfully",101,"Successfully updated");
			}
			else if(operation==2)
			{
				jputil.settopref("provided_mail",email_name);
				jputil.settopref("mail_found", true);
				cdia.set_args("Email has been setted successfully",101,"Successfully updated");
			}
			else if(operation==3)
			{
				cdia.set_args("PIN has been sent to your email address",101,"Successfully sent");
			}
			else if(operation==4)
			{
				if(!mail_found)
				{
					String res=updown.getresult();
					try {
						JSONObject job=new JSONObject(res);
						key=job.getLong("saved_server_key");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					cdia.set_args("Your Server key: "+key,101,"Got key");
				}
				else cdia.set_args("Server key has been sent to your email address",101,"Successfully sent");
			}
			else if(operation==5)
			{
				cdia.set_args("Device has been changed successfully",101,"Successfully changed");
			}
			else if(operation==6)
			{
				cdia.set_args("Server key has been sent to your email address",101,"Successfully sent");
			}
			cdia.show(getSupportFragmentManager(), "updating data");
		}
		else if(operation==0&&st_int==275)
		{
			String res=updown.getresult();
			try {
				JSONObject job=new JSONObject(res);
				if(job.has("saved_pin"))
				{
					pin=job.getLong("saved_pin");
					jputil.settopref("provided_pin", pin);
				}
				if(job.has("saved_mail")) 
				{
					email_name=job.getString("saved_mail");
					jputil.settopref("provided_mail", email_name);
					jputil.settopref("mail_found", true);
				}
				else jputil.settopref("mail_found", false);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jputil.settopref("pin_found", true);
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			
			cdia.set_args("PIN already been configured",201,"Important notice");
			cdia.show(getSupportFragmentManager(), "updating data");
		}
		else 
		{
			Dialogmessageview cdia=new Dialogmessageview();
			cdia.initialize_bmp(getApplicationContext());
			
			cdia.set_args(updown.get_status_string(),201,"Error happned");
			cdia.show(getSupportFragmentManager(), "updating data");
		}
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		if(!jputil.getfrompref("pin_found", false)) finish();
		else restart_recovery_options();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
