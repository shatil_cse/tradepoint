package com.shatil.addpoint;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class GenerateNotification {
	@SuppressWarnings("deprecation")
	public GenerateNotification(Context context, String message) {
        int icon = R.drawable.noti_app_icon;
        String title = context.getString(R.string.app_name);
        //for new version of notification use bellow comment code...
        //Notification noti = new Notification.Builder(context).setContentTitle(title).setContentText(message).setSmallIcon(icon).setLargeIcon(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), icon),24,24,false)).setAutoCancel(true).build();
        Intent notificationIntent = new Intent(context, CoreActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        //notification.setLatestEventInfo(context, title, message, null);
        //notification.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(message);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
	@SuppressWarnings("deprecation")
	public GenerateNotification(Context context, String message,Intent notificationintent,int icon) {
        String title = context.getString(R.string.app_name);
        notificationintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationintent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(message);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
	
}
