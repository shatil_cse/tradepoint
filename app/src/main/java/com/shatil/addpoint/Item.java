package com.shatil.addpoint;

import org.json.JSONObject;

import android.graphics.Bitmap;

public class Item {
	boolean opened=false,removed=false;
	int is_loding=0;
	Bitmap bmp=null;
	int position=0;
    JSONObject jsonobj=new JSONObject();
    public Item(JSONObject jsonobj) {
        super();
        this.jsonobj=jsonobj;
    }
    public JSONObject getjson() {
        return jsonobj;
    }
    public boolean get_open_status() {
		return opened;
	}
    public void set_open_status(boolean opened) {
		this.opened=opened;
	}
    public int get_loding_status() {
		return is_loding;
	}
    public void set_loding_status(int is_loading) {
    	this.is_loding=is_loading;
	}
    public Bitmap get_bitmap() {
		return bmp;
	}
    public void set_bitmap(Bitmap bmp) {
    	this.bmp=bmp;
	}
    public int get_position() {
		return position;
	}
    public void set_position(int position) {
    	this.position=position;
	}
    public boolean get_remove_status() {
		return removed;
	}
    public void set_remove_status(boolean removed) {
    	this.removed=removed;
	}
}