package com.shatil.addpoint;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_NAME;
import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.NOTIFIED_PRODUCT;
/**
 * Created by Shatil on 1/26/2016.
 */
public class SavedNotificationListFragment extends Fragment implements View.OnClickListener {
    JSONObject jsonobj;
    static Reset_product_list reset_notified_product_list;
    static Ratingloaded reset_notified_product_rating;
    JPUtilities jputil;
    ItemAdapter iadp;
    String result;
    ListView lvProductList;
    TextView tvClearAll;
    Activity activity;
    Context context;
    ProgressDialog progressDialog;
    LinearLayout layHeader;
    @SuppressLint("UseSparseArrays")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.saved_product_list_fragment , container , false);
        context=getContext();
        activity=getActivity();
        layHeader=(LinearLayout)v.findViewById(R.id.lay_header);
        layHeader.setVisibility(View.GONE);
        iadp=new ItemAdapter(getActivity(), new ArrayList<Item>());
        lvProductList=(ListView) v.findViewById(R.id.lv_product_list);
        lvProductList.setAdapter(iadp);
        jputil=new JPUtilities(getActivity());
        result = jputil.getjarrfrompref(NOTIFICATION_STATEMENT).toString();
        new LoadInBackground().execute("");
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
        }
    }

    public class ItemAdapter extends ArrayAdapter<Item> {
        public ItemAdapter(Context c, List<Item> items) {
            super(c, 0);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            NotificationView messageView = (NotificationView)convertView;
            if (null == messageView)
            {
                messageView = NotificationView.inflate(parent);
                messageView.set_status(11);
            }
            messageView.setItem(getItem(position));
            return messageView;
        }
    }

    public class LoadInBackground extends AsyncTask<String,Integer,String>
    {
        ArrayList<Item> items = new ArrayList<Item>();
        @Override
        protected void onPreExecute() {
            progressDialog=ProgressDialog.show(activity,"","Loading products..");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try{
                JSONArray jsonarr=new JSONArray(result);
                int len=jsonarr.length();
                for (int i=0; i < len; i++) {
                    try {
                        JSONObject jsonobj = jsonarr.getJSONObject(i);
                        Item item = new Item(jsonobj);
                        items.add(item);
                    } catch (Exception e) {
                    }
                }
            }catch(Exception e)
            {
                Log.d(getClass().getSimpleName(),"NotifiedProductListFragment Load failed:"+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                if(progressDialog!=null&&progressDialog.isShowing()) progressDialog.dismiss();
                iadp.clear();
                iadp.addAll(items);
                Log.d(getClass().getSimpleName(), "NotifiedProductListFragment Loaded: " + iadp.getCount());
                lvProductList.setAdapter(iadp);
                iadp.notifyDataSetChanged();
            }catch (Exception e)
            {
                Log.d(getClass().getSimpleName(),"NotifiedProductListFragment Load updating failed:"+e.toString());
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            JSONArray jsonArray = jputil.getjarrfrompref(NOTIFICATION_STATEMENT);
            if(jsonArray.length()==0)
            {
                cancelalarm(context);
                JPUtilities jpUtilities=new JPUtilities(context);
                jpUtilities.settopref(CommonUtilities.ALARM_FLAG, false);
            }
        }catch (Exception e)
        {

        }
    }
    public void cancelalarm(Context tempcont) {
        // TODO Auto-generated method stub
        try {
            Intent in = new Intent(tempcont, Myalarm.class);
            PendingIntent alarmin = PendingIntent.getBroadcast(tempcont, 0, in, 0);
            @SuppressWarnings("static-access")
            AlarmManager am = (AlarmManager) context.getSystemService(tempcont.ALARM_SERVICE);
            am.cancel(alarmin);
            disablebootreceiver(tempcont);
        } catch (Exception e) {
            //Toast.makeText(tempcont, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void disablebootreceiver(Context tempcont) {
        // TODO Auto-generated method stub
        ComponentName receiver = new ComponentName(tempcont,
                Simplebootrecever.class);
        PackageManager packm = tempcont.getPackageManager();
        packm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
