package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.reg_id;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Support extends FragmentActivity implements OnClickListener, OnDismissListener,Dialogmessageview.NoticeDialogListener{

	TextView tv_mail;
	EditText et_message;
	Button bt_send;
	ProgressDialog pdia;
	Uploaddownload updown;
	int Error_code=0;
	Dialogmessageview dialogmessage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.support);
		tv_mail=(TextView)findViewById(R.id.tv_mail);
		et_message=(EditText)findViewById(R.id.et_message);
		bt_send=(Button)findViewById(R.id.bt_send);
		bt_send.setOnClickListener(this);
		tv_mail.setOnClickListener(this);
		pdia=new ProgressDialog(this);
		ImageView ivBack=(ImageView) findViewById(R.id.ivback);
		ivBack.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.ivback:
				finish();
				break;
			case R.id.bt_send:
				pdia=ProgressDialog.show(this, "Operation in progress", "Please wait..");
				pdia.setOnDismissListener(this);
				try{
					JSONObject jobj=new JSONObject();
					String message=et_message.getText().toString();
					if(message.length()<1||message.length()>10000)
					{
						Toast.makeText(getApplicationContext(), "Type message between 1 to 10000 characters", Toast.LENGTH_LONG).show();
						Error_code=720;
						pdia.dismiss();
						return;
					}
					//do the search....
					jobj.put("message_body", message);
					jobj.put("user_id", admin_id);
					jobj.put("reg_id", reg_id);
					updown=new Uploaddownload(getApplicationContext(), "/report", jobj.toString(),pdia);
					Error_code=0;
				}catch (Exception e) {
					// TODO: handle exception
					Error_code=700;
					pdia.dismiss();
				}
				break;

			case R.id.tv_mail:
				mailme();
				break;
		}
	}
	public void mailme()
	{
		String emailadd[]={"appsupport@gmail.com"};
		Intent emailintent=new Intent(android.content.Intent.ACTION_SEND);
		emailintent.putExtra(android.content.Intent.EXTRA_EMAIL,emailadd);//email addresses must be string array.....  
		emailintent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Need support");
		emailintent.putExtra(android.content.Intent.EXTRA_TEXT, "");
		emailintent.setType("plain/text");
		startActivity(emailintent);
	}
	@Override
	public void onDismiss(DialogInterface arg0) {
		// TODO Auto-generated method stub
		try{
		Dialogmessageview cdia=new Dialogmessageview();
		cdia.initialize_bmp(getApplicationContext());	
		if(Error_code<700)
		{
			int code=updown.get_status_int();
			if(code==260)
			{
				cdia.set_args(updown.get_status_string(),101);
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
			else 
			{
				cdia.set_args(updown.get_status_string(),201);
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
		}
		else if(Error_code==700)
		{
			cdia.set_args("Apps Error",201);
			cdia.show(getSupportFragmentManager(), "Uploading_result");
		}
		}catch(Exception e)
		{
			//Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
