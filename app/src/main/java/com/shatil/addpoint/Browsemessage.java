package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.MESSAGE_VIEW_STATUS;
import static com.shatil.addpoint.CommonUtilities.REG_STATUS;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.CommonUtilities.dbutil;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.stjson;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

public class Browsemessage extends ActionBarActivity implements OnTouchListener, OnClickListener, OnDismissListener,Dialogmessageview.NoticeDialogListener{

    Toolbar mToolbar;
	LinearLayout lay_op;
    int status_curr=0;
	int status_come_from=0;
	long interval=0;
	int review=0;
	JPUtilities jputil;
	String _id,saver_id,catagory;
	EditText et_message;
	JSONObject jobj;
	JSONObject jmes;
	String message_body="no_message";
	TextView tv_confirm;
	ImageView ivSendMessage;
	RadioButton rb_good,rb_bad,rb_cheat;
	boolean can_send=false,oncreate=false;
	int confirmation_status=0;
	Uploaddownload updown;
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		oncreate=true;
		int open=0;
		try{
			open=dbutil.isopen();
			if(open==0) finish();
		}catch(Exception e)
		{
			finish();
		}
		if(open==0) finish();
		else
		{

            setContentView(R.layout.message_list);
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            lay_op=(LinearLayout)findViewById(R.id.message_menu);
			tv_confirm=(TextView)findViewById(R.id.tv_confirm);
            tv_confirm.setOnClickListener(this);
            ivSendMessage=(ImageView) findViewById(R.id.iv_send_message);
            ivSendMessage.setOnTouchListener(this);
            jputil=new JPUtilities(getApplicationContext());
			status_come_from=getIntent().getIntExtra("status_come_from",0);
	        MESSAGE_VIEW_STATUS=status_come_from;
	        if(MESSAGE_VIEW_STATUS==0)
	        {
                getSupportActionBar().setTitle("Product with messages");
	        }
	        else if(MESSAGE_VIEW_STATUS==1)
	        {
                getSupportActionBar().setTitle("Buyers with messages");
	        }
	        else
	        {
                getSupportActionBar().setTitle("Messages");
	        }

			load_statics();
            if(status_come_from==2)
			{
	        	catagory=getIntent().getStringExtra("catagory");
				_id=getIntent().getStringExtra("_id");
				saver_id=getIntent().getStringExtra("saver_id");
				jobj=dbutil.get_messages(_id, saver_id);
				try {
					JSONObject json=new JSONObject(jputil.getfrompref("got_new_message"));
					json.remove(_id);
					jputil.settopref("got_new_message", json.toString());
					Log.d(getClass().getSimpleName(),"Removed new Message"+_id);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		        setview();
			}
			else lay_op.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void setview() {
		// TODO Auto-generated method stub
		et_message=(EditText)findViewById(R.id.et_message);
		//bt_confirm=(Button)findViewById(R.id.bt_confirm);
		//bt_review=(Button)findViewById(R.id.bt_review);
        review=4;
        change_view();
	}
	
	private void change_view() {
		// TODO Auto-generated method stub
		try{
		can_send=dbutil.get_can_send(jobj);
		can_send=true;
		confirmation_status=dbutil.get_confirm_status(jobj);
		}catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString()+"\n"+saver_id, Toast.LENGTH_LONG).show();
			finish();
		}
		/*if(!can_send)
		{
			tv_message.setText("Wait for message");
		}
		else tv_message.setText("Send message");*/
		
		if(confirmation_status==0)
		{
			tv_confirm.setText("Send confirmation");
		}
		else if(confirmation_status==1)
		{
			tv_confirm.setText("Wait for confirmation");
		}
		else if(confirmation_status==2)
		{
			tv_confirm.setText("Review");
		}
	}
	public JSONObject constract_message(String message,int confirm)
	{
		jmes=new JSONObject();
		try {
			jmes.put("sender_name",admin_name);
			jmes.put("_id", _id);
			jmes.put("reg_id", reg_id);
			jmes.put("catagory", catagory);
			jmes.put("from", admin_id);
			jmes.put("to", saver_id);
			jmes.put("saver_id",saver_id);
			jmes.put("message_body", message);
			jmes.put("confirm", confirm);
			jmes.put("access_time",System.currentTimeMillis());
			this.message_body=message;
			this.confirmation_status=confirm;
			if(confirm==5)
			{
				jmes.put("review",review);
                jmes.put("review_message",reviewMessage);
			}
			else if(confirm==1)
			{
				jmes.put("validity",confirmationValidity);
                jmes.put("confirmation_message",confirmMessage);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jmes;
	}
	public void send_message(String message_body,int confirm)
	{
		ProgressDialog pdia=ProgressDialog.show(this, "Sending message", "Please wait..");
		pdia.setOnDismissListener(this);
		//do the search....
		updown=new Uploaddownload(getApplicationContext(), "/send_message", constract_message(message_body, confirm).toString(),pdia);
		//new GenerateNotification(getApplicationContext(), constract_message(message_body, confirm).toString());
	}
	private void work_for_click(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*case R.id.tv_message:
			if(can_send)
			{
				lay_message.setVisibility(View.VISIBLE);
				status_curr=1;
			}
			else Toast.makeText(getApplicationContext(), "Wait for other user message", Toast.LENGTH_LONG).show();
			break;*/
		case R.id.tv_confirm:
			if(confirmation_status==2)
			{
				status_curr=4;
				setReviewDialogViews();
			}
			else if(confirmation_status==0||confirmation_status==5)
			{
				status_curr=2;
				setConfirmationDialogViews();
			}
			else Toast.makeText(getApplicationContext(), "Wait for other user confirmation", Toast.LENGTH_LONG).show();
			break;
		case R.id.rbgood:
			rb_bad.setChecked(false);
			rb_cheat.setChecked(false);
			rb_good.setChecked(true);
			review=4;
			break;
		case R.id.rbbad:
			rb_bad.setChecked(true);
			rb_cheat.setChecked(false);
			rb_good.setChecked(false);
			review=2;
			break;
		case R.id.rbcheat:
			rb_bad.setChecked(false);
			rb_cheat.setChecked(true);
			rb_good.setChecked(false);
			review=0;
			break;
		case R.id.iv_send_message:
			send_message(et_message.getText().toString(), 0);
			break;
		}
	}
	
	public void change_lay()
	{
		status_curr=0;
		onResume();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		work_for_click(v);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(status_curr==0) finish();
		else change_lay();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!oncreate)
		{
			load_statics();
		}
		else oncreate=false;
	}
	public void load_statics()
	{
		jputil.settopref("should_open_option" + "service_message", false);
		try {
			jputil.settopref("stjson_open", true);
			stjson=new JSONObject(jputil.getfrompref("user_rating"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			stjson=new JSONObject();
			jputil.settopref("user_rating", stjson.toString());
		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		jputil.settopref("stjson_open", false);
		jputil.settopref("user_rating", stjson.toString());
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		try{
			if(updown.get_status_int()==1||updown.get_status_int()==500)
			{
				String result=updown.getresult();
				JSONObject jres=new JSONObject(result);
				int message_code=jres.getInt("message_code");
				jmes=jres.getJSONObject("message");
				confirmation_status=jmes.getInt("confirm");
				if(confirmation_status==5||confirmation_status==2) confirmation_status=0;
				try{
					can_send=jmes.getBoolean("can_send");
				}catch(Exception e)
				{
					can_send=false;
				}
				if(message_code==0)
				{
					JSONObject jdet=jres.getJSONObject("details");
					JSONObject juser=jres.getJSONObject("user_info");
					stjson.put(juser.getString("_id"),juser);
					dbutil.set_message(_id, jdet, jmes, 0, can_send);
				}
				else if(message_code==501)
				{
					jputil.setintpref(REG_STATUS, 0);
					GCMRegistrar.setRegisteredOnServer(getApplicationContext(), false);
					Intent i=new Intent(getApplicationContext(),Tab.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
				}
				else
				{
					//Toast.makeText(getApplicationContext(), "con:"+confirmation_status,Toast.LENGTH_LONG).show();
					dbutil.set_message(_id, can_send, confirmation_status, jmes.getString("to"));
				}
				if(message_code==0)
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(getApplicationContext());
					cdia.set_args("Message sent successfully",101,"Successfully sent");
					cdia.show(getSupportFragmentManager(), "Uploading_result");
				}
				else if(message_code==401)
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(getApplicationContext());
					cdia.set_args("Wait for users response",201,"Error happened");
					cdia.show(getSupportFragmentManager(), "Uploading_result");
				}
				else if(message_code!=0)
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(getApplicationContext());
					cdia.set_args("Message not sent",201,"Error happned");
					cdia.show(getSupportFragmentManager(), "Uploading_result");
				}
			}
			else
			{
				Dialogmessageview cdia=new Dialogmessageview();
				cdia.initialize_bmp(getApplicationContext());
				cdia.set_args("Message not sent",201,"Error happned");
				cdia.show(getSupportFragmentManager(), "Uploading_result");
			}
		}catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
		jobj=dbutil.get_messages(_id, saver_id);
		change_view();
	}
	
	@Override
	public void onDialogPositiveClick(
			android.support.v4.app.DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDialogNegativeClick(
			android.support.v4.app.DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	Dialog dialog;
    long confirmationValidity=3;
    String reviewMessage="",confirmMessage="";
	public void setConfirmationDialogViews()
	{
		final NumberPicker np;
        final EditText etMessage;
        Button btDialogCancel;
		ImageView ivSendMessage;
		final TextView tvHelp,tvHelpMessage;
        dialog=new Dialog(this);
		dialog.setTitle("Confimation Message");
		dialog.setContentView(R.layout.confirmation_layout);
		btDialogCancel = (Button) dialog.findViewById(R.id.bt_cancel);
		ivSendMessage=(ImageView) dialog.findViewById(R.id.iv_send_message);
        np=(NumberPicker)dialog.findViewById(R.id.nppicker1);
        tvHelp=(TextView)dialog.findViewById(R.id.tv_show_help_message);
        tvHelpMessage=(TextView)dialog.findViewById(R.id.tv_help_message);
        tvHelpMessage.setVisibility(View.GONE);
        np.setMinValue(1);
        np.setMaxValue(30);
        np.setValue(3);
        etMessage=(EditText)dialog.findViewById(R.id.et_message);
        btDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ivSendMessage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationValidity=np.getValue()*1000*60*60*24;
                confirmMessage=etMessage.getText().toString();
                dialog.dismiss();
                send_message("This is a Confirmation Message", 1);
                //send_message("Review message", 5);
            }
        });
        tvHelp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHelpMessage.setVisibility(View.VISIBLE);
            }
        });

        dialog.show();
	}
	public void setReviewDialogViews()
	{
        final NumberPicker np;
        final EditText etMessage;
        Button btDialogCancel;
        ImageView ivSendMessage;
        dialog=new Dialog(this);
        dialog.setTitle("Review");
        dialog.setContentView(R.layout.review_layout);
        btDialogCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        ivSendMessage=(ImageView) dialog.findViewById(R.id.iv_send_message);
        rb_bad=(RadioButton)dialog.findViewById(R.id.rbbad);
        rb_good=(RadioButton)dialog.findViewById(R.id.rbgood);
        rb_cheat=(RadioButton)dialog.findViewById(R.id.rbcheat);
        rb_bad.setChecked(false);
        rb_cheat.setChecked(false);
        rb_good.setChecked(true);
        rb_bad.setOnClickListener(this);
        rb_good.setOnClickListener(this);
        rb_cheat.setOnClickListener(this);

        etMessage=(EditText)dialog.findViewById(R.id.et_message);
        btDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ivSendMessage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewMessage = etMessage.getText().toString();
                dialog.dismiss();
                send_message("Review message", 5);
            }
        });
        dialog.show();
    }

}
