package com.shatil.addpoint;

/**
 * Created by Shatil on 11/7/2015.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Shatil on 11/7/2015.
 */
public class CoreNavigationDrawerAdapter extends RecyclerView.Adapter<CoreNavigationDrawerAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private static String[] titles = null;
    private static int[] icons = null;

    public CoreNavigationDrawerAdapter(Context context) {
        this.context = context;
        // drawer labels
        titles = context.getResources().getStringArray(R.array.nav_drawer_labels);
        // drawer icons
        icons = new int[]{
                R.drawable.uploadtoserver,
                R.drawable.uploadtoserverdone,
                R.drawable.admin,
                R.drawable.app_icon,
        };
        inflater = LayoutInflater.from(context);

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.base_nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(titles[position]);
        holder.icon.setImageResource(icons[position]);  // don't call as BackgroundRes/Others whatever
        holder.icon.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in));
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.drawer_list_title);
            icon = (ImageView) itemView.findViewById(R.id.drawer_list_icon);
        }
    }
}

