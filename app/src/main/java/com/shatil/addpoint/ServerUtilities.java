/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.TAG;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.REG_STATUS;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.CommonUtilities.progdia;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.stjson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;

/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtilities {

    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();
    static String SERVER_URL="";
    static URL gurl;
    static String gstatement;
    static Context cont;
    static int status_reg=1;
    /**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    static void appsettings(Context context)
    {
    	SharedPreferences  datacenter = context.getSharedPreferences(filename, 0);
    	try {
			JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
			SERVER_URL=jobj.getString("server_url");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    static boolean register(final Context context, final String regId) {
    	reg_id=regId;
        Log.i(TAG, "registering device (regId = " + regId + ")");
        status_reg=1;
        cont=context;
        URL url=null;
		try {
			appsettings(context);
			url = new URL(SERVER_URL+"/register");
		} catch (MalformedURLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
        JSONObject jsonobj=new JSONObject();
        try {
			jsonobj.put("reg_id", regId);
			jsonobj.put("_id", regId);
			jsonobj.put("user_name", admin_name);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                //displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
            	new GenerateNotification(cont, "Requesting to register..("+ i+"/"+MAX_ATTEMPTS+")");
                upload(url,jsonobj.toString());
                GCMRegistrar.setRegisteredOnServer(context, true);
                try{
            		progdia.dismiss();
            	}catch(Exception e)
            	{	
            	}
                return true;
            } catch (Exception e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                //new GenerateNotification(cont, "Error_serv:"+e.toString());
            	Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        new GenerateNotification(cont, "Registration not successfull connection error and will try again in next session");
        try{
    		progdia.dismiss();
    	}catch(Exception e)
    	{	
    	}
        return false;
    }

    /**
     * Unregister this account/device pair within the server.
     */
    static boolean unregister(final Context context, final String regId) {
    	status_reg=0;
    	URL url=null;
		try {
			appsettings(context);
			url = new URL(SERVER_URL+"/unregister");
		} catch (MalformedURLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
        JSONObject jsonobj=new JSONObject();
        try {
			jsonobj.put("reg_id", regId);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to unregister");
            try {
                //displayMessage(context, "unregistering\ntry no:"+i);
                new GenerateNotification(cont, "Trying to unregistering...");
            	upload(url,jsonobj.toString());
                return true;
            } catch (Exception e) {
                Log.e(TAG, "Failed to unregister on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        new GenerateNotification(cont, "Unregistration error");
        return false;
    }
    public static String upload(URL url,String result) throws Exception
	{
		JSONObject json=new JSONObject();
		gstatement=result;
        gurl=url;
        //new GenerateNotification(cont, "Trying to open url"+result);
		HttpURLConnection conn = (HttpURLConnection) gurl.openConnection();
		conn.setDoOutput(true);
		conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf8");
        //conn.setRequestProperty("Authorization","key=AIzaSyAHPNRSNuECQdIBUIrIEtqCyYrL3hgNjxA");
        OutputStream os = conn.getOutputStream();
        os.write(gstatement.getBytes());
        os.flush();
        //int tres=conn.getResponseCode();
        BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		String output;
		String res="";
		while ((output = br.readLine()) != null) {
			System.out.println(output);
			res=res+output;
		}
		br.close();
		json=new JSONObject(res);
		String str="Registration";
		if(status_reg==0)
		{
			str="Unegistration";
		}
		if(status_reg==1) 
    	{
    		JPUtilities jputil=new JPUtilities(cont);
    		admin_id=json.getString("user_id");
    		jputil.settopref("user_id",admin_id);
    		jputil.settopref("reg_id",reg_id);
			jputil.setintpref(REG_STATUS, 2);
			stjson.put(admin_id, new JSONObject().put("user_name", admin_name));
			jputil.settopref("user_rating", stjson.toString());
    	}
    	return str;
    }
}
