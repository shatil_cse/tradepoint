package com.shatil.addpoint;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.shatil.addpoint.CommonUtilities.SAVE_LIST;
/**
 * Created by Shatil on 1/26/2016.
 */
public class SavedProductListFragment extends Fragment implements View.OnClickListener,DialogInterface.OnClickListener {
    JSONObject jsonobj;
    static Reset_product_list reset_saved_product_list;
    static Ratingloaded reset_saved_product_rating;
    JPUtilities jputil;
    ItemAdapter iadp;
    String result;
    ListView lvProductList;
    TextView tvClearAll;
    LinearLayout layHeader;
    TextView tvSetting;
    Activity activity;
    Context context;
    ProgressDialog progressDialog;
    @SuppressLint("UseSparseArrays")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.saved_product_list_fragment , container , false);
        context=getContext();
        activity=getActivity();
        lvProductList=(ListView)v.findViewById(R.id.lv_product_list);
        tvClearAll=(TextView)v.findViewById(R.id.tv_clear_all);
        layHeader=(LinearLayout)v.findViewById(R.id.lay_header);
        layHeader.setVisibility(View.GONE);
        tvClearAll.setOnClickListener(this);
        tvSetting=(TextView)v.findViewById(R.id.tv_notification_setting);
        tvSetting.setVisibility(View.GONE);
        iadp=new ItemAdapter(getActivity(), new ArrayList<Item>());
        lvProductList.setAdapter(iadp);
        jputil=new JPUtilities(getActivity());
        result = jputil.getjarrfrompref(SAVE_LIST).toString();
        new LoadInBackground().execute("");
        //set the list items.................
        reset_saved_product_list=new Reset_product_list() {

            @Override
            public void on_data_reset_pref_key(String tag) {
                // TODO Auto-generated method stub
                try {
                    Log.d(getClass().getSimpleName(), "Resetting product list");
                    result = jputil.getjarrfrompref(SAVE_LIST).toString();
                    new LoadInBackground().execute("");
                }catch (Exception e)
                {
                }
            }
            @Override
            public void on_data_reset_intent_result(String tag) {
                // TODO Auto-generated method stub
                Log.d(getClass().getSimpleName(), "Resetting by given product list");
                result=tag;
                new LoadInBackground().execute("");
            }
        };
        reset_saved_product_rating=new Ratingloaded() {

            @Override
            public void on_rating_loaded(String tag) {
                // TODO Auto-generated method stub
                try {
                    iadp.notifyDataSetChanged();
                    Log.d(getClass().getSimpleName(), "Rating reloaded of product list");
                }catch (Exception e)
                {

                }
            }
        };
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_clear_all:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setMessage("Are you sure to Clear all?");
                alertDialogBuilder.setPositiveButton("Confirm", this);
                alertDialogBuilder.setNegativeButton("Cancel", this);
                alertDialogBuilder.setTitle("Confirmation");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which)
        {
            case AlertDialog.BUTTON_POSITIVE:
                jputil.removefrompref(SAVE_LIST);
                Log.d(getClass().getSimpleName(),"Removing saved products...");
                break;
        }
    }

    public class ItemAdapter extends ArrayAdapter<Item> {
        public ItemAdapter(Context c, List<Item> items) {
            super(c, 0);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemView itemView = (ItemView)convertView;
            if (null == itemView) itemView = ItemView.inflate(parent);
            itemView.setItem(getItem(position),position);
            return itemView;
        }
    }

    public class LoadInBackground extends AsyncTask<String,Integer,String>
    {
        ArrayList<Item> items = new ArrayList<Item>();
        @Override
        protected void onPreExecute() {
            progressDialog=ProgressDialog.show(activity,"","Loading products..");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try{
                JSONArray jsonarr=new JSONArray(result);
                int len=jsonarr.length();
                for (int i=0; i < len; i++) {
                    try {
                        JSONObject jsonobj = jsonarr.getJSONObject(i);
                        Item item = new Item(jsonobj);
                        items.add(item);
                    } catch (Exception e) {
                    }
                }
            }catch(Exception e)
            {
                Log.d(getClass().getSimpleName(),"SavedProductListFragment Load failed:"+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                if(progressDialog!=null&&progressDialog.isShowing()) progressDialog.dismiss();
                iadp.clear();
                iadp.addAll(items);
                Log.d(getClass().getSimpleName(), "SavedProductListFragment Loaded: " + iadp.getCount());
                lvProductList.setAdapter(iadp);
                iadp.notifyDataSetChanged();
            }catch (Exception e)
            {
                Log.d(getClass().getSimpleName(),"SavedProductListFragment Load updating failed:"+e.toString());
            }
        }
    }
}
