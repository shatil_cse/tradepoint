package com.shatil.addpoint;


import static com.shatil.addpoint.CommonUtilities.LAST_SEARCH;
import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_NAME;
import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.reg_id;
import static com.shatil.addpoint.ItemListFragment.rpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Browseproduct extends Fragment implements OnClickListener,Dialogmessageview.NoticeDialogListener, OnTouchListener{
	String catagory[];
	int selecteditem=0;
	int open_prog=0;
	EditText etname;
	Spinner spcatagory,spFilters;
	ImageView btupload,btnotify;
	LinearLayout pb;
	LinearLayout lay_search;
	int backcolor;
	ImageView tv_search,iv_search,ivNotifyIcon;
	TextView tv_help,tvNotifyText;
	LinearLayout lay_note_set;
	String jsondata="";
	long interval=0;
	boolean ennotify=false,isExactMatchEnabled=false,isOnlyShopEnabled=false,isOnlyNewEnabled=false,isHomedelivarableEnabled=false;
	Dialogmessageview saveddialog;
	Uploaddownload updown;
	JPUtilities jputil;
	Typeface tf2,tf3;
	Context context;
	int active=0,show_dialog=0;
	static On_task_complete search_task_complete;
	public Browseproduct()
	{

	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		rootView = inflater.inflate(R.layout.browseproduct , container , false);
		context=getActivity().getApplicationContext();
		jputil=new JPUtilities(context);
		tf3=Typeface.createFromAsset(context.getAssets(),"uitalic.ttf");
		tf2=Typeface.createFromAsset(context.getAssets(),"uregular.ttf");
		appsettings();
		setviews(rootView);
        rpl.on_data_reset_pref_key(LAST_SEARCH);
		return rootView;
	}

	@Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
		String st=dialog.getTag();
		if(st.equals("exit_decission"))
		{
			//finish();
		}
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    	String st=dialog.getTag();
		if(st.equals("notification_decission"))
		{
			
		}
    }
	public void appsettings()
    {
    	SharedPreferences  datacenter = context.getSharedPreferences(filename, 0);
    	try {
			JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
			JSONArray jarr=new JSONArray();
			jarr=jobj.getJSONArray("catagory");
			int len=jarr.length();
			catagory=new String[len+1];
			catagory[0]="All recent product";
			for(int i=0;i<len;i++) catagory[i+1]=jarr.getString(i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	String []filterText={"Exact match with search text","Only new products","Only shop products","Only home deleverable products",};
    private void setviews(View view) {
		// TODO Auto-generated method stub
    	lay_note_set=(LinearLayout)view.findViewById(R.id.laynotelist);
    	lay_note_set.setOnTouchListener(this);
        tvNotifyText=(TextView)view.findViewById(R.id.tv_notify_text);
        ivNotifyIcon=(ImageView)view.findViewById(R.id.iv_notify_icon);
        setNotifyStatus(false);
    	tv_help=(TextView)view.findViewById(R.id.tv_help);
    	tv_help.setOnTouchListener(this);
    	etname=(EditText)view.findViewById(R.id.etname);
    	etname.setTypeface(tf2);
    	spcatagory=(Spinner)view.findViewById(R.id.spcatagory);
    	ArrayAdapter<String> spad=new ArrayAdapter<String>(context,R.layout.textview,catagory);
        spcatagory.setAdapter(spad);
		spFilters=(Spinner)view.findViewById(R.id.sp_filter);
		ArrayAdapter<String> adapterFilter=new ArrayAdapter<String>(context,android.R.layout.select_dialog_multichoice,filterText);
		spFilters.setAdapter(adapterFilter);
        tv_search=(ImageView)view.findViewById(R.id.tvsearch);
    	iv_search=(ImageView)view.findViewById(R.id.ivclose);
    	lay_search=(LinearLayout)view.findViewById(R.id.lay_search);
    	lay_search.setVisibility(View.GONE);
    	//lay_search.setOnTouchListener(this);
    	pb=(LinearLayout)view.findViewById(R.id.pb_search);
    	iv_search.setOnClickListener(this);
    	tv_search.setOnTouchListener(this);
    	//buttons..................
    	btnotify=(ImageView)view.findViewById(R.id.ivnotify);
    	btupload=(ImageView)view.findViewById(R.id.ivupload);
        btnotify.setOnTouchListener(this);
		btupload.setOnTouchListener(this);
        //radio buttons.................
        search_task_complete=new On_task_complete() {
			
			@Override
			public void on_task_complete(String tag) {
				// TODO Auto-generated method stub
				open_prog=0;
				pb.setVisibility(View.GONE);
				tv_search.setEnabled(true);
				if(updown.get_status_int()==0||updown.get_status_int()==99)
				{
					String result=updown.getresult();
					jputil.settopref(LAST_SEARCH, result);
					if(active==1) rpl.on_data_reset_pref_key(LAST_SEARCH);
					download_rating(result);
					lay_search.setVisibility(View.GONE);	
					//Toast.makeText(context, text, duration);
				}
				else if(updown.get_status_int()==99) 
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(context);
					cdia.set_args(updown.get_status_string(),101,BitmapFactory.decodeResource(getResources(), R.drawable.warning),"Empty result");
					//if(active==1) cdia.show(context.getSupportFragmentManager(), "downloding_result");
					//else saveddialog=cdia;
					Toast.makeText(context,"No Product found ...",Toast.LENGTH_LONG);
				}
				else
				{
					Dialogmessageview cdia=new Dialogmessageview();
					cdia.initialize_bmp(context);
					cdia.set_args(updown.get_status_string(),201,"Error happned");
					//if(active==1) cdia.show(getSupportFragmentManager(), "downloding_result");
					//else saveddialog=cdia;
                    Toast.makeText(context,updown.get_status_string(),Toast.LENGTH_LONG);
					
				}
				if(active==0) show_dialog=1;
			}
		};
        spcatagory.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				selecteditem=arg2;
				close_keyboard(spcatagory);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
    public void close_keyboard(View view)
    {
    	InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public int scaletextsize(float tflo,int psize,int ssize)
    {
    	tflo=tflo*ssize;
    	return (int)tflo/psize;
    }
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		close_keyboard(spcatagory);
		active=0;
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		active=1;
		if(show_dialog==1)
		{
			//saveddialog.show(getSupportFragmentManager(), "uploading_result");
		}
		show_dialog=0;
		search();
		super.onResume();
	}
	public boolean search()
	{
		open_prog=1;
		try{
			if(checkinput())
			{

                Log.d(getClass().getSimpleName(),"Json Data: "+jsondata);
				//do the search....
				updown=new Uploaddownload(context, "/getalldata", jsondata,1);
				if(ennotify)
				{
					save_notification();
				}
                pb.setVisibility(View.VISIBLE);
                tv_search.setEnabled(false);
                return true;
			}
		}catch(Exception e)
		{

            Log.d(getClass().getSimpleName(),"Error in search: "+e.toString());
			//Toast.makeText(context, "Operation Failed:"+e.toString(), Toast.LENGTH_LONG).show();
		}
		//set work to upload........................
		return false;
	}
	
	public void work_for_click(View v) {
		switch (v.getId()) {
		case R.id.ivupload:
			lay_search.setVisibility(View.VISIBLE);
			if(open_prog==0) 
			{
				pb.setVisibility(View.GONE);
				tv_search.setEnabled(true);
			}
			else 
			{
				tv_search.setEnabled(false);
				pb.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.tvsearch:
			search();
			break;
		case R.id.ivclose:
			lay_search.setVisibility(View.GONE);
			break;
		case R.id.laynotelist:
			//show_product(NOTIFICATION_STATEMENT,11);
			ennotify=!ennotify;
            setNotifyStatus(ennotify);
            break;
			
		}
		close_keyboard(v);
	}
    public void setNotifyStatus(boolean ennotify)
    {
        if(ennotify)
        {
            ivNotifyIcon.setImageResource(R.drawable.note_product);
            tvNotifyText.setText("Notify applied");
            tvNotifyText.setTextColor(getResources().getColor(R.color.search_area_text_color_enable));
        }
        else
        {
            ivNotifyIcon.setImageResource(R.drawable.note_product);
            tvNotifyText.setText("Notify Not Applied");
            tvNotifyText.setTextColor(getResources().getColor(R.color.search_area_text_color_disable));
        }
    }
	public boolean show_product(String key,int view_status)
	{
		try{
			//send preference key to load data from preference..
			jputil.settopref("should_open_option"+key,false);
			Intent i=new Intent(context,Browseresult.class);
			i.putExtra("pref_key", key);
			i.putExtra("load_result", 1);
			i.putExtra("view_status", view_status);
			startActivity(i);
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}
    View rootView;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)rootView.findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			View view=(View)rootView.findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			else if(temp>20)
			{
				try{
				Toast.makeText(context,v.getTag().toString(), Toast.LENGTH_LONG).show(); 
				}catch(Exception e)
				{
					
				}
			}
			//if(v.getId()==R.id.tvsearch) tv_search.setBackgroundColor(Color.parseColor("#334444"));
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 

			View view=(View)rootView.findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			interval=0;
			//if(v.getId()==R.id.tvsearch) tv_search.setBackgroundColor(Color.parseColor("#334444"));
		}
		return true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		work_for_click(v);
	}
	//for notification setup........................................
	public void save_notification() throws JSONException
	{
		
		String selectname="";
		JSONObject jobj=new JSONObject(jsondata);
		JSONObject statement=jobj.getJSONObject("statement");
		JSONObject cmptime=new JSONObject();
		cmptime.put("$gt",jputil.getfrompref("notification_time", 0));
		statement.put("curtime", cmptime);
		if(selecteditem>0) 
		{
			selectname="Search in catagory "+catagory[selecteditem];
			statement.put("catagory", catagory[selecteditem]);
		}
		else 
		{
			selectname="Search in all recent products";
		}
		if(jobj.getBoolean("text_search"))
		{
			selectname+=" filter "+jobj.getString("filter_name");
		}
		if(isExactMatchEnabled||isHomedelivarableEnabled||isOnlyNewEnabled||isOnlyShopEnabled)
		{
			selectname=selectname+"\n";
			if(isExactMatchEnabled) selectname+="Exact match with search text ";
			if(isOnlyNewEnabled) selectname+="new ";
			if(isHomedelivarableEnabled) selectname+="home deliverable ";
			if(isOnlyShopEnabled) selectname+="Shop or business ";
			selectname+="product";
		}
		JPUtilities jp=new JPUtilities(context);
		jobj.put("statement", statement);
		if(jp.savecapedmessage(NOTIFICATION_NAME, selectname,false,true,4)) {

			jobj.put("message_body", selectname);
			jp.savecapedmessage(NOTIFICATION_STATEMENT, jobj,false,4);
		}
		
		boolean alarm_flag=jp.getfrompref("alarm_flag",false);
		if(!alarm_flag)
		{
			Toast.makeText(context, "Notification Enabled check preferences to view details", Toast.LENGTH_LONG).show();
			setalarm();
			jp.settopref("alarm_flag", true);
		}
		Toast.makeText(context, "Notification statement saved.", Toast.LENGTH_LONG).show();
        ennotify=false;
        setNotifyStatus(ennotify);
    }
	public void setalarm() {
		// TODO Auto-generated method stub
		try {
			AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent in = new Intent(context, Myalarm.class);
			in.putExtra("come_from", 0);
			PendingIntent alarmin = PendingIntent.getBroadcast(context, 0, in, 0);
			long interval=jputil.getfrompref("notification_time_interval", 60*60*1000);
			am.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+interval, interval, alarmin);
			enableboorreceiver();
		} catch (Exception e) {
			//Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	private void enableboorreceiver() {
		// TODO Auto-generated method stub
		try{
		ComponentName receiver = new ComponentName(context,
				Simplebootrecever.class);
		PackageManager packm = context.getPackageManager();
		packm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
		}catch(Exception e)
		{
		}
	}
	public JSONObject set_additional_options(JSONObject statement) throws JSONException
	{
		if(isHomedelivarableEnabled) statement.put("type_delivery", 1);//1 is for home delivery
		if(isOnlyNewEnabled) statement.put("type_use", 0); //0 for new product
		if(isOnlyShopEnabled) statement.put("type_product",1); //1 for business product
		return statement;
	}
	public boolean checkinput() {
		try{
			JSONObject statement=new JSONObject();
			statement=set_additional_options(statement);
            JSONObject jsonobj=new JSONObject();
			if(selecteditem>0)
			{
					jsonobj.put("collection_name","searchdet"+catagory[selecteditem]);
			}
			else jsonobj.put("collection_name","notification_collection");
			
			String name="";
			try{
				name=etname.getText().toString().toLowerCase();		
			}catch(Exception e)
			{
				
			}

			//name=jputil.get_saerch_staring(name, cbmatch.isChecked());//was useed for text search...........

			if(name.equals("")||name.endsWith(" "))
			{
				etname.setText("");
				jsonobj.put("text_search", false);
			}
			else
			{
				name=name.trim();
                jsonobj.put("text_search", true);
                jsonobj.put("index_name", "tag_name");
                jsonobj.put("filter_name", name);
			}
			jsonobj.put("statement", statement);
			jsondata=jsonobj.toString();
			return true;
		}catch(Exception e)
		{
            Log.d(getClass().getSimpleName(),"Error in search: "+e.toString());
            //Toast.makeText(context, "Error in setting data:\n"+e.toString(), Toast.LENGTH_LONG).show();
			return false;
		}
	}
	public void download_rating(String result)
	{
	try{
		JSONArray jarr=new JSONArray(result);
		int len=jarr.length();
		if(len<1) return;
		JSONArray jarr2=new JSONArray();
		JSONObject jtrac=new JSONObject();
		for(int i=0;i<len;i++) 
		{
			String tuser_id=jarr.getJSONObject(i).getString("user_id");
			if(!jtrac.has(tuser_id)) 
			{
				jarr2.put(new JSONObject().put("_id",tuser_id));
				jtrac.put(tuser_id, true);
			}
		}
		JSONObject object=new JSONObject();
		object.put("reg_id",reg_id );
		object.put("user_id",admin_id);
		JSONObject statement=new JSONObject();
		statement.put("$or", jarr2);
		object.put("statement",statement);
		//Toast.makeText(context, object.toString(), Toast.LENGTH_LONG).show();
		//Toast.makeText(context, object.toString(), Toast.LENGTH_LONG).show();
		updown=new Uploaddownload(context, "/download_rating",object.toString(),3);
		}catch(Exception e)
		{
			//new GenerateNotification(context, e.toString());
		}
	}
}
