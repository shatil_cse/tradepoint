package com.shatil.addpoint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class Simplebootrecever extends BroadcastReceiver{

	public Context cont;
		@Override
		public void onReceive(Context context, Intent intent) {
			cont=context;
			// TODO Auto-generated method stub
			try{
				AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
				Intent in = new Intent(context, Myalarm.class);
				in.putExtra("come_from", 0);
				PendingIntent alarmin = PendingIntent.getBroadcast(context, 0, in, 0);
				JPUtilities jputil=new JPUtilities(cont);
				long interval=jputil.getfrompref("notification_time_interval", 60*60*1000);
				am.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+interval, interval, alarmin);
			}catch(Exception e)
			{
				Toast.makeText(context,"Error in boot:"+ intent.getDataString()+e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}
