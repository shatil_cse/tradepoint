/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shatil.addpoint;

import java.util.HashMap;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
public final class CommonUtilities {

    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
	static String SERVER_URL="http://tradepoint-airship.rhcloud.com/tradepoint";
    static String SAVE_LIST="saved_list";
    static String NOTIFIED_PRODUCT="notified_product";
    static String ADMIN_PRODUCT="admin_product";
    static String admin_id;
    static String reg_id;
    static String admin_name;
    static HashMap<Integer, Item>hm_item;
	static JSONObject stjson;
    static int is_loading,element_count;
    static String APP_OPEN="app_open";
    static String DB_OPEN="db_open";
    static String ST_OPEN="st_open";
    static String REG_STATUS="reg_status";
    static int MESSAGE_VIEW_STATUS;
    static String NOTIFICATION_NAME="notification_name";
    static String NOTIFICATION_STATEMENT="notification_statement";
    static String ERROR_STATEMENT="Error_identified";
    static String LAST_SEARCH="last_search_product_lists";
    static String catagory[]={"Select catagory","Furnitures","Sports","Electronics","Mobile Phones","Others"};
    static final String filename="preferencessofBuyandSell";
	static DButilities dbutil;
	static ProgressDialog progdia;
    public static String ALARM_FLAG="alarm_flag";
    public static String NOTIFICATION_INTERVAL_TIME="notification_time_interval";
    /**
     * Google API project id registered to use GCM.
     */
    static String SENDER_ID = "819466763396";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "Tab";

    /**
     * Intent used to display a message in the screen.
     */
    /*static final String DISPLAY_MESSAGE_ACTION =
            "com.shatil.tradepoint.DISPLAY_MESSAGE";*/
    /**
     * Intent's extra that contains the message to be displayed.
     */
    static final String EXTRA_MESSAGE = "message";

    static void displayMessage(Context context, String message) {
    	try{
    	Intent intent = new Intent(context,Myalarm.class);
    	intent.putExtra("come_from", 1);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    	}catch(Exception e)
    	{
    		message=e.toString();
    	}
    }
}
