package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_NAME;
import static com.shatil.addpoint.CommonUtilities.NOTIFICATION_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.filename;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NotificationView extends RelativeLayout  implements OnTouchListener{
	JPUtilities jputil;
	SharedPreferences datacenter;
	Bitmap bmp_admin,bmp_not_saved,bmp_saved;
	long interval=0;
	ImageView ivremove;
	LinearLayout lay_total;
	TextView tv_message_body;
	String message_body=" ";
	String admin_id;
	Context context;
	int status_view=11;
	JSONObject jobj=new JSONObject();
	public static NotificationView inflate(ViewGroup parent) {
		 NotificationView messageView = ( NotificationView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notificationview, parent, false);
        return  messageView;
    }

    public NotificationView(Context c) {
        this(c, null);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

		LayoutInflater.from(context).inflate(R.layout.notification_children_view, this, true);
		jputil=new JPUtilities(context,4);
		datacenter = context.getSharedPreferences(filename, 0);
		bmp_admin=BitmapFactory.decodeResource(context.getResources(), R.drawable.admin);
		bmp_saved=BitmapFactory.decodeResource(context.getResources(), R.drawable.select);
		bmp_not_saved=BitmapFactory.decodeResource(context.getResources(), R.drawable.add_product);
		admin_id=jputil.getfrompref("user_id");
		setupChildren(context);
        this.context=context;
    }
    public void set_status(int status_view)
    {
    	this.status_view=status_view;
    }
    public void setupChildren(Context context)
    {
    	tv_message_body=(TextView)findViewById(R.id.tvmessage_body);
    	ivremove=(ImageView)findViewById(R.id.ivremove);
    	lay_total=(LinearLayout)findViewById(R.id.laytotal);
    	ivremove.setOnTouchListener(this);
    }
    public void setItem(Item item) {
    try {
		setjsonitem(item);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	tv_message_body.setText(message_body);
    }
	private void setjsonitem(Item item) throws JSONException {
		// TODO Auto-generated method stub
		jobj=item.getjson();
		message_body=jobj.getString("message_body");
	}
	public void remove(){
		if(status_view==11)
		{
			jputil.savecapedmessage(NOTIFICATION_NAME,message_body,true,true);
			jputil.remove_capedmessage(NOTIFICATION_STATEMENT,jobj,"message_body");
			lay_total.setVisibility(View.GONE);
		}
	}
	public void work_for_click(View v)
	{
		switch (v.getId()) {
		case R.id.ivremove:
			remove();
			break;
		}
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(MotionEvent.ACTION_DOWN==event.getAction())
		{
			interval=System.currentTimeMillis();
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.argb(100, 0, 200, 0));
		}
		else if(MotionEvent.ACTION_UP==event.getAction()&&interval!=0)
		{
			long temp=System.currentTimeMillis()-interval;
			if(temp>20&&temp<2000) work_for_click(v);
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
			
		}
		else if(MotionEvent.ACTION_CANCEL==event.getAction()||MotionEvent.ACTION_OUTSIDE==event.getAction())
		{ 
			interval=0;
			View view=(View)findViewById(v.getId());
			view.setBackgroundColor(Color.TRANSPARENT);
		}
		return true;
	}
	
}
