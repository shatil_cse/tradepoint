package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.ERROR_STATEMENT;
import static com.shatil.addpoint.CommonUtilities.SERVER_URL;
import static com.shatil.addpoint.CommonUtilities.progdia;
import static com.shatil.addpoint.CommonUtilities.NOTIFIED_PRODUCT;
import static com.shatil.addpoint.CommonUtilities.filename;
import static com.shatil.addpoint.CommonUtilities.stjson;
import static com.shatil.addpoint.CommonUtilities.admin_id;
import static com.shatil.addpoint.CommonUtilities.admin_name;
import static com.shatil.addpoint.ItemListFragment.rt_load;
import static com.shatil.addpoint.Myalarm.notification_download_complete;
import static com.shatil.addpoint.Browseproduct.search_task_complete;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class Uploaddownload{
	Context context;
	String path;
	String jsondata="",result="";
	String status_string=" ";
	int staus_int=400;
	ProgressDialog pdia;
	int status_back=0;
	String tag="no_tag";
	Download_finish df;
	//called when it downloads data by user intension....................
	public Uploaddownload(Context context,String path,String jsondata,ProgressDialog pdia)
	{
		this.status_back=0;
		this.context=context;
		this.path=path;
		this.pdia=pdia;
		this.jsondata=jsondata;
		appsettings(context);
		//this.pdia=ProgressDialog.show(context, "Operation in progress", "Please wait");	
		new Updownbyasyn().execute("failed");
	}
	public Uploaddownload(Context context,String path,String jsondata,Download_finish df,String tag)
	{
		this.status_back=100;
		this.context=context;
		this.path=path;
		this.tag=tag;
		this.df=df;
		this.jsondata=jsondata;
		appsettings(context);
		//this.pdia=ProgressDialog.show(context, "Operation in progress", "Please wait");	
		new Updownbyasyn().execute("failed");
	}
	//this contructor will be called when it will execute in background.......
	public Uploaddownload(Context context,String path,String jsondata,int status)
	{
		this.status_back=status;
		this.context=context;
		this.path=path;
		this.jsondata=jsondata;
		appsettings(context);
		//this.pdia=ProgressDialog.show(context, "Operation in progress", "Please wait");	
		new Updownbyasyn().execute("failed");
	}
	
	public void appsettings(Context context)
    {
    	SharedPreferences  datacenter = context.getSharedPreferences(filename, 0);
    	try {
			JSONObject jobj=new JSONObject(datacenter.getString("settings","{}"));
			//SERVER_URL=jobj.getString("server_url");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	public String downloaddata()
	{
		URL url;
		try {

			Log.d(getClass().getSimpleName(),"Url:"+SERVER_URL+path+" \nReqqStr:"+jsondata);
			url = new URL(SERVER_URL+path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/json; charset=utf8");
	        conn.setConnectTimeout(30000);
	        //conn.setRequestProperty("Authorization","key=AIzaSyAHPNRSNuECQdIBUIrIEtqCyYrL3hgNjxA");
	        OutputStream os = conn.getOutputStream();
	        os.write(jsondata.getBytes());
	        os.flush();
	        
	        BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			String output;
			String tres="";
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				tres=tres+output;
			}
			br.close();
			staus_int=0;

			Log.d(getClass().getSimpleName(),"ResStr: "+tres);
			return tres;
			
		} catch (Exception e) {
			// TODO Auto-generated catch blo
			e.printStackTrace();
			status_string=e.toString();
			staus_int=400;
			Log.d(getClass().getSimpleName(),"Error in download:"+"Code:"+staus_int+" str:"+status_string);
			return status_string;

		}
	}
	public class Updownbyasyn extends AsyncTask<String, Integer, String>
	{
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			return downloaddata();
		}
		@Override
		protected void onPostExecute(String res) {
			// TODO Auto-generated method stub
			super.onPostExecute(res);
			try {
				JPUtilities jputil=new JPUtilities(context);
				result=res; //first initialize the returned data ....
				//new GenerateNotification(context, res);
				JSONObject rset=new JSONObject(res);
				staus_int=rset.getInt("status_int");
				//new GenerateNotification(context, " "+staus_int);
				if(staus_int==1111)
				{
					try{
						progdia.dismiss();
					}catch(Exception e)
					{
						
					}
					return;
				}
				if(staus_int==2222)
				{
					//new GenerateNotification(context, "rating");
					try{
						if(!jputil.getfrompref("stjson_open", false)) 
						{
							try{
								stjson=new JSONObject(jputil.getfrompref("user_rating"));
							}catch(Exception e)
							{
								stjson=new JSONObject();
							}
						}
						//remove 1000 rating if it goes over 5000
						if(stjson.length()>5000) 
						{
							Iterator<String> it=stjson.keys();
							for(int i=0;i<1000&&it.hasNext();i++)
							{
								String key=it.next();
								if(!key.equals(admin_id)) stjson.remove(key);
							}
						}
						JSONArray jarr=rset.getJSONArray("user_info");
						int len=jarr.length();
						for(int i=0;i<len;i++)
						{
							JSONObject juser=jarr.getJSONObject(i);
	            			stjson.put(juser.getString("_id"), juser);
						}
						if(jputil.getfrompref("stjson_open", false)) jputil.settopref("user_rating", stjson.toString());
						try{
							rt_load.on_rating_loaded("rating loaded");
						}catch(Exception e)
						{
							//new GenerateNotification(context, "ETriger:"+e.toString());
						}
					}catch(Exception e)
					{
						//new GenerateNotification(context, "ETriger:"+e.toString());
					}
					return;
				}
				if(staus_int==250)
				{
					JSONObject jsonobj=new JSONObject(jsondata);
					int type=jsonobj.getInt("data_change_st");
					if(type==8)
					{
						new GenerateNotification(context, "Account transfer successfull");
						admin_id=rset.getString("_id");
						admin_name=rset.getString("user_name");
						//Toast.makeText(context, admin_id, Toast.LENGTH_LONG).show();
						jputil.settopref("user_id",admin_id);
						jputil.settopref("user_name",admin_name);
					}
					if(status_back==0) pdia.dismiss();
					return;
				}
				if(staus_int==260)
				{
					if(status_back==0) pdia.dismiss();
					return;
				}
				if(staus_int==100||staus_int==0)
				{
					JSONArray jarr=rset.getJSONArray("search_result");
					long note_time=rset.getLong("notification_time");
					result=jarr.toString();  //if result format is ok then set returned result to result
					if(status_back==100) df.download_finish(tag);
					else if(status_back==0) pdia.dismiss();
					else if(status_back==1) search_task_complete.on_task_complete(result);
					else if(status_back==2&&!result.startsWith(ERROR_STATEMENT))
					{
						jputil.chang_notification_time(note_time);
						jputil.settopref("notification_time", note_time);
						if(jarr.length()>0) 
						{
							notification_download_complete.on_task_complete(result);
							jputil.savecapedmessage(NOTIFIED_PRODUCT, jarr,100);
							jputil.settopref("should_open_option"+NOTIFIED_PRODUCT,true);
							/*Intent intent=new Intent(context,Browseresult.class);
							intent.putExtra("pref_key",NOTIFIED_PRODUCT);
							intent.putExtra("load_result", 1);
							intent.putExtra("view_status", 0);*/
							Intent intent=new Intent(context,CommonNotificationActivity.class);
							intent.putExtra("flag",NOTIFIED_PRODUCT);
							intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
							new GenerateNotification(context, "New product found for you",intent,R.drawable.noti_app_icon);
							NotifiedProductListFragment.reset_saved_product_list.on_data_reset_pref_key(NOTIFIED_PRODUCT);
						}
					}
				}
				else if(status_back==100) df.download_finish(tag);
				else if(status_back==0) 
				{
					if(pdia.isShowing()) pdia.dismiss();
				}
				else if(status_back==1) search_task_complete.on_task_complete(result);
				else if(status_back==2) notification_download_complete.on_task_complete(result);
				//new GenerateNotification(context, "int1:"+staus_int+"back:"+status_back);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				staus_int=400;
				//new GenerateNotification(context, "int:"+staus_int+"back:"+status_back);
				if(status_back==100) df.download_finish(tag);
				else if(status_back==0) 
				{
					if(pdia.isShowing()) pdia.dismiss();
				}
				else if(status_back==1) 
				{
					//new GenerateNotification(context, "int2:"+staus_int+"back:"+status_back);
					search_task_complete.on_task_complete(result);
					//new GenerateNotification(context, "int3:"+staus_int+"back:"+status_back);
					
				}
				else if(status_back==2) notification_download_complete.on_task_complete(result);
			}
			try{
				progdia.dismiss();
			}catch(Exception e)
			{
				
			}
		}
	}
	public int get_status_int() {
		// TODO Auto-generated method stub
		return staus_int;
	}
	/**
	 * if(staus_int==0) return "Search successfull";
		else if(staus_int==100) return "Upload successfull";
		else if(staus_int==260) return "Message send successfull";
		else if(staus_int==200) return "Successfully removed";
		else if(staus_int==99) return "No product found";
		else if(staus_int==400) return "Connection Error";
		else if(staus_int==500) return "Servers internal problem happened";
		else return "Unknown result";
	 * @return
	 */
	public String get_status_string() {
		// TODO Auto-generated method stub
		if(staus_int==0) return "Search successfull";
		else if(staus_int==100) return "Upload successfull";
		else if(staus_int==260) return "Message send successfull";
		else if(staus_int==200) return "Successfully removed";
		else if(staus_int==99) return "No product found";
		else if(staus_int==400) return "Connection Error";
		else if(staus_int==500) return "Servers internal problem happened";
		else return "Unknown result";
		
	}
	public String getresult() {
		// TODO Auto-generated method stub
		return result;
	}
}
