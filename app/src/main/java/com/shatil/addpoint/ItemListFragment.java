package com.shatil.addpoint;
import static com.shatil.addpoint.CommonUtilities.element_count;
import static com.shatil.addpoint.CommonUtilities.is_loading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

@SuppressLint("DefaultLocale")
public class ItemListFragment extends ListFragment{
	String jsondata;
	JSONObject jsonobj;
	JSONArray jsonarr;
	int load_result=0;
	int view_status=0;
	int new_data=0;
	static Reset_product_list rpl;
	static Ratingloaded rt_load;
	Reset_product_list trpl;
	Ratingloaded trt_load;
	JPUtilities jputil;	
	ItemAdapter iadp;
	int need_resume=0;
	String result="";
	@SuppressLint("UseSparseArrays")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        element_count=0;
        is_loading=0;
        iadp=new ItemAdapter(getActivity(), new ArrayList<Item>());
        setListAdapter(iadp);
        result=new JSONArray().toString();
        jputil=new JPUtilities(getActivity());
    	load_result=getActivity().getIntent().getIntExtra("load_result", 0);
        view_status=getActivity().getIntent().getIntExtra("view_status", 0);
        //get result string......................
        if( load_result==0) result=getActivity().getIntent().getStringExtra("intent_result");
        else if(load_result==1)
        {
        	String pref_key=getActivity().getIntent().getStringExtra("pref_key");
        	result=jputil.getjarrfrompref(pref_key).toString();
        }
        //set the list items.................
        setlistitems(result);
        rpl=new Reset_product_list() {
			
			@Override
			public void on_data_reset_pref_key(String tag) {
				// TODO Auto-generated method stub
				try{
				//Toast.makeText(getActivity(), "len="+jputil.getjarrfrompref(tag).toString(),Toast.LENGTH_LONG).show();
				}catch(Exception e)
				{
					//Toast.makeText(getActivity(), e.toString(),Toast.LENGTH_LONG).show();
				}
				result=jputil.getjarrfrompref(tag).toString();
				setlistitems(result);
			}
			
			@Override
			public void on_data_reset_intent_result(String tag) {
				// TODO Auto-generated method stub
				result=tag;
				setlistitems(result);
			}
		};
		rt_load=new Ratingloaded() {
				
				@Override
				public void on_rating_loaded(String tag) {
					// TODO Auto-generated method stub
					//new GenerateNotification(getActivity(), tag);
					iadp.notifyDataSetChanged();
			}
		};
		trpl=rpl;
		trt_load=rt_load;
        return v;
    }
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(need_resume==1)
		{
			need_resume=0;
			/*String result=new JSONArray().toString();
			if(load_result==1)
	        {
	        	String pref_key=getActivity().getIntent().getStringExtra("pref_key");
	        	result=jputil.getjarrfrompref(pref_key).toString();
	        	setlistitems(result);
	        }*/
			setlistitems(result);
	        rpl=trpl;
			rt_load=trt_load;
		}
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		need_resume=1;
		super.onPause();
	}
    public class ItemAdapter extends ArrayAdapter<Item> {
        public ItemAdapter(Context c, List<Item> items) {
        	super(c, 0);
        }
        
		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	if(view_status<10) 
        	{
                ItemView itemView = (ItemView)convertView;
                if (null == itemView) itemView = ItemView.inflate(parent);
                itemView.setItem(getItem(position),position);
                return itemView;
        	}
        	else
        	{
        		NotificationView messageView = (NotificationView)convertView;
                if (null == messageView)
                {
                    messageView = NotificationView.inflate(parent);
                    messageView.set_status(view_status);
                }
                messageView.setItem(getItem(position));
                return messageView;
        	}
        }
    }
    
    //sets the list... sets the notification time if it is a request for a notification... 
    public void setlistitems(String result)
    {
    	try{
    	JSONArray jsonarr=new JSONArray(result);
    	int len=jsonarr.length();
    	ArrayList<Item> items = new ArrayList<Item>();
		for (int i=0; i < len; i++) {
			try {
				JSONObject jsonobj = jsonarr.getJSONObject(i);
				Item item = new Item(jsonobj);
				items.add(item);
			} catch (Exception e) {
			}
		}
		iadp.clear();
		if(len>0) iadp.addAll(items);
		iadp.notifyDataSetChanged();
		}catch(Exception e)
    	{
    		//Toast.makeText(getActivity(), "ER2"+e.toString(),Toast.LENGTH_LONG).show();
			
    	}
    }
}