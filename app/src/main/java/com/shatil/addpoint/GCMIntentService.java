
package com.shatil.addpoint;

import static com.shatil.addpoint.CommonUtilities.SENDER_ID;
import static com.shatil.addpoint.CommonUtilities.displayMessage;
import static com.shatil.addpoint.CommonUtilities.progdia;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(SENDER_ID);
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        ServerUtilities.register(context, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            ServerUtilities.unregister(context, registrationId);
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }
    }
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message =" ";
        message=intent.getStringExtra("MESSAGE");
        //Toast.makeText(context, "Message received"+message, Toast.LENGTH_LONG).show();
        //Toast.makeText(context, "Message received", Toast.LENGTH_LONG).show();
        //Toast.makeText(context, "Message received", Toast.LENGTH_LONG).show();
        //new GenerateNotification(context, "Message receivedfuck you");
        displayMessage(context, message);
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        //displayMessage(context, message);
        // notifies user
        new GenerateNotification(context, message);
    }
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        //displayMessage(context, getString(R.string.gcm_error, errorId));
        new GenerateNotification(context, "Please contact with me to get working app server trail period has expired");
        try{
        	progdia.dismiss();
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        //displayMessage(context, getString(R.string.gcm_recoverable_error,
          //      errorId));
        new GenerateNotification(context,"Check your connection or the trail period has expired");
        try{
        	progdia.dismiss();
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
        return super.onRecoverableError(context, errorId);
    }

}
