package com.shatil.addpoint;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Selectoption extends Activity implements OnItemClickListener{

	String menuitem[]={"My profile","Contact us by sending message","Recovery options for your device change","Offline help about this app","Terms & conditions","About developer"};
	Intent i;
	ListView lv;
	Boolean mail_found=false,pin_found=false;
	int count=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectoption);
		ImageView ivBack=(ImageView) findViewById(R.id.ivback);
		ivBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		JPUtilities jputil=new JPUtilities(getApplicationContext());
		mail_found=jputil.getfrompref("mail_found", false);
		pin_found=jputil.getfrompref("pin_found",false);
		ArrayAdapter <String> ap=new ArrayAdapter<String>(getApplicationContext(),R.layout.textview, menuitem){

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				TextView tv=(TextView)super.getView(position, convertView, parent);
				tv.setTextColor(Color.BLACK);
				if(tv.getText().toString().equals(menuitem[2]))
				{
					if(!mail_found||!pin_found) tv.setTextColor(Color.RED);
				}
				return tv;
			}
			
		};
		lv=(ListView)findViewById(R.id.lvselect);
		lv.setAdapter(ap);
		lv.setOnItemClickListener(this);
		super.onResume();
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch (arg2) {
		case 1:
			startActivity(new Intent(getApplicationContext(),Support.class));
			break;
		case 2:
			startActivity(new Intent(getApplicationContext(),Recover_options.class));
			break;
		case 3:
			startActivity(new Intent(getApplicationContext(),Help.class));
			break;
		case 5:
			startActivity(new Intent(getApplicationContext(),ApplicationInformation.class));
			break;
		case 4:
			startActivity(new Intent(getApplicationContext(),Policy.class));
			break;
		case 0:
			startActivity(new Intent(getApplicationContext(),Profile.class));
			break;
		}
	}
}