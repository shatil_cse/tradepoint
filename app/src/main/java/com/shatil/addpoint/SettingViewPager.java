package com.shatil.addpoint;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;

/**
 * Created by Shatil on 1/26/2016.
 */
public class SettingViewPager extends FragmentPagerAdapter {

    CharSequence Titles[]=null;//new String[]{"Search Ads","Saved Adds","Notified Adds","Messages"}; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs=3; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

    int[] icons = null;//new int[]{R.drawable.search,R.drawable.select,R.drawable.note_set,R.drawable.message};
    Context context;

    Fragment enabledNotificationFragment = null;
    Fragment notificationSettingFragment = null;
    Fragment otherSettingFragment = null;
    // Build a Constructor and assign the passed Values to appropriate values in the class
    public SettingViewPager(Context context, FragmentManager fm,String [] titles,int [] icons,int length) {
        super(fm);
        this.context = context;
        this.Titles=titles;
        this.icons=icons;
        this.NumbOfTabs=length;
    }
    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0)
        {
            if(enabledNotificationFragment==null) enabledNotificationFragment=new SavedNotificationListFragment();
            return enabledNotificationFragment;
        }
        else if(position == 1)
        {
            if(notificationSettingFragment==null) notificationSettingFragment=new NotificationSettingFragment();
            return notificationSettingFragment;
        }
        else if(position==2){
            //if(groupFragment==null)groupFragment=new GroupFragment();
            if(otherSettingFragment==null) otherSettingFragment=new OtherSettingFragment();
            return otherSettingFragment;
        }

        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
